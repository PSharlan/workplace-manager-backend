---
swagger: "2.0"
info:
  title: Workplace Manager
  version: "unknown"
  description: Workplace backend
basePath: "/api/v1"
produces: ["application/json"]
tags:
  - name: Auth
  - name: Employees
  - name: Departments
  - name: Offices
  - name: Places
  - name: Requests
  - name: Rooms
  - name: Users

responses:

  BadRequest:
    description: Bad Request
    schema:
      $ref: "#/definitions/Problem"

  Unauthorized:
    description: Unauthorized
    schema:
      $ref: "#/definitions/Problem"

  Forbidden:
    description: Forbidden
    schema:
      $ref: "#/definitions/Problem"

  NotFound:
    description: Not Found
    schema:
      $ref: "#/definitions/Problem"

  Conflict:
    description: Conflict
    schema:
      $ref: "#/definitions/Problem"

  UnprocessableEntity:
    description: Unprocessable Entity
    schema:
      $ref: "#/definitions/Problem"

  ServiceUnavailable:
    description: Service Unavailable
    schema:
      $ref: "#/definitions/Problem"

paths:

  ########################################
  # Auth API
  ########################################

  "/login":
    post:
      tags: [Auth]
      operationId: login
      security: []
      consumes:
        - application/json
      parameters:
        - in: body
          name: body
          required: true
          schema:
            "$ref": "#/definitions/LoginRequest"
      responses:
        "201":
          description: Created
          schema:
            $ref: "#/definitions/Token"
        "400":
          "$ref": "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  "/refresh":
    post:
      tags: [Auth]
      operationId: refresh
      security: []
      parameters:
        - in: query
          name: refreshToken
          type: string
          required: true
      responses:
        "201":
          description: Created
          schema:
            $ref: "#/definitions/Token"
        "400":
          "$ref": "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "404":
          $ref: "#/responses/NotFound"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  ########################################
  # Departments API
  ########################################

  "/departments":
    get:
      tags: [Departments]
      operationId: getDepartments
      security:
        - userAuth: []
      responses:
        "200":
          description: OK
          schema:
            type: array
            items:
              type: string
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  "/departments/groups":
    get:
      tags: [Departments]
      operationId: getDepartmentGroups
      security:
        - userAuth: []
      parameters:
        - in: query
          name: department
          type: string
          required: true
      responses:
        "200":
          description: OK
          schema:
            type: array
            items:
              type: string
        "401":
          $ref: "#/responses/Unauthorized"
        "404":
          $ref: "#/responses/NotFound"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  ########################################
  # Employees API
  ########################################

  "/employees":
    get:
      tags: [Employees]
      operationId: getAllEmployees
      security:
        - userAuth: []
      parameters:
        - in: query
          name: department
          type: string
        - in: query
          name: username
          type: string
      responses:
        "200":
          description: OK
          schema:
            type: array
            items:
              $ref: "#/definitions/Employee"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  "/employees/{id}/place":
    parameters:
      - $ref: "#/parameters/UuidPathParameter"
    get:
      tags: [Employees]
      operationId: getEmployeePlace
      security:
        - userAuth: []
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/Place"
        "401":
          $ref: "#/responses/Unauthorized"
        "404":
          $ref: "#/responses/NotFound"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  ########################################
  # Offices API
  ########################################

  "/offices":
    post:
      tags: [Offices]
      description: Delete office if exists, otherwise create new one and import data from vsdx file
      operationId: importOfficesFromVsdx
      security:
        - userAuth: []
      consumes:
        - multipart/form-data
      parameters:
        - in: formData
          name: schemaFile
          description: Office schema as VSDX file
          type: file
          required: true
        - in: formData
          name: city
          description: Office's city
          type: string
          required: true
        - in: formData
          name: officeName
          type: string
          required: true
        - in: formData
          name: imageFile
          description: Office image for avatar
          type: file
          required: false
        - in: formData
          name: ignoreWarnings
          description: VSDX file processing parameter for ignoring all warnings
          type: boolean
          default: false
        - in: formData
          name: latitude
          description: Office geo coordinates - latitude
          type: number
          format: double
        - in: formData
          name: longitude
          description: Office geo coordinates - longitude
          type: number
          format: double
      responses:
        "201":
          description: CREATED
          schema:
            $ref: "#/definitions/Office"
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "422":
          $ref: "#/responses/UnprocessableEntity"
        "503":
          $ref: "#/responses/ServiceUnavailable"
    get:
      tags: [Offices]
      operationId: getAllOffices
      security:
        - userAuth: []
      responses:
        "200":
          description: OK
          schema:
            type: array
            items:
              $ref: "#/definitions/Office"
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  "/offices/{id}/rooms":
    parameters:
      - $ref: "#/parameters/UuidPathParameter"
    get:
      tags: [Offices]
      operationId: getOfficeRooms
      security:
        - userAuth: []
      responses:
        "200":
          description: OK
          schema:
            type: array
            items:
              $ref: "#/definitions/Room"
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  "/offices/{id}":
    parameters:
      - $ref: "#/parameters/UuidPathParameter"
    delete:
      tags: [Offices]
      operationId: deleteOfficeById
      security:
        - userAuth: []
      responses:
        "204":
          description: NO_CONTENT
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  ########################################
  # Places API
  ########################################

  "/places":
    get:
      tags: [Places]
      operationId: getAllPlaces
      security:
        - userAuth: []
      parameters:
        - in: query
          name: location
          type: string
        - in: query
          name: roomNumber
          type: string
        - in: query
          name: number
          type: integer
          default: 0
      responses:
        "200":
          description: OK
          schema:
            type: array
            items:
              $ref: "#/definitions/Place"
        "401":
          $ref: "#/responses/Unauthorized"
        "404":
          $ref: "#/responses/NotFound"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  ########################################
  # Requests API
  ########################################

  "/requests":
    post:
      tags: [Requests]
      operationId: createRequest
      security:
        - userAuth: []
      consumes:
        - application/json
      parameters:
        - in: body
          name: body
          required: true
          schema:
            $ref: "#/definitions/CreateRequest"
      responses:
        "201":
          description: CREATED
          schema:
            $ref: "#/definitions/Request"
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  ########################################
  # Rooms API
  ########################################

  "/rooms":
    post:
      tags: [Rooms]
      description: Delete room if exists, otherwise create
      operationId: importRoomsFromVsdx
      security:
        - userAuth: []
      consumes:
        - multipart/form-data
      parameters:
        - in: formData
          name: schemaFile
          type: file
          required: true
        - in: query
          name: officeId
          type: string
          format: uuid
          required: true
        - in: query
          name: ignoreWarnings
          type: boolean
          default: false
      responses:
        "201":
          description: CREATED
          schema:
            $ref: "#/definitions/Room"
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "404":
          $ref: "#/responses/NotFound"
        "422":
          $ref: "#/responses/UnprocessableEntity"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  "/rooms/{id}":
    parameters:
      - $ref: "#/parameters/UuidPathParameter"
    get:
      tags: [Rooms]
      operationId: getRoomById
      security:
        - userAuth: []
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/Room"
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"
    put:
      tags: [Rooms]
      operationId: updateRoomAttributes
      security:
        - userAuth: []
      parameters:
        - in: query
          name: newManagerId
          type: string
          format: uuid
          required: true
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/Room"
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "404":
          $ref: "#/responses/NotFound"
        "503":
          $ref: "#/responses/ServiceUnavailable"
    delete:
      tags: [Rooms]
      operationId: deleteRoomById
      security:
        - userAuth: []
      responses:
        "204":
          description: NO_CONTENT
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  "/rooms/{id}/svg":
    parameters:
      - $ref: "#/parameters/UuidPathParameter"
    get:
      tags: [Rooms]
      operationId: getRoomSvgById
      security:
        - userAuth: []
      produces:
        - image/svg+xml
      responses:
        "200":
          description: OK
          schema:
            type: file
        "400":
          $ref: "#/responses/BadRequest"
        "401":
          $ref: "#/responses/Unauthorized"
        "404":
          $ref: "#/responses/NotFound"
        "503":
          $ref: "#/responses/ServiceUnavailable"

  ########################################
  # Users API
  ########################################

  "/users/current":
    get:
      tags: [Users]
      operationId: getCurrentUser
      security:
        - userAuth: []
      responses:
        "200":
          description: OK
          schema:
            $ref: "#/definitions/User"
        "401":
          $ref: "#/responses/Unauthorized"
        "503":
          $ref: "#/responses/ServiceUnavailable"

definitions:

  CreateRequest:
    type: object
    required:
      - details
    properties:
      details:
        type: array
        items:
          $ref: "#/definitions/CreateRequestDetails"

  CreateRequestDetails:
    type: object
    required:
      - employeeId
      - requestType
    properties:
      placeFromId:
        type: string
        format: uuid
        x-nullable: true
      placeToId:
        type: string
        format: uuid
        x-nullable: true
      employeeId:
        type: string
        format: uuid
      type:
        $ref: "#/definitions/RequestDetailsType"

  Employee:
    type: object
    properties:
      id:
        type: string
        format: uuid
      smgId:
        type: string
      imageLink:
        type: string
      department:
        type: string
      group:
        type: string
      name:
        type: string
      localizedName:
        type: string
      location:
        type: string

  LoginRequest:
    type: object
    required:
      - username
      - password
    properties:
      username:
        type: string
        x-format: not-blank
      password:
        type: string
        x-format: not-blank

  Office:
    type: object
    properties:
      id:
        type: string
        format: uuid
      location:
        type: string
      address:
        type: string
      imageLink:
        type: string
      visioLink:
        type: string
      coordinates:
        type: array
        items:
          type: number
          format: double

  Place:
    type: object
    properties:
      id:
        type: string
        format: uuid
      employee:
        $ref: "#/definitions/Employee"
      roomId:
        type: string
        format: uuid
      number:
        type: integer
        format: int32
      visioCardId:
        type: integer
        format: int64
      status:
        $ref: "#/definitions/PlaceStatus"

  Problem:
    type: object
    properties:
      type:
        type: string
        format: uri
      title:
        type: string
      status:
        type: integer
        format: int32
        minimum: 100
        maximum: 600
        exclusiveMaximum: true
      detail:
        type: string
      instance:
        type: string
        format: uri
      violations:
        type: array
        items:
          $ref: "#/definitions/Violation"

  RequestDetails:
    type: object
    properties:
      id:
        type: string
        format: uuid
      requestId:
        type: string
        format: uuid
      placeFrom:
        $ref: "#/definitions/Place"
      placeTo:
        $ref: "#/definitions/Place"
      type:
        $ref: "#/definitions/RequestDetailsType"
      status:
        $ref: "#/definitions/RequestDetailsStatus"

  Request:
    type: object
    properties:
      id:
        type: string
        format: uuid
      closedBy:
        $ref: "#/definitions/User"
      requester:
        $ref: "#/definitions/User"
      requestsDetails:
        type: array
        items:
          $ref: "#/definitions/RequestDetails"
      status:
        $ref: "#/definitions/RequestStatus"


  Room:
    type: object
    properties:
      id:
        type: string
        format: uuid
      manager:
        $ref: "#/definitions/Employee"
      officeId:
        type: string
        format: uuid
      floorNumber:
        type: integer
        format: int32
      number:
        type: string
      freePlaces:
        type: integer
        format: int64
      totalPlaces:
        type: integer
        format: int32
      visioPageId:
        type: integer
        format: int32
      svgLink:
        type: string
      places:
        type: array
        items:
          $ref: "#/definitions/Place"

  Token:
    type: object
    properties:
      issuedAt:
        type: integer
        format: int64
      accessToken:
        type: string
      accessExpiresIn:
        type: integer
        format: int64
      refreshToken:
        type: string
      refreshExpiresIn:
        type: integer
        format: int64

  Violation:
    type: object
    properties:
      field:
        type: string
      message:
        type: string

  User:
    type: object
    properties:
      id:
        type: string
        format: uuid
      smgId:
        type: string
      imageLink:
        type: string
      department:
        type: string
      group:
        type: string
      name:
        type: string
      localizedName:
        type: string
      location:
        type: string
      role:
        type: string

  PlaceStatus:
    type: string
    enum:
      - "AVAILABLE"
      - "REQUESTED"

  RequestDetailsType:
    type: string
    enum:
      - "MOVE"
      - "REMOVE"
      - "PLACE"

  RequestStatus:
    type: string
    enum:
      - "COMPLETED"
      - "APPROVED"
      - "REJECTED"
      - "PENDING"

  RequestDetailsStatus:
    type: string
    enum:
      - "APPROVED"
      - "REJECTED"
      - "PENDING"

  UserRole:
    type: string
    enum:
      - "ADMIN"
      - "USER"

parameters:

  UuidPathParameter:
    in: path
    name: id
    type: string
    format: uuid
    required: true

securityDefinitions:
  userAuth:
    in: header
    type: apiKey
    name: Authorization