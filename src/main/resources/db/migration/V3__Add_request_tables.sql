CREATE TYPE REQUEST_STATUS AS ENUM ('COMPLETED', 'PENDING', 'REJECTED', 'APPROVED' );
CREATE TYPE REQUEST_DETAILS_TYPE AS ENUM ('REMOVE', 'MOVE', 'PLACE' );
CREATE TYPE REQUEST_DETAILS_STATUS AS ENUM ('PENDING', 'REJECTED', 'APPROVED' );

CREATE TABLE request
(
    id           UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT request_pkey PRIMARY KEY,
    closed_by_id UUID
        CONSTRAINT request_closed_by_id_fkey REFERENCES workplace_user (id),
    requester_id UUID                                                NOT NULL
        CONSTRAINT request_requester_id_fkey REFERENCES workplace_user (id),
    status       REQUEST_STATUS                                      NOT NULL,
    created_at   TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at   TIMESTAMP WITH TIME ZONE,
    deleted_at   TIMESTAMP WITH TIME ZONE
);

CREATE TABLE request_details
(
    id            UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT request_details_pkey PRIMARY KEY,
    request_id    UUID                                                NOT NULL
        CONSTRAINT request_details_request_id_fkey REFERENCES request (id) ON DELETE CASCADE,
    place_from_id UUID
        CONSTRAINT request_place_from_id_fkey REFERENCES place (id) ON DELETE CASCADE,
    place_to_id   UUID
        CONSTRAINT request_place_to_id_fkey REFERENCES place (id) ON DELETE CASCADE,
    type          REQUEST_DETAILS_TYPE                                NOT NULL,
    status        REQUEST_DETAILS_STATUS                              NOT NULL,
    created_at    TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE,
    deleted_at    TIMESTAMP WITH TIME ZONE,
    CHECK ( type = 'REMOVE' AND place_from_id IS NOT NULL AND place_to_id IS NULL
        OR type = 'MOVE' AND place_from_id IS NOT NULL AND place_to_id IS NOT NULL
        OR type = 'PLACE' AND place_from_id IS NULL AND place_to_id IS NOT NULL)
);

CREATE TABLE request_approve
(
    id                 UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT request_approve_pkey PRIMARY KEY,
    request_details_id UUID                                                NOT NULL
        CONSTRAINT request_approve_request_details_id_fkey REFERENCES request_details (id) ON DELETE CASCADE,
    approver_id        UUID                                                NOT NULL
        CONSTRAINT request_approve_approver_id_fkey REFERENCES workplace_user (id),
    status             REQUEST_DETAILS_STATUS                              NOT NULL,
    created_at         TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at         TIMESTAMP WITH TIME ZONE,
    deleted_at         TIMESTAMP WITH TIME ZONE
);