CREATE TYPE PLACE_STATUS AS ENUM ('REQUESTED', 'AVAILABLE' );

CREATE TABLE office
(
    id         UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT office_pkey PRIMARY KEY,
    location   TEXT UNIQUE                                         NOT NULL,
    address    TEXT,
    image_link TEXT,
    visio_link TEXT                                                NOT NULL,
    latitude   DOUBLE PRECISION,
    longitude  DOUBLE PRECISION,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE room
(
    id            UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT room_pkey PRIMARY KEY,
    office_id     UUID                                                NOT NULL
        CONSTRAINT room_office_id_fkey REFERENCES office (id) ON DELETE CASCADE,
    manager_id    UUID                                                NOT NULL
        CONSTRAINT room_manager_id_fkey REFERENCES employee (id),
    floor_number  INTEGER                                             NOT NULL,
    number        TEXT                                                NOT NULL,
    total_places  INTEGER                                             NOT NULL,
    visio_page_id INTEGER                                             NOT NULL,
    svg_link      TEXT,
    created_at    TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE,
    deleted_at    TIMESTAMP WITH TIME ZONE
);

CREATE TABLE place
(
    id            UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT place_pkey PRIMARY KEY,
    employee_id   UUID UNIQUE
        CONSTRAINT place_employee_id_fkey REFERENCES employee (id),
    room_id       UUID                                                NOT NULL
        CONSTRAINT place_room_id_fkey REFERENCES room (id) ON DELETE CASCADE,
    number        INTEGER                                             NOT NULL,
    visio_card_id BIGINT                                              NOT NULL,
    status        PLACE_STATUS                                        NOT NULL,
    created_at    TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE,
    deleted_at    TIMESTAMP WITH TIME ZONE
);

CREATE VIEW room_view AS
SELECT room.*,
       (SELECT count(*) FROM place WHERE room.id = place.room_id AND employee_id IS NULL) AS free_places
FROM room;
