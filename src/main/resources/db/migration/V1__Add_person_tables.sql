CREATE TYPE USER_ROLE AS ENUM ('USER', 'ADMIN');

CREATE TABLE employee
(
    id               UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT employee_pkey PRIMARY KEY,
    name             TEXT UNIQUE                                         NOT NULL,
    localized_name   TEXT                                                NOT NULL,
    department       TEXT,
    department_group TEXT,
    location         TEXT,
    smg_id           TEXT                                                NOT NULL
        CONSTRAINT employee_smg_id_unique UNIQUE,
    image_link       TEXT                                                NOT NULL,
    created_at       TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at       TIMESTAMP WITH TIME ZONE,
    deleted_at       TIMESTAMP WITH TIME ZONE
);


CREATE TABLE workplace_user
(
    id                 UUID                     DEFAULT uuid_generate_v4() NOT NULL
        CONSTRAINT workplace_user_pkey PRIMARY KEY,
    username           TEXT                                                NOT NULL
        CONSTRAINT workplace_user_username_unique UNIQUE,
    encrypted_password TEXT                                                NOT NULL,
    display_name       TEXT UNIQUE                                         NOT NULL,
    role               USER_ROLE                                           NOT NULL,
    created_at         TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
    updated_at         TIMESTAMP WITH TIME ZONE,
    deleted_at         TIMESTAMP WITH TIME ZONE
);
