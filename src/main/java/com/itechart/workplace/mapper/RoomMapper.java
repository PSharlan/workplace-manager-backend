package com.itechart.workplace.mapper;

import com.itechart.workplace.entity.OfficeEntity;
import com.itechart.workplace.entity.RoomEntity;
import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.generated.model.Room;
import com.itechart.workplace.vsdx.model.ParsedRoom;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(builder = @Builder(disableBuilder = true), uses = {OfficeMapper.class, EmployeeMapper.class, PlaceMapper.class})
public interface RoomMapper {

    RoomMapper ROOM_MAPPER = getMapper(RoomMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "places", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "freePlaces", ignore = true)
    @Mapping(target = "office", source = "office")
    @Mapping(target = "manager", source = "manager")
    @Mapping(target = "svgLink", source = "parsedRoom.svgGeneratedLink")
    @Mapping(target = "visioPageId", source = "parsedRoom.pageId")
    @Mapping(target = "number", source = "parsedRoom.roomInfoCard.roomNumber")
    @Mapping(target = "totalPlaces",
            expression = "java(parsedRoom.getTables() == null ? 0 : parsedRoom.getTables().size())")
    @Mapping(target = "floorNumber",
            expression = "java(Integer.parseInt(parsedRoom.getRoomInfoCard().getRoomNumber()) / 100)")
    RoomEntity toEntity(ParsedRoom parsedRoom, OfficeEntity office, Employee manager);

    @Mapping(target = "officeId", source = "entity.office.id")
    Room toModel(RoomEntity entity);

    @Mapping(target = "places", ignore = true)
    @Mapping(target = "officeId", source = "entity.office.id")
    Room toPreviewModel(RoomEntity entity);

}
