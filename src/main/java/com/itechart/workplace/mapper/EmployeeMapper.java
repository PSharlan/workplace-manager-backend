package com.itechart.workplace.mapper;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.generated.model.Employee;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Map;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(builder = @Builder(disableBuilder = true))
public interface EmployeeMapper {

    EmployeeMapper EMPLOYEE_MAPPER = getMapper(EmployeeMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "place", ignore = true)
    @Mapping(target = "imageLink", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "smgId", expression = "java(fieldMap.get(1))")
    @Mapping(target = "department", expression = "java(fieldMap.get(2))")
    @Mapping(target = "group", expression = "java(fieldMap.get(3))")
    @Mapping(target = "name", expression = "java(fieldMap.get(4))")
    @Mapping(target = "localizedName", expression = "java(fieldMap.get(5))")
    @Mapping(target = "location", expression = "java(fieldMap.get(6))")
    EmployeeEntity toEntity(Map<Integer, String> fieldMap);

    @Mapping(target = "place", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    EmployeeEntity toEntity(Employee model);

    Employee toModel(EmployeeEntity entity);

}
