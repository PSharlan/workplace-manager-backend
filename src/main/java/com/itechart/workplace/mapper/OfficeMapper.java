package com.itechart.workplace.mapper;

import com.itechart.workplace.entity.OfficeEntity;
import com.itechart.workplace.generated.model.Office;
import com.itechart.workplace.vsdx.model.ParsedOffice;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(builder = @Builder(disableBuilder = true), imports = java.util.List.class)
public interface OfficeMapper {

    OfficeMapper OFFICE_MAPPER = getMapper(OfficeMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "rooms", ignore = true)
    @Mapping(target = "address", ignore = true)
    @Mapping(target = "latitude", ignore = true)
    @Mapping(target = "longitude", ignore = true)
    @Mapping(target = "imageLink", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    OfficeEntity toEntity(ParsedOffice parsedOffice);

    @Mapping(target = "coordinates", expression = "java(List.of(entity.getLatitude(), entity.getLongitude()))")
    Office toModel(OfficeEntity entity);

}
