package com.itechart.workplace.mapper;

import com.itechart.workplace.client.model.SmgEmployeeProfile;
import com.itechart.workplace.entity.EmployeeEntity;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(builder = @Builder(disableBuilder = true))
public interface EmployeeProfileMapper {

    EmployeeProfileMapper EMPLOYEE_PROFILE_MAPPER = Mappers.getMapper(EmployeeProfileMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "department", expression = "java( profile.getDepartmentCode() )")
    @Mapping(target = "group", ignore = true)
    @Mapping(source = "profileId", target = "smgId")
    @Mapping(target = "name",
            expression = "java( String.format(\"%s %s\", profile.getLastNameEng(), profile.getFirstNameEng()) )")
    @Mapping(target = "localizedName",
            expression = "java( String.format(\"%s %s\", profile.getLastName(), profile.getFirstName()) )")
    @Mapping(source = "image", target = "imageLink")
    @Mapping(target = "place", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    EmployeeEntity toEntity(SmgEmployeeProfile profile);

}
