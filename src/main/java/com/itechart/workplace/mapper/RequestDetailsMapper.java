package com.itechart.workplace.mapper;

import com.itechart.workplace.entity.RequestDetailsEntity;
import com.itechart.workplace.entity.enums.RequestDetailsStatusEntity;
import com.itechart.workplace.entity.enums.RequestDetailsTypeEntity;
import com.itechart.workplace.entity.enums.RequestStatusEntity;
import com.itechart.workplace.generated.model.RequestDetails;
import com.itechart.workplace.generated.model.RequestDetailsStatus;
import com.itechart.workplace.generated.model.RequestDetailsType;
import com.itechart.workplace.generated.model.RequestStatus;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(builder = @Builder(disableBuilder = true), uses = PlaceMapper.class)
public interface RequestDetailsMapper {

    RequestDetailsMapper REQUEST_DETAILS_MAPPER = getMapper(RequestDetailsMapper.class);

    RequestDetailsTypeEntity toEntity(RequestDetailsType model);

    RequestDetailsStatusEntity toEntity(RequestDetailsStatus model);

    @Mapping(target = "requestId", source = "entity.request.id")
    RequestDetails toModel(RequestDetailsEntity entity);

    RequestDetailsType toModel(RequestDetailsTypeEntity entity);

    RequestDetailsStatus toModel(RequestDetailsStatusEntity entity);

    RequestStatus toModel(RequestStatusEntity entity);

}
