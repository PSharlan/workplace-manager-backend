package com.itechart.workplace.mapper;

import com.itechart.workplace.entity.UserEntity;
import com.itechart.workplace.entity.enums.UserRoleEntity;
import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.generated.model.User;
import com.itechart.workplace.generated.model.UserRole;
import com.itechart.workplace.security.impl.UserDetailsImpl;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.UUID;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(builder = @Builder(disableBuilder = true))
public interface UserMapper {

    UserMapper USER_MAPPER = getMapper(UserMapper.class);

    @Mapping(target = "authorities", ignore = true)
    UserDetailsImpl toUserDetails(UUID userId, UserRoleEntity role, String displayName, String smgWebAuth);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    UserEntity toUserEntity(String username,
                            String encryptedPassword,
                            String displayName,
                            UserRoleEntity role);

    @Mapping(target = "id", source = "userId")
    @Mapping(target = "role", source = "role")
    @Mapping(target = "smgId", source = "employee.smgId")
    @Mapping(target = "imageLink", source = "employee.imageLink")
    @Mapping(target = "department", source = "employee.department")
    @Mapping(target = "group", source = "employee.group")
    @Mapping(target = "name", source = "employee.name")
    @Mapping(target = "localizedName", source = "employee.localizedName")
    @Mapping(target = "location", source = "employee.location")
    User toModel(UUID userId, UserRoleEntity role, Employee employee);

    UserRole toModel(UserRoleEntity entity);

}
