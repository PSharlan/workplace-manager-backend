package com.itechart.workplace.mapper;

import com.itechart.workplace.entity.RequestEntity;
import com.itechart.workplace.entity.enums.RequestStatusEntity;
import com.itechart.workplace.generated.model.Request;
import com.itechart.workplace.generated.model.RequestStatus;
import com.itechart.workplace.generated.model.User;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(builder = @Builder(disableBuilder = true), uses = {RequestDetailsMapper.class, EmployeeMapper.class})
public interface RequestMapper {

    RequestMapper REQUEST_MAPPER = getMapper(RequestMapper.class);

    @Mapping(target = "id", source = "entity.id")
    @Mapping(target = "requester", source = "requester")
    @Mapping(target = "closedBy", source = "closedBy")
    Request toModel(RequestEntity entity, User requester, User closedBy);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "requestsDetails", ignore = true)
    @Mapping(target = "status", source = "status")
    @Mapping(target = "closedBy.id", source = "closedBy.id")
    @Mapping(target = "requester.id", source = "requester.id")
    RequestEntity toEntity(User requester, User closedBy, RequestStatus status);

    RequestStatusEntity toEntity(RequestStatus model);

}
