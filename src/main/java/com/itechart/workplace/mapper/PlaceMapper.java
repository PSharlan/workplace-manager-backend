package com.itechart.workplace.mapper;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.entity.PlaceEntity;
import com.itechart.workplace.entity.RoomEntity;
import com.itechart.workplace.entity.enums.PlaceStatusEntity;
import com.itechart.workplace.generated.model.Place;
import com.itechart.workplace.vsdx.model.EmployeeCard;
import com.itechart.workplace.vsdx.model.Table;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(builder = @Builder(disableBuilder = true), uses = RoomMapper.class)
public interface PlaceMapper {

    PlaceMapper PLACE_MAPPER = getMapper(PlaceMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "room", source = "room")
    @Mapping(target = "status", source = "status")
    @Mapping(target = "employee", source = "employee")
    @Mapping(target = "visioCardId", source = "card.shapeId")
    @Mapping(target = "number", source = "table.number")
    PlaceEntity toEntity(RoomEntity room, EmployeeEntity employee, EmployeeCard card, Table table,
                         PlaceStatusEntity status);

    @Mapping(target = "roomId", source = "entity.room.id")
    Place toModel(PlaceEntity entity);

}
