package com.itechart.workplace.security;

import com.itechart.workplace.entity.enums.UserRoleEntity;
import com.itechart.workplace.generated.model.Token;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.UUID;

public interface TokenProvider {

    String getSmgWebAuthByToken(String token);

    Token createToken(UserDetails userDetails);

    Token refreshToken(String refreshToken, String smgWebAuth);

    UserDetails buildUserDetailsByToken(String token);

    UserDetails buildUserDetailsByToken(String token, String smgWebAuth);

    UUID getUserIdByToken(String token);

    UserRoleEntity getUserRoleByToken(String token);

}
