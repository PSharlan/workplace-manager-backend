package com.itechart.workplace.security.impl;

import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.entity.enums.UserRoleEntity;
import com.itechart.workplace.generated.model.Token;
import com.itechart.workplace.security.TokenProvider;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.util.Date;
import java.util.UUID;

import static com.itechart.workplace.entity.enums.UserRoleEntity.fromValue;
import static com.itechart.workplace.mapper.UserMapper.USER_MAPPER;
import static io.jsonwebtoken.io.Decoders.BASE64;

@Slf4j
@Component
@RequiredArgsConstructor
public class TokenProviderImpl implements TokenProvider {

    private static final int MILLIS_IN_SECOND = 1000;

    private static final String DISPLAY_NAME_KEY = "dpn";
    private static final String SMG_KEY = "smg";
    private static final String TYPE_KEY = "typ";
    private static final String ROLE_KEY = "rol";

    private static final String ACCESS_TOKEN = "access";
    private static final String REFRESH_TOKEN = "refresh";

    private final AppConfiguration appConfiguration;

    private Key key;

    private long accessTokenValidityMillis;
    private long refreshTokenValidityMillis;

    @PostConstruct
    public void init() {
        this.key = Keys.hmacShaKeyFor(BASE64.decode(appConfiguration.getJwt().getBase64Secret()));
        accessTokenValidityMillis = appConfiguration.getJwt().getAccessTokenValidity() * MILLIS_IN_SECOND;
        refreshTokenValidityMillis = appConfiguration.getJwt().getRefreshTokenValidity() * MILLIS_IN_SECOND;
    }

    @Override
    public Token createToken(final UserDetails userDetails) {
        var currentTimeMillis = System.currentTimeMillis();
        var issuedAt = new Date(currentTimeMillis);
        var accessExpiresIn = new Date(currentTimeMillis + accessTokenValidityMillis);
        var refreshExpiresIn = new Date(currentTimeMillis + refreshTokenValidityMillis);
        var userDetailsImpl = (UserDetailsImpl) userDetails;

        var accessToken = createToken(userDetailsImpl, issuedAt, accessExpiresIn, ACCESS_TOKEN);
        var refreshToken = createToken(userDetailsImpl, issuedAt, refreshExpiresIn, REFRESH_TOKEN);
        return new Token()
                .issuedAt(issuedAt.getTime())
                .accessToken(accessToken)
                .accessExpiresIn(accessExpiresIn.getTime())
                .refreshToken(refreshToken)
                .refreshExpiresIn(refreshExpiresIn.getTime());
    }

    private String createToken(final UserDetailsImpl userDetails,
                               final Date issuedAt,
                               final Date expiresIn,
                               final String tokenType) {
        return Jwts.builder()
                .setSubject(userDetails.getUserId().toString())
                .setIssuedAt(issuedAt)
                .setExpiration(expiresIn)
                .claim(TYPE_KEY, tokenType)
                .claim(DISPLAY_NAME_KEY, userDetails.getDisplayName())
                .claim(ROLE_KEY, userDetails.getRole().toString())
                .claim(SMG_KEY, userDetails.getSmgWebAuth())
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    @Override
    public Token refreshToken(final String refreshToken, final String smgWebAuth) {
        var userDetails = buildUserDetailsByToken(refreshToken, smgWebAuth);
        return createToken(userDetails);
    }

    @Override
    public UserDetails buildUserDetailsByToken(final String token) {
        var smgWebAuth = getSmgWebAuthByToken(token);
        var role = getUserRoleByToken(token);
        var userId = getUserIdByToken(token);
        var displayName = getDisplayNameByToken(token);
        return USER_MAPPER.toUserDetails(userId, role, displayName, smgWebAuth);
    }

    @Override
    public UserDetails buildUserDetailsByToken(final String token, final String smgWebAuth) {
        var role = getUserRoleByToken(token);
        var userId = getUserIdByToken(token);
        var displayName = getDisplayNameByToken(token);
        return USER_MAPPER.toUserDetails(userId, role, displayName, smgWebAuth);
    }

    @Override
    public String getSmgWebAuthByToken(final String token) {
        return (String) Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().get(SMG_KEY);
    }

    @Override
    public UUID getUserIdByToken(final String token) {
        return UUID.fromString(Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject());
    }

    @Override
    public UserRoleEntity getUserRoleByToken(final String token) {
        return fromValue((String) Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().get(ROLE_KEY));
    }

    private String getDisplayNameByToken(final String token) {
        return (String) Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().get(DISPLAY_NAME_KEY);
    }

}
