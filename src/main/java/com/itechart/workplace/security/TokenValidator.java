package com.itechart.workplace.security;

public interface TokenValidator {

    boolean validateToken(String token);

}
