package com.itechart.workplace.vsdx.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@SuppressWarnings({"MemberName"})
public class CoordinatesDot {

    private double x;
    private double y;

}
