package com.itechart.workplace.vsdx.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoordinatesSvg {

    private double height;
    private double width;

    private double angle;

    private double tlX;
    private double tlY;

    @Override
    public String toString() {
        return "SVG Coordinates [ "
                + "angle: " + angle
                + " | tlX: " + tlX + " tlY: " + tlY
                + " | width: " + width + " height: " + height + " ]";
    }

}
