package com.itechart.workplace.vsdx.model;

import com.itechart.workplace.vsdx.model.enums.TableType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

import static java.util.UUID.randomUUID;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Table {

    private final UUID id;
    private Long shapeId;
    private Long parentId;
    private Integer number;
    private Coordinates coordinates;
    private TableType type;

    public Table() {
        id = randomUUID();
    }

    @Override
    public String toString() {
        return "Table [shape id: " + shapeId + " | number: " + number + "]";
    }

}
