package com.itechart.workplace.vsdx.model;

import lombok.Getter;

import java.util.List;

import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.toList;

@Getter
public final class CoordinatesAbsoluteDots {

    private CoordinatesDot tlDot;
    private CoordinatesDot trDot;
    private CoordinatesDot brDot;
    private CoordinatesDot blDot;

    private CoordinatesAbsoluteDots(final CoordinatesDot tlDot, final CoordinatesDot trDot,
                                    final CoordinatesDot brDot, final CoordinatesDot blDot) {
        this.tlDot = tlDot;
        this.trDot = trDot;
        this.brDot = brDot;
        this.blDot = blDot;
    }

    public static CoordinatesAbsoluteDots createFromDots(final List<CoordinatesDot> dots) {
        if (dots.size() != 4) {
            throw new IllegalArgumentException("4 dots must be specified");
        }

        var topDots = dots.stream().sorted(comparingDouble(CoordinatesDot::getY).reversed())
                .limit(2).collect(toList());
        var trDot = topDots.stream().max(comparingDouble(CoordinatesDot::getX)).orElse(topDots.get(0));
        var tlDot = topDots.stream().min(comparingDouble(CoordinatesDot::getX)).orElse(topDots.get(1));

        var bottomDots = dots.stream().sorted(comparingDouble(CoordinatesDot::getY))
                .limit(2).collect(toList());
        var brDot = bottomDots.stream().max(comparingDouble(CoordinatesDot::getX)).orElse(bottomDots.get(0));
        var blDot = bottomDots.stream().min(comparingDouble(CoordinatesDot::getX)).orElse(bottomDots.get(1));

        return new CoordinatesAbsoluteDots(tlDot, trDot, brDot, blDot);
    }

    @Override
    public String toString() {
        return "Absolute dots: [ "
                + "tl: (x: " + tlDot.getX() + " - y: " + tlDot.getY() + ") "
                + "tr: (x: " + trDot.getX() + " - y: " + trDot.getY() + ") "
                + "br: (x: " + brDot.getX() + " - y: " + brDot.getY() + ") "
                + "bl: (x: " + blDot.getX() + " - y: " + blDot.getY() + ") ]";
    }
}
