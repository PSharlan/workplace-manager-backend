package com.itechart.workplace.vsdx.model;

import lombok.Getter;

@Getter
public class BackgroundInfo {

    private double backgroundWidth;
    private double backgroundHeight;
    private double outerWidth;
    private double outerHeight;
    private double outerLeftMargin;
    private double outerBottomMargin;

    public BackgroundInfo(final double backgroundWidth, final double backgroundHeight,
                   final double outerWidth, final double outerHeight,
                   final double outerLeftMargin, final double outerBottomMargin) {
        this.backgroundWidth = backgroundWidth;
        this.backgroundHeight = backgroundHeight;
        this.outerWidth = outerWidth;
        this.outerHeight = outerHeight;
        this.outerLeftMargin = outerLeftMargin;
        this.outerBottomMargin = outerBottomMargin;
    }

}
