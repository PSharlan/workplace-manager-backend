package com.itechart.workplace.vsdx.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

import static java.util.UUID.randomUUID;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ParsedOffice {

    private final UUID id;
    private String location;
    private String visioLink;
    private List<ParsedRoom> parsedRooms;

    public ParsedOffice() {
        id = randomUUID();
    }

}
