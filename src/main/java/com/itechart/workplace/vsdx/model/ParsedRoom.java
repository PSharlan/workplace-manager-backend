package com.itechart.workplace.vsdx.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.UUID.randomUUID;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ParsedRoom {

    private final UUID id;
    private Integer pageId;
    private RoomInfoCard roomInfoCard;
    private Integer freeSeats;
    private String svgGeneratedLink;
    private String svgSourceLink;
    private List<Table> tables;
    private List<EmployeeCard> employeeCards;
    private Map<Integer, UUID> tableCardRelations;

    public ParsedRoom() {
        id = randomUUID();
    }

    @Override
    public String toString() {
        return "ParsedRoom [name: " + roomInfoCard.getRoomNumber()
                + " | responsible: " + roomInfoCard.getResponsibleName()
                + " | free seats: " + freeSeats
                + " | total places: " + tables.size()
                + " ]";
    }

}
