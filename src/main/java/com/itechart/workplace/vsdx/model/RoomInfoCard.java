package com.itechart.workplace.vsdx.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomInfoCard {

    private String responsibleName;
    private String roomNumber;
    private Long shapeId;

}
