package com.itechart.workplace.vsdx.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoordinatesOuterDots {

    private double tlX;
    private double tlY;

    private double trX;
    private double trY;

    private double brX;
    private double brY;

    private double blX;
    private double blY;

    @Override
    public String toString() {
        return "Outer dots: [ "
                + "tl: (x: " + tlX + " - y: " + tlY + ") "
                + "tr: (x: " + trX + " - y: " + trY + ") "
                + "br: (x: " + brX + " - y: " + brY + ") "
                + "bl: (x: " + blX + " - y: " + blY + ") ]";
    }
}
