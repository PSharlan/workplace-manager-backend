package com.itechart.workplace.vsdx.model;

import lombok.Getter;

@Getter
public class TableNumber {

    private int number;
    private long parentId;
    private double centerX;
    private double centerY;

    public TableNumber(final int number, final long parentId, final double centerX, final double centerY) {
        this.number = number;
        this.parentId = parentId;
        this.centerX = centerX;
        this.centerY = centerY;
    }

    @Override
    public String toString() {
        return "Table number: " + number + " parentId: " + parentId;
    }

}
