package com.itechart.workplace.vsdx.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

import static java.util.UUID.randomUUID;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class EmployeeCard {

    private final UUID id;
    private Coordinates coordinates;
    private String employeeDepartment;
    private String employeeName;
    private Long shapeId;
    private Table table;

    public EmployeeCard() {
        id = randomUUID();
    }

    @Override
    public String toString() {
        return "EmployeeCard [shape id: " + shapeId
                + " | employee name: " + employeeName
                + " department: " + employeeDepartment
                + "]";
    }

}
