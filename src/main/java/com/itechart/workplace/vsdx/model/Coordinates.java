package com.itechart.workplace.vsdx.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Coordinates {

    private CoordinatesAbsoluteDots coordinatesAbsoluteDots;
    private CoordinatesOuterDots coordinatesOuterDots;
    private CoordinatesSvg coordinatesSvg;

    private double height;
    private double width;

    private double angle;

    private double centerX;
    private double centerY;

    @Override
    public String toString() {
        return "Coordinates [ "
                + "angle: (" + angle + ")"
                + "center: (x: " + centerX + " - y: " + centerY + ") "
                + coordinatesAbsoluteDots;
    }
}
