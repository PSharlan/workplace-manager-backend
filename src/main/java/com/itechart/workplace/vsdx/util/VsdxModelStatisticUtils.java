package com.itechart.workplace.vsdx.util;

import com.itechart.workplace.exception.enums.ParseWarningType;
import com.itechart.workplace.generated.model.Violation;
import com.itechart.workplace.vsdx.model.BackgroundInfo;
import com.itechart.workplace.vsdx.model.Coordinates;
import com.itechart.workplace.vsdx.model.CoordinatesSvg;
import com.itechart.workplace.vsdx.model.EmployeeCard;
import com.itechart.workplace.vsdx.model.ParsedOffice;
import com.itechart.workplace.vsdx.model.ParsedRoom;
import com.itechart.workplace.vsdx.model.Table;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.itechart.workplace.exception.enums.ParseWarningType.INVALID_ATTRIBUTE;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.cardOnTable;
import static java.lang.Math.PI;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Slf4j
@UtilityClass
@SuppressWarnings("PMD")
public class VsdxModelStatisticUtils {

    public static List<Violation> analyzeParsedData(final ParsedOffice parsedOffice) {
        final List<Violation> violations = new ArrayList<>();
        var rooms = parsedOffice.getParsedRooms();
        for (ParsedRoom room : rooms) {
            violations.addAll(printRoomStatistics(room));
        }
        return violations;
    }

    public static List<Violation> analyzeParsedData(final ParsedRoom parsedRoom) {
        return printRoomStatistics(parsedRoom);
    }

    private static List<Violation> printRoomStatistics(final ParsedRoom parsedRoom) {
        boolean isConversationRoom = parsedRoom.getRoomInfoCard().getRoomNumber().endsWith("2");
        boolean isSixthRoom = parsedRoom.getRoomInfoCard().getRoomNumber().endsWith("6");
        final List<Violation> violations = new ArrayList<>();
        var roomNumber = parsedRoom.getRoomInfoCard().getRoomNumber();

        log.info(parsedRoom.toString());
        if (parsedRoom.getTables() == null) {
            var message = format("Places were parsed incorrect in room %s", roomNumber);
            violations.add(createViolation(INVALID_ATTRIBUTE, message));
        } else {
            var tablesWithoutNumber = parsedRoom.getTables().stream().filter(t -> t.getNumber() == null)
                    .collect(toList());
            if (!tablesWithoutNumber.isEmpty() && !isConversationRoom
                    && !(isSixthRoom && tablesWithoutNumber.size() == 2)) {
                var message = format("There are %s tables without number in room %s",
                        tablesWithoutNumber.size(), roomNumber);
                violations.add(createViolation(INVALID_ATTRIBUTE, message));
            }

            int totalPlaces = parsedRoom.getTables().size();
            int totalCards = parsedRoom.getEmployeeCards().size();
            if (totalPlaces != totalCards && !isConversationRoom
                    && !(isSixthRoom && totalCards == (totalPlaces - tablesWithoutNumber.size()))) {
                var message = format("Total places and cards are not equals! Tables: %s | Cards: %s. Room %s",
                        totalPlaces, totalCards, roomNumber);
                violations.add(createViolation(INVALID_ATTRIBUTE, message));
            }

            var responsible = parsedRoom.getRoomInfoCard().getResponsibleName();
            if (responsible.trim().isEmpty() && !isConversationRoom) {
                var message = format("Responsible name is not defined in room %s", roomNumber);
                violations.add(createViolation(INVALID_ATTRIBUTE, message));
            }

            var freeSeats = parsedRoom.getFreeSeats();
            if (freeSeats == null) {
                var message = format("Free seats are not defined in room %s", roomNumber);
                violations.add(createViolation(INVALID_ATTRIBUTE, message));
            }

            var cardsWithoutEmployee = parsedRoom.getEmployeeCards().stream().filter(c -> c.getEmployeeName().isEmpty())
                    .collect(toList());
            if (freeSeats != null && cardsWithoutEmployee.size() != freeSeats && !isConversationRoom && !(isSixthRoom
                    && cardsWithoutEmployee.size() == (freeSeats + tablesWithoutNumber.size()))) {
                var message = format("%s cards without name but free seats number is %s in room %s",
                        cardsWithoutEmployee.size(), freeSeats, roomNumber);
                violations.add(createViolation(INVALID_ATTRIBUTE, message));
            }
        }

        return violations;
    }

    public static Violation analyzeTableCardRelations(final Table table, final List<EmployeeCard> cards,
                                                      final String roomNumber) {
        var newCandidates = new LinkedList<EmployeeCard>();
        cards.forEach(card -> {
            if (cardOnTable(card, table)) {
                newCandidates.add(card);
            }
        });

        final String message;
        if (newCandidates.isEmpty()) {
            message = format("There is no card for table with %s number in room %s",
                    table.getNumber() == 0 ? "parsed incorrectly" : table.getNumber(), roomNumber);
        } else {
            message = format("Card is already related to other table in room %s", roomNumber);
        }
        return createViolation(INVALID_ATTRIBUTE, message);
    }

    public static CoordinatesSvg createCoordinatesDefaultSvg(final Coordinates coordinates,
                                                       final BackgroundInfo backgroundInfo) {
        var centerX = coordinates.getCenterX();
        var centerY = coordinates.getCenterY();
        var angle = coordinates.getAngle();
        var width = coordinates.getWidth();
        var height = coordinates.getHeight();

        //coordinates for SVG from top left corner
        //angle for SVG in degrees
        //coordinates and sizes for SVG are relative
        var coordinatesSvg = new CoordinatesSvg();
        coordinatesSvg.setWidth(width / backgroundInfo.getBackgroundWidth());
        coordinatesSvg.setHeight(height / backgroundInfo.getBackgroundHeight());
        coordinatesSvg.setTlX((centerX - width / 2) / backgroundInfo.getBackgroundWidth());
        coordinatesSvg.setTlY(1 - (centerY + height / 2) / backgroundInfo.getBackgroundHeight());

        var radian = 180 / PI;
        coordinatesSvg.setAngle((360 + angle * radian) % 360);
        return coordinatesSvg;
    }

    public static Violation createViolation(final ParseWarningType warningType, final String message) {
        var violation = new Violation();
        violation.setField(warningType.toString());
        violation.setMessage(message);
        return violation;
    }

}
