package com.itechart.workplace.vsdx.util;

import com.aspose.diagram.Shape;
import com.itechart.workplace.vsdx.model.BackgroundInfo;
import com.itechart.workplace.vsdx.model.Coordinates;
import com.itechart.workplace.vsdx.model.CoordinatesAbsoluteDots;
import com.itechart.workplace.vsdx.model.CoordinatesDot;
import com.itechart.workplace.vsdx.model.CoordinatesOuterDots;
import lombok.experimental.UtilityClass;

import java.awt.geom.AffineTransform;

import static com.itechart.workplace.vsdx.model.CoordinatesAbsoluteDots.createFromDots;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isBottom;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isLeft;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isRight;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isTop;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.cropAngle;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.util.Arrays.asList;

@UtilityClass
@SuppressWarnings({"ReturnCount", "PMD", "LocalVariableName"})
public class VsdxCoordinatesUtils {

    public static Coordinates findCoordinates(final Shape shape) {
        double centerX;
        double centerY;
        var shapeWidth = shape.getXForm().getWidth().getValue();
        var shapeHeight = shape.getXForm().getHeight().getValue();

        var absoluteAngle = shape.getXForm().getAngle().getValue();
        var parentShape = shape.getParentShape();

        var childValueX = shape.getXForm().getPinX().getValue();
        var childValueY = shape.getXForm().getPinY().getValue();

        if (parentShape == null) {
            return createCoordinates(childValueX, childValueY, absoluteAngle, shapeWidth, shapeHeight);
        }

        do {
            var parentAngle = parentShape.getXForm().getAngle().getValue();
            absoluteAngle += parentAngle;

            var m = new AffineTransform();
            m.rotate((float) parentAngle);
            m.translate(childValueX, childValueY);

            var childRelPinX = m.getTranslateX();
            var childRelPinY = m.getTranslateY();

            var parentAbsCenterX = parentShape.getXForm().getPinX().getValue();
            var parentAbsCenterY = parentShape.getXForm().getPinY().getValue();
            var parentRelCenterX = parentShape.getXForm().getLocPinX().getValue();
            var parentRelCenterY = parentShape.getXForm().getLocPinY().getValue();

            if (isTop(parentAngle)) {
                centerX = parentAbsCenterX - parentRelCenterX + childRelPinX;
                centerY = parentAbsCenterY - parentRelCenterY + childRelPinY;
            } else if (isBottom(parentAngle)) {
                centerX = parentAbsCenterX + parentRelCenterX + childRelPinX;
                centerY = parentAbsCenterY + parentRelCenterY + childRelPinY;
            } else if (isLeft(parentAngle)) {
                centerX = parentAbsCenterX + parentRelCenterY + childRelPinX;
                centerY = parentAbsCenterY - parentRelCenterX + childRelPinY;
            } else if (isRight(parentAngle)) {
                centerX = parentAbsCenterX - parentRelCenterY + childRelPinX;
                centerY = parentAbsCenterY + parentRelCenterX + childRelPinY;
            } else { // if angle is not 0 || 90 || 180 || 270
                return createCoordinatesRotated(shape);
            }

            parentShape = parentShape.getParentShape();
            childValueX = centerX;
            childValueY = centerY;
        } while (parentShape != null);

        return createCoordinates(centerX, centerY, absoluteAngle, shapeWidth, shapeHeight);
    }

    public static CoordinatesOuterDots createOuterDots(final CoordinatesAbsoluteDots dots,
                                                       final BackgroundInfo backgroundInfo) {
        var outerDots = new CoordinatesOuterDots();
        outerDots.setTlX(dots.getTlDot().getX() / backgroundInfo.getBackgroundWidth()
                * backgroundInfo.getOuterWidth() + backgroundInfo.getOuterLeftMargin());
        outerDots.setTlY(dots.getTlDot().getY() / backgroundInfo.getBackgroundHeight()
                * backgroundInfo.getOuterHeight() + backgroundInfo.getOuterBottomMargin());
        outerDots.setTrX(dots.getTrDot().getX() / backgroundInfo.getBackgroundWidth()
                * backgroundInfo.getOuterWidth() + backgroundInfo.getOuterLeftMargin());
        outerDots.setTrY(dots.getTrDot().getY() / backgroundInfo.getBackgroundHeight()
                * backgroundInfo.getOuterHeight() + backgroundInfo.getOuterBottomMargin());
        outerDots.setBrX(dots.getBrDot().getX() / backgroundInfo.getBackgroundWidth()
                * backgroundInfo.getOuterWidth() + backgroundInfo.getOuterLeftMargin());
        outerDots.setBrY(dots.getBrDot().getY() / backgroundInfo.getBackgroundHeight()
                * backgroundInfo.getOuterHeight() + backgroundInfo.getOuterBottomMargin());
        outerDots.setBlX(dots.getBlDot().getX() / backgroundInfo.getBackgroundWidth()
                * backgroundInfo.getOuterWidth() + backgroundInfo.getOuterLeftMargin());
        outerDots.setBlY(dots.getBlDot().getY() / backgroundInfo.getBackgroundHeight()
                * backgroundInfo.getOuterHeight() + backgroundInfo.getOuterBottomMargin());
        return outerDots;
    }

    //It works only for parent of this shape or for current shape if it haven't parents.
    //It won't work for sub shapes
    public static Coordinates createCoordinatesRotated(final Shape shape) {
        var parentShape = shape.getParentShape();
        if (parentShape == null) {
            parentShape = shape;
        }

        var parentAbsX = parentShape.getXForm().getPinX().getValue();
        var parentAbsY = parentShape.getXForm().getPinY().getValue();
        var parentWidth = parentShape.getXForm().getWidth().getValue();
        var parentHeight = parentShape.getXForm().getHeight().getValue();
        var parentAngle = parentShape.getXForm().getAngle().getValue();
        var dots = createAbsoluteDots(parentAbsX, parentAbsY, parentAngle, parentWidth, parentHeight);

        var coordinates = new Coordinates();
        coordinates.setCenterX(parentAbsX);
        coordinates.setCenterY(parentAbsY);
        coordinates.setAngle(parentAngle);
        coordinates.setWidth(parentWidth);
        coordinates.setHeight(parentHeight);
        coordinates.setCoordinatesAbsoluteDots(dots);
        return coordinates;
    }

    private static Coordinates createCoordinates(final double centerX, final double centerY, final double angle,
                                                 final double width, final double height) {
        var readyAngle = cropAngle(angle);
        var dots = createAbsoluteDots(centerX, centerY, angle, width, height);

        var coordinates = new Coordinates();
        coordinates.setHeight(height);
        coordinates.setWidth(width);
        coordinates.setAngle(readyAngle);
        coordinates.setCenterX(centerX);
        coordinates.setCenterY(centerY);
        coordinates.setCoordinatesAbsoluteDots(dots);

        return coordinates;
    }

    private static CoordinatesAbsoluteDots createAbsoluteDots(final double centerX, final double centerY,
                                                              final double angle, final double width,
                                                              final double height) {
        var readyAngle = cropAngle(angle);
        var halfWidth = width / 2;
        var halfHeight = height / 2;
        var sin = sin(readyAngle);
        var cos = cos(readyAngle);

        var tlX = centerX - halfWidth * cos - halfHeight * sin;
        var tlY = centerY + halfWidth * sin + halfHeight * cos;
        var trX = centerX + halfWidth * cos + halfHeight * sin;
        var trY = centerY + halfWidth * sin + halfHeight * cos;
        var brX = centerX + halfWidth * cos + halfHeight * sin;
        var brY = centerY - halfWidth * sin - halfHeight * cos;
        var blX = centerX - halfWidth * cos - halfHeight * sin;
        var blY = centerY - halfWidth * sin - halfHeight * cos;

        var simpleDots = asList(new CoordinatesDot(brX, brY), new CoordinatesDot(trX, trY),
                new CoordinatesDot(tlX, tlY), new CoordinatesDot(blX, blY));
        return createFromDots(simpleDots);
    }

}
