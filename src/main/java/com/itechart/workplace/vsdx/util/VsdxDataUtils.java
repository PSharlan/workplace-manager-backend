package com.itechart.workplace.vsdx.util;

import com.aspose.diagram.Page;
import com.aspose.diagram.Shape;
import com.aspose.diagram.ShapeCollection;
import com.itechart.workplace.vsdx.model.BackgroundInfo;
import com.itechart.workplace.vsdx.model.Coordinates;
import com.itechart.workplace.vsdx.model.EmployeeCard;
import com.itechart.workplace.vsdx.model.RoomInfoCard;
import com.itechart.workplace.vsdx.model.Table;
import com.itechart.workplace.vsdx.model.TableNumber;
import com.itechart.workplace.vsdx.model.enums.TableType;
import lombok.experimental.UtilityClass;

import java.util.LinkedList;
import java.util.List;

import static com.itechart.workplace.vsdx.model.enums.TableType.SEMICIRCLE;
import static com.itechart.workplace.vsdx.util.VsdxCoordinatesUtils.createCoordinatesRotated;
import static com.itechart.workplace.vsdx.util.VsdxCoordinatesUtils.createOuterDots;
import static com.itechart.workplace.vsdx.util.VsdxCoordinatesUtils.findCoordinates;
import static com.itechart.workplace.vsdx.util.VsdxModelStatisticUtils.createCoordinatesDefaultSvg;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.coordinatesInDots;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.determineTableType;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.findIntersectionArea;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isBackground;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isCard;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isRoomFreeSeats;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isRoomNameCard;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isTableNumber;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.cropText;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.parseDepartment;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.parseEmployeeName;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.parseRoomFreeSeats;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.parseRoomNumber;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.parseRoomResponsible;
import static java.util.Comparator.comparingDouble;

@UtilityClass
@SuppressWarnings({"ReturnCount", "PMD"})
public class VsdxDataUtils {

    public static BackgroundInfo findPageInfo(final Page mainPage, final Page backgroundPage) {
        var shapes = mainPage.getShapes();
        for (var object : shapes) {
            var shape = (Shape) object;
            if (isBackground(shape)) {
                var backgroundWidth = backgroundPage.getPageSheet().getPageProps().getPageWidth().getValue();
                var backgroundHeight = backgroundPage.getPageSheet().getPageProps().getPageHeight().getValue();

                var outerWidth = shape.getXForm().getWidth().getValue();
                var outerHeight = shape.getXForm().getHeight().getValue();

                var leftMargin = shape.getXForm().getPinX().getValue() - outerWidth / 2;
                var bottomMargin = shape.getXForm().getPinY().getValue() - outerHeight / 2;

                return new BackgroundInfo(backgroundWidth, backgroundHeight, outerWidth,
                        outerHeight, leftMargin, bottomMargin);
            }
        }
        return null;
    }

    public static RoomInfoCard findRoomInfo(final ShapeCollection shapes) {
        for (var object : shapes) {
            var shape = (Shape) object;
            var id = shape.getID();
            var shapeText = cropText(shape.getText().getValue().getText());
            if (isRoomNameCard(shape)) {
                var card = new RoomInfoCard();
                card.setShapeId(id);
                card.setRoomNumber(parseRoomNumber(shapeText));
                card.setResponsibleName(parseRoomResponsible(shapeText));
                return card;
            }
        }
        return null;
    }

    public static List<EmployeeCard> findEmployeeCards(final ShapeCollection shapes) {
        var cards = new LinkedList<EmployeeCard>();
        for (var object : shapes) {
            var shape = (Shape) object;
            var id = shape.getID();
            var shapeText = cropText(shape.getText().getValue().getText());

            if (isCard(shape)) {
                var card = new EmployeeCard();
                card.setShapeId(id);
                card.setEmployeeName(parseEmployeeName(shapeText));
                card.setEmployeeDepartment(parseDepartment(shapeText));
                card.setCoordinates(findCoordinates(shape));
                cards.add(card);
            }
        }
        return cards;
    }

    public static Integer findRoomFreeSeats(final ShapeCollection shapes) {
        long freeSeatsCardId = 0;
        for (var object : shapes) {
            var shape = (Shape) object;
            if (isRoomFreeSeats(shape)) {
                freeSeatsCardId = shape.getID();
            }
        }

        if (freeSeatsCardId != 0) {
            for (var subObject : shapes.getShape(freeSeatsCardId).getShapes()) {
                var subShape = (Shape) subObject;
                var freeSeatsTextNum = cropText(subShape.getText().getValue().getText());
                if (freeSeatsTextNum.trim().matches("[0-9]+")) {
                    return parseRoomFreeSeats(freeSeatsTextNum);
                }
            }
        }
        return null;
    }

    public static List<Table> findTablesWithoutNumbers(final ShapeCollection backgroundShapes, final List<Table> tables,
                                                       final BackgroundInfo backgroundInfo) {
        for (var object : backgroundShapes) {
            var shape = (Shape) object;
            var innerShapes = shape.getShapes();

            if (innerShapes != null) {
                findTablesWithoutNumbers(innerShapes, tables, backgroundInfo);
            }

            var tableType = determineTableType(shape);
            if (tableType != null) {
                var table = parseTable(shape, tableType, backgroundInfo);
                tables.add(table);
            }
        }
        return tables;
    }

    public static List<TableNumber> findTableNumbers(final ShapeCollection backgroundShapes,
                                                     final List<TableNumber> tableNumbers) {
        for (var object : backgroundShapes) {
            var shape = (Shape) object;
            var innerShapes = shape.getShapes();

            if (innerShapes != null) {
                findTableNumbers(innerShapes, tableNumbers);
            }

            if (isTableNumber(shape)) {
                var tableNumber = parseTableNumber(shape);
                tableNumbers.add(tableNumber);
            }
        }
        return tableNumbers;
    }

    public static Integer findIntersectingNumber(final Table table, final List<TableNumber> tableNumbers) {
        for (var tableNumber : tableNumbers) {
            if (table.getType() == SEMICIRCLE && tableNumber.getParentId() != 0
                    && table.getParentId() == tableNumber.getParentId()) { // table and tableNumber have the same parent
                return tableNumber.getNumber();
            } else { // check coordinates and search for intersection
                var x = tableNumber.getCenterX();
                var y = tableNumber.getCenterY();
                var dots = table.getCoordinates().getCoordinatesAbsoluteDots();

                if (coordinatesInDots(x, y, dots)) {
                    return tableNumber.getNumber();
                }
            }
        }
        return 0;
    }

    public static EmployeeCard findLargestIntersection(final Table table, final List<EmployeeCard> cards) {
        return cards.stream().max(comparingDouble(c -> findIntersectionArea(table, c))).orElse(cards.get(0));
    }

    private static Table parseTable(final Shape shape, final TableType tableType, final BackgroundInfo backgroundInfo) {
        long parentId = 0;
        if (shape.getParentShape() != null) {
            parentId = shape.getParentShape().getID();
        }

        var table = new Table();
        table.setShapeId(shape.getID());
        table.setParentId(parentId);
        table.setType(tableType);

        Coordinates coordinates;
        if (tableType == SEMICIRCLE) {
            coordinates = createCoordinatesRotated(shape);
        } else {
            coordinates = findCoordinates(shape);
        }

        var svgDefault = createCoordinatesDefaultSvg(coordinates, backgroundInfo);
        coordinates.setCoordinatesSvg(svgDefault);

        var outerDots = createOuterDots(coordinates.getCoordinatesAbsoluteDots(), backgroundInfo);
        coordinates.setCoordinatesOuterDots(outerDots);

        table.setCoordinates(coordinates);
        return table;
    }

    private static TableNumber parseTableNumber(final Shape shape) {
        var text = cropText(shape.getText().getValue().getText());

        long parentId = 0;
        if (shape.getParentShape() != null) {
            parentId = shape.getParentShape().getID();
        }

        int tableNumber = Integer.parseInt(text);
        var centerX = shape.getXForm().getPinX().getValue();
        var centerY = shape.getXForm().getPinY().getValue();

        return new TableNumber(tableNumber, parentId, centerX, centerY);
    }

}
