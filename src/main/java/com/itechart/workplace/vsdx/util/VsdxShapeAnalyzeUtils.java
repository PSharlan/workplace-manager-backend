package com.itechart.workplace.vsdx.util;

import com.aspose.diagram.Shape;
import com.itechart.workplace.vsdx.model.CoordinatesAbsoluteDots;
import com.itechart.workplace.vsdx.model.EmployeeCard;
import com.itechart.workplace.vsdx.model.Table;
import com.itechart.workplace.vsdx.model.enums.TableType;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedList;

import static com.itechart.workplace.vsdx.model.enums.TableType.RECTANGLE;
import static com.itechart.workplace.vsdx.model.enums.TableType.SEMICIRCLE;
import static com.itechart.workplace.vsdx.util.VsdxShapeUtils.cropText;
import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.StrictMath.PI;

@Slf4j
@UtilityClass
@SuppressWarnings({"PMD", "LocalVariableName"})
public class VsdxShapeAnalyzeUtils {

    private static final double ANGLE_DELTA = 0.1;

    private static final String CARD_NAME_WITH_DEP_REGEXP = "^.+(\\([a-zA-Z0-9]+\\))$";
    private static final String CARD_NAME_WITHOUT_DEP_REGEXP = "^([A-Z][a-z]+)\\s([A-Z][a-z]+)$";
    private static final String CARD_RESERVED = "reserved";

    private static final double CARD_MIN_HEIGHT = 0.3;
    private static final double CARD_MAX_HEIGHT = 0.6;
    private static final double CARD_MIN_WIDTH = 2;
    private static final double CARD_MAX_WIDTH = 8;

    public static boolean isCard(final Shape shape) {
        var width = shape.getXForm().getWidth().getValue();
        var height = shape.getXForm().getHeight().getValue();

        var text = cropText(shape.getText().getValue().getText()).trim();

        return text.matches(CARD_NAME_WITH_DEP_REGEXP) || (height > CARD_MIN_HEIGHT && height < CARD_MAX_HEIGHT
                && width > CARD_MIN_WIDTH && width < CARD_MAX_WIDTH) && (text.matches(CARD_NAME_WITHOUT_DEP_REGEXP)
                || text.equalsIgnoreCase(CARD_RESERVED) || text.isEmpty());
    }

    public static boolean isRoomNameCard(final Shape shape) {
        var text = cropText(shape.getText().getValue().getText());
        return text.toLowerCase().contains("responsible:");
    }

    public static boolean isRoomFreeSeats(final Shape shape) {
        var subShapes = shape.getShapes();
        var isRoomFreeSeats = false;

        if (subShapes == null) {
            return isRoomFreeSeats;
        }

        for (var object : subShapes) {
            var subShape = (Shape) object;
            var text = cropText(subShape.getText().getValue().getText());
            if (text.toLowerCase().contains("free seats")) {
                isRoomFreeSeats = true;
                break;
            }
        }

        return isRoomFreeSeats;
    }

    public static boolean isBackground(final Shape shape) {
        return shape.getForeignData().getImageData() != null;
    }

    public static TableType determineTableType(final Shape shape) {
        var width = shape.getXForm().getWidth().getValue();
        var height = shape.getXForm().getHeight().getValue();
        TableType type = null;

        if (width > 47 && width < 48 && height > 51 && height < 52) {
            type = SEMICIRCLE;
        } else if (width > 31 && width < 32 && height > 62 && height < 64) {
            type = RECTANGLE;
        } else if (isTableNumber(shape) && width > 23 && width < 24 && height > 62 && height < 64) {
            type = RECTANGLE;
        }
        return type;
    }

    public static boolean isTableNumber(final Shape shape) {
        var text = cropText(shape.getText().getValue().getText()).trim();

        return text.matches("[0-9]+") && text.trim().length() <= 2;
    }

    public static boolean isVertical(final double angle) {
        return isTop(angle) || isBottom(angle);
    }

    public static boolean isHorizontal(final double angle) {
        return isLeft(angle) || isRight(angle);
    }

    public static boolean isTop(final double angle) {
        return angle > -ANGLE_DELTA && angle < ANGLE_DELTA;
    }

    public static boolean isBottom(final double angle) {
        return angle > PI - ANGLE_DELTA || angle < -PI + ANGLE_DELTA;
    }

    public static boolean isLeft(final double angle) {
        return angle > PI / 2 - ANGLE_DELTA && angle < PI / 2 + ANGLE_DELTA;
    }

    public static boolean isRight(final double angle) {
        return angle < -PI / 2 + ANGLE_DELTA && angle > -PI / 2 - ANGLE_DELTA;
    }

    public static boolean cardOnTable(final EmployeeCard card, final Table table) {
        var tableDots = table.getCoordinates().getCoordinatesOuterDots();
        var cardDots = card.getCoordinates().getCoordinatesAbsoluteDots();
        return tableDots.getTlX() < cardDots.getBrDot().getX() && tableDots.getBrX() > cardDots.getTlDot().getX()
                && tableDots.getTlY() > cardDots.getBrDot().getY() && tableDots.getBrY() < cardDots.getTlDot().getY();
    }

    public static boolean coordinatesInDots(final double x, final double y, final CoordinatesAbsoluteDots dots) {
        var triangleAreas = new LinkedList<Double>();
        triangleAreas.add(getTriangleArea(x, y, dots.getBlDot().getX(), dots.getBlDot().getY(), dots.getBrDot().getX(),
                dots.getBrDot().getY()));
        triangleAreas.add(getTriangleArea(x, y, dots.getBrDot().getX(), dots.getBrDot().getY(), dots.getTrDot().getX(),
                dots.getTrDot().getY()));
        triangleAreas.add(getTriangleArea(x, y, dots.getTrDot().getX(), dots.getTrDot().getY(), dots.getTlDot().getX(),
                dots.getTlDot().getY()));
        triangleAreas.add(getTriangleArea(x, y, dots.getTlDot().getX(), dots.getTlDot().getY(), dots.getBlDot().getX(),
                dots.getBlDot().getY()));
        var totalTrianglesArea = (int) triangleAreas.stream().mapToDouble(Double::doubleValue).sum();

        var rectangleArea = (int) getRectangleArea(dots.getTrDot().getX(), dots.getTrDot().getY(),
                dots.getTlDot().getX(), dots.getTrDot().getY(), dots.getBrDot().getX(), dots.getBlDot().getY());
        return totalTrianglesArea == rectangleArea;
    }

    private static double getTriangleArea(final double x1, final double y1,
                                          final double x2, final double y2,
                                          final double x3, final double y3) {
        return abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
    }

    private static double getRectangleArea(final double trX, final double trY,
                                           final double tlX, final double tlY,
                                           final double brX, final double brY) {
        var length = Math.sqrt((trX - tlX) * (trX - tlX) + (trY - tlY) * (trY - tlY));
        var height = Math.sqrt((trX - brX) * (trX - brX) + (trY - brY) * (trY - brY));
        return length * height;
    }

    public static Double findIntersectionArea(final Table table, final EmployeeCard card) {
        var tablesDots = table.getCoordinates().getCoordinatesOuterDots();
        var cardDots = card.getCoordinates().getCoordinatesAbsoluteDots();

        var xOverlap = min(tablesDots.getTrX(), cardDots.getTrDot().getX())
                - max(tablesDots.getTlX(), cardDots.getTlDot().getX());
        var yOverlap = min(tablesDots.getTlY(), cardDots.getTlDot().getY())
                - max(tablesDots.getBrY(), cardDots.getBrDot().getY());
        return xOverlap * yOverlap;
    }

}
