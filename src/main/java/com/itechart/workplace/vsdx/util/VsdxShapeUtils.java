package com.itechart.workplace.vsdx.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import static java.lang.Integer.parseInt;
import static java.lang.Math.PI;

@Slf4j
@UtilityClass
public class VsdxShapeUtils {

    public static String cropText(final String text) {
        var sb = new StringBuilder();
        var chars = text.toCharArray();
        var write = true;

        for (var c : chars) {
            if (c == '<') {
                write = false;
            }
            if (write) {
                sb.append(c);
            }
            if (c == '>') {
                write = true;
            }
        }
        return sb.toString().replaceAll("\n", "").replaceAll("\r", "");
    }

    public static double cropAngle(final double angle) {
        return angle % PI;
    }

    public static String parseDepartment(final String cardText) {
        var sb = new StringBuilder();
        var chars = cardText.toCharArray();

        var canWrite = false;
        for (int i = chars.length - 1; i >= 0; i--) {
            if (chars[i] == '(') {
                break;
            }
            if (canWrite) {
                sb.insert(0, chars[i]);
            }
            if (chars[i] == ')') {
                canWrite = true; // writing gonna start from next symbol (i--)
            }
        }
        return sb.toString();
    }

    public static String parseEmployeeName(final String cardText) {
        var sb = new StringBuilder();
        var chars = cardText.toCharArray();

        var canWrite = false;
        for (int i = chars.length - 1; i >= 0; i--) {
            if (canWrite) {
                sb.insert(0, chars[i]);
            }
            if (chars[i] == '(') {
                canWrite = true;
            }
        }
        return sb.toString();
    }

    public static String parseRoomNumber(final String roomNameCardText) {
        return roomNameCardText.replaceAll("[^0-9]", "");
    }

    public static String parseRoomResponsible(final String roomNameCardText) {
        var splitted = roomNameCardText.split(":");
        return splitted[1];
    }

    public static Integer parseRoomFreeSeats(final String roomFreeSeatsCardText) {
        var replaced = roomFreeSeatsCardText.replaceAll("[^0-9]", "");
        return parseInt(replaced);
    }

}
