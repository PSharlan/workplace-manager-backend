package com.itechart.workplace.vsdx.service;

import com.itechart.workplace.vsdx.model.ParsedRoom;

public interface SvgService {

    String generateSvg(ParsedRoom room, String generatedImagesFolder);

}
