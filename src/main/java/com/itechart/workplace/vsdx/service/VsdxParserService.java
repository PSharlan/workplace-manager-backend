package com.itechart.workplace.vsdx.service;

import com.itechart.workplace.vsdx.model.ParsedOffice;
import com.itechart.workplace.vsdx.model.ParsedRoom;

public interface VsdxParserService {

    ParsedOffice parseOffice(String filePath);

    ParsedRoom parseRoom(String filePath);

}
