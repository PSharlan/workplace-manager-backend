package com.itechart.workplace.vsdx.service.impl;

import com.itechart.workplace.vsdx.model.ParsedRoom;
import com.itechart.workplace.vsdx.model.Table;
import com.itechart.workplace.vsdx.service.SvgService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.svg2svg.SVGTranscoder;
import org.apache.batik.util.XMLResourceDescriptor;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@RequiredArgsConstructor
@SuppressWarnings({"AbbreviationAsWordInName", "PMD"})
public class SvgServiceImpl implements SvgService {

    private static final Double PT_COEFFICIENT = 0.75;
    private static final String SVG_NAMESPACE = "http://www.w3.org/2000/svg";
    private static final String SVG_UNITS = "pt";

    private static final String CLASS_LABEL_NAME_GROUP = "svg-label-name";
    private static final String CLASS_LABEL_NUMBER_GROUP = "svg-label-number";
    private static final String CLASS_TABLE_RECT = "svg-table";
    private static final String CLASS_TABLE_TEXT_NAME = "svg-table-text";
    private static final String ID_TABLE_RECT = "svg-table-%s";
    private static final String ID_TABLE_TEXT_NAME = "svg-table-text-%s";
    private static final String LABEL_BORDER_RADIUS = "2";
    private static final String LABEL_RECT_STYLE = "fill:white;stroke:black;stroke-width:1;opacity:1;z-index:1;";
    private static final String LABEL_TEXT_STYLE = "fill:black;text-anchor:middle;z-index:2;";
    private static final String TABLE_COLOR = "rgba(57, 153, 82, 0)";
    private static final String TABLE_NUMBER_REGEXP = "\\d+";
    private static final String WATERMARK_PART = "evaluation version";

    /**
     * Generates SVG images for room maps.
     *
     * @param room parser room
     * @param generatedImagesFolder folder where generated SVG image should be saved
     * @return path to generated SVG image
     */
    @Override
    @SneakyThrows
    public String generateSvg(final ParsedRoom room, final String generatedImagesFolder) {
        var parser = XMLResourceDescriptor.getXMLParserClassName();
        var saxSvgDocumentFactory = new SAXSVGDocumentFactory(parser);

        var svgDocument = saxSvgDocumentFactory.createDocument(room.getSvgSourceLink());
        var svgDocumentRoot = svgDocument.getDocumentElement();
        var textElements = svgDocumentRoot.getElementsByTagName("text");
        removeText(textElements);

        for (Table table : room.getTables()) {
            if (table.getNumber() != null && table.getNumber() != 0) {
                var tableElement = createTableGroup(svgDocument, table);
                svgDocumentRoot.appendChild(tableElement);
            }
        }
        return downloadFile(svgDocument, generatedImagesFolder);
    }

    private void removeText(final NodeList nodeList) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            var item = nodeList.item(i);

            if (item.hasChildNodes()) {
                var childNodes = item.getChildNodes();
                removeText(childNodes);
            }

            var text = item.getTextContent();
            if (text.toLowerCase().contains(WATERMARK_PART) || text.matches(TABLE_NUMBER_REGEXP)) {
                item.getParentNode().setTextContent(" ");
                return;
            }
        }
    }

    private Element createTableGroup(final Document document, final Table table) {
        var svgTableCoordinates = createSvgTableCoordinates(document, table);

        var group = createGroup(document);
        group.appendChild(createTable(document, svgTableCoordinates));
        group.appendChild(createNameLabel(document, svgTableCoordinates));
        group.appendChild(createNumberLabel(document, svgTableCoordinates));
        return group;
    }

    private Element createTable(final Document document, final SvgTableCoordinates table) {
        var rectangle = createRectangle(document, table.tlX, table.tlY, table.width, table.height);
        rectangle.setAttributeNS(null, "fill", TABLE_COLOR);
        rectangle.setAttributeNS(null, "transform", format("rotate(%s)", table.rotation));
        rectangle.setAttributeNS(null, "id", format(ID_TABLE_RECT, table.number));
        rectangle.setAttributeNS(null, "class", CLASS_TABLE_RECT);
        return rectangle;
    }

    private Element createNameLabel(final Document document, final SvgTableCoordinates table) {
        var labelHeight = table.height * 0.2;
        var labelTlY = table.centerY - labelHeight * 1.1;

        var rectangle = createLabelRectangle(document, table.tlX, labelTlY, table.width, labelHeight);
        rectangle.setAttributeNS(null, "id", table.number);

        var text = createNameText(document, table);
        text.setAttributeNS(null, "id", format(ID_TABLE_TEXT_NAME, table.number));
        text.setAttributeNS(null, "class", CLASS_TABLE_TEXT_NAME);

        var group = createGroup(document);
        group.setAttributeNS(null, "class", CLASS_LABEL_NAME_GROUP);
        group.appendChild(rectangle);
        group.appendChild(text);
        return group;
    }

    private Element createNameText(final Document document, final SvgTableCoordinates table) {
        var textBlY = table.centerY - table.nameFontHeight * 1.7;
        var spanBlY = textBlY + table.nameFontHeight * 1.2;

        var text = createTexElement(document, "text", table.centerX, textBlY, table.nameFontHeight);
        var depSpan = createTexElement(document, "tspan", table.centerX, spanBlY, table.nameFontHeight);
        text.appendChild(depSpan);
        return text;
    }

    private Element createNumberLabel(final Document document, final SvgTableCoordinates table) {
        var labelHeight = table.height * 0.15;
        var labelWidth = labelHeight * 1.5;
        var labelTlX = table.centerX - labelWidth / 2;

        var rectangle = createLabelRectangle(document, labelTlX, table.centerY, labelWidth, labelHeight);
        var text = createNumberText(document, table);

        var group = createGroup(document);
        group.appendChild(rectangle);
        group.appendChild(text);
        group.setAttributeNS(null, "class", CLASS_LABEL_NUMBER_GROUP);
        return group;
    }

    private Element createNumberText(final Document document, final SvgTableCoordinates table) {
        var textBlY = table.centerY + table.numberFontHeight;

        var text = document.createElementNS(SVG_NAMESPACE, "text");
        text.setAttributeNS(null, "x", valueOf(table.centerX));
        text.setAttributeNS(null, "y", valueOf(textBlY));
        text.setAttributeNS(null, "style", LABEL_TEXT_STYLE);
        text.setAttributeNS(null, "font-size", table.numberFontHeight + "px");

        text.setTextContent(valueOf(table.number));
        return text;
    }

    private Element createRectangle(final Document document, final double tlX, final double tlY, final double width,
                                    final double height) {
        var rectangle = document.createElementNS(SVG_NAMESPACE, "rect");
        rectangle.setAttributeNS(null, "x", valueOf(tlX));
        rectangle.setAttributeNS(null, "y", valueOf(tlY));
        rectangle.setAttributeNS(null, "width", valueOf(width));
        rectangle.setAttributeNS(null, "height", valueOf(height));
        return rectangle;
    }

    private Element createLabelRectangle(final Document document, final double tlX, final double tlY,
                                         final double width, final double height) {
        var rect = createRectangle(document, tlX, tlY, width, height);
        rect.setAttributeNS(null, "rx", LABEL_BORDER_RADIUS);
        rect.setAttributeNS(null, "ry", LABEL_BORDER_RADIUS);
        rect.setAttributeNS(null, "style", LABEL_RECT_STYLE);
        return rect;
    }

    private Element createTexElement(final Document document, final String qualifiedName, final double x,
                                     final double y, final double fontHeight) {
        var text = document.createElementNS(SVG_NAMESPACE, qualifiedName);
        text.setAttributeNS(null, "x", valueOf(x));
        text.setAttributeNS(null, "y", valueOf(y));
        text.setAttributeNS(null, "style", LABEL_TEXT_STYLE);
        text.setAttributeNS(null, "font-size", fontHeight + "px");
        text.setTextContent(" ");
        return text;
    }

    private Element createGroup(final Document document) {
        return document.createElementNS(SVG_NAMESPACE, "g");
    }

    @SneakyThrows
    private String downloadFile(final Document document, final String generatedImagesFolder) {
        var documentURI = document.getDocumentURI();
        var fileName = documentURI.substring(documentURI.indexOf("room"));
        var generatedSvgLink = Paths.get(generatedImagesFolder, fileName).toString();
        var fileOutputStream = new FileOutputStream(generatedSvgLink);

        var transcoderInput = new TranscoderInput(document);
        var outputStreamWriter = new OutputStreamWriter(fileOutputStream, UTF_8);
        var bufferedWriter = new BufferedWriter(outputStreamWriter);
        var transcoderOutput = new TranscoderOutput(bufferedWriter);
        var transcoderSvg = new SVGTranscoder();

        transcoderSvg.transcode(transcoderInput, transcoderOutput);

        outputStreamWriter.flush();
        outputStreamWriter.close();

        fileOutputStream.flush();
        fileOutputStream.close();

        return generatedSvgLink;
    }

    private Double getImgWidth(final Document document) {
        var svgTag = document.getElementsByTagName("svg");
        var width = svgTag.item(0).getAttributes().getNamedItem("width").getFirstChild().getNodeValue();
        var widthWithoutUnits = width.substring(0, width.indexOf(SVG_UNITS));
        var imgWidth = Double.parseDouble(widthWithoutUnits);
        return imgWidth / PT_COEFFICIENT;
    }

    private Double getImgHeight(final Document document) {
        var svgTag = document.getElementsByTagName("svg");
        var height = svgTag.item(0).getAttributes().getNamedItem("height").getFirstChild().getNodeValue();
        var heightWithoutUnits = height.substring(0, height.indexOf(SVG_UNITS));
        var imgHeight = Double.parseDouble(heightWithoutUnits);
        return imgHeight / PT_COEFFICIENT;
    }

    private String getRotation(final Double angle, final Double x, final Double y,
                               final Double width, final Double height) {
        var widthForRotate = width / 2 + x;
        var heightForRotate = height / 2 + y;
        var angleInverse = 180 - angle;
        return angleInverse + " " + widthForRotate + " " + heightForRotate;
    }

    private SvgTableCoordinates createSvgTableCoordinates(final Document document, final Table table) {
        var imgWidth = getImgWidth(document);
        var imgHeight = getImgHeight(document);
        var coordinates = table.getCoordinates().getCoordinatesSvg();

        var tlX = coordinates.getTlX() * imgWidth;
        var tlY = coordinates.getTlY() * imgHeight;
        var width = coordinates.getWidth() * imgWidth;
        var height = coordinates.getHeight() * imgHeight;
        var rotation = getRotation(coordinates.getAngle(), tlX, tlY, width, height);
        var centerX = tlX + width / 2;
        var centerY = tlY + height / 2;
        var nameFontHeight = height * 0.08;
        var numberFontHeight = height * 0.12;

        return new SvgTableCoordinates(valueOf(table.getNumber()),
                tlX, tlY, centerX, centerY, width, height, rotation, nameFontHeight, numberFontHeight);
    }

    @AllArgsConstructor
    private class SvgTableCoordinates {

        String number;
        double tlX;
        double tlY;
        double centerX;
        double centerY;
        double width;
        double height;
        String rotation;
        double nameFontHeight;
        double numberFontHeight;

    }

}
