package com.itechart.workplace.vsdx.service.impl;

import com.aspose.diagram.Diagram;
import com.aspose.diagram.Page;
import com.aspose.diagram.SVGSaveOptions;
import com.aspose.diagram.SaveFileFormat;
import com.aspose.diagram.Shape;
import com.aspose.diagram.ShapeCollection;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.event.VsdxParsingCompletionEvent;
import com.itechart.workplace.exception.BadRequestException;
import com.itechart.workplace.validators.VsdxParsingValidator;
import com.itechart.workplace.vsdx.model.BackgroundInfo;
import com.itechart.workplace.vsdx.model.EmployeeCard;
import com.itechart.workplace.vsdx.model.ParsedOffice;
import com.itechart.workplace.vsdx.model.ParsedRoom;
import com.itechart.workplace.vsdx.model.Table;
import com.itechart.workplace.vsdx.model.TableNumber;
import com.itechart.workplace.vsdx.service.SvgService;
import com.itechart.workplace.vsdx.service.VsdxParserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.itechart.workplace.exception.enums.ParseWarningType.INVALID_ATTRIBUTE;
import static com.itechart.workplace.exception.enums.ParseWarningType.OBJECT_IS_NULL;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findEmployeeCards;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findIntersectingNumber;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findLargestIntersection;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findPageInfo;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findRoomFreeSeats;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findRoomInfo;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findTableNumbers;
import static com.itechart.workplace.vsdx.util.VsdxDataUtils.findTablesWithoutNumbers;
import static com.itechart.workplace.vsdx.util.VsdxModelStatisticUtils.analyzeParsedData;
import static com.itechart.workplace.vsdx.util.VsdxModelStatisticUtils.analyzeTableCardRelations;
import static com.itechart.workplace.vsdx.util.VsdxModelStatisticUtils.createViolation;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.cardOnTable;
import static com.itechart.workplace.vsdx.util.VsdxShapeAnalyzeUtils.isBackground;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@RequiredArgsConstructor
@SuppressWarnings({"ReturnCount", "PMD", "LocalVariableName"})
public class VsdxParserServiceImpl implements VsdxParserService {

    private final SvgService svgService;

    private final AppConfiguration appConfiguration;
    private final VsdxParsingValidator vsdxParsingValidator;
    private final ApplicationEventPublisher applicationEventPublisher;

    private String vsdxFolder;
    private String sourceImagesFolder;
    private String generatedImagesFolder;

    @PostConstruct
    public void init() {
        vsdxFolder = appConfiguration.getVsdx().getFolder();
        sourceImagesFolder = appConfiguration.getVsdx().getSourceImagesFolder();
        generatedImagesFolder = appConfiguration.getVsdx().getGeneratedImagesFolder();
    }

    /**
     * Parses office map from given VSDx file and saves rooms' maps as SVG images.
     *
     * @param filepath path where VSDx image is located
     * @return meta info about parsed office
     */
    @Override
    @SneakyThrows
    public ParsedOffice parseOffice(final String filepath) {

        var office = new ParsedOffice();
        var diagram = new Diagram(filepath);
        var pages = diagram.getPages();
        var officeFolder = Paths.get(filepath).getParent().toString();

        var rooms = new LinkedList<ParsedRoom>();
        for (var object : pages) {
            var page = (Page) object;
            var room = parsePage(page, officeFolder);
            rooms.add(room);
        }

        office.setParsedRooms(rooms);
        var officeVsdxRelativePath = Paths.get(vsdxFolder).relativize(Paths.get(filepath)).toString();
        office.setVisioLink(FilenameUtils.separatorsToUnix(officeVsdxRelativePath));
        vsdxParsingValidator.addViolations(analyzeParsedData(office));

        this.applicationEventPublisher.publishEvent(new VsdxParsingCompletionEvent(this, officeFolder));

        log.info("Office was successfully parsed from the file {}", filepath);
        return office;
    }

    @Override
    @SneakyThrows
    public ParsedRoom parseRoom(final String filepath) {

        var officeFolder = Paths.get(filepath).getParent().toString();
        var diagram = new Diagram(filepath);
        var pageCollection = diagram.getPages();

        var pages = new ArrayList<Page>();
        for (var object : pageCollection) {
            pages.add((Page) object);
        }
        if (pages.size() != 1) {
            throw new BadRequestException("File must contain one room");
        }

        var room = parsePage(pages.get(0), officeFolder);
        vsdxParsingValidator.addViolations(analyzeParsedData(room));
        log.info("Room was successfully parsed");
        return room;
    }

    private ParsedRoom parsePage(final Page page, final String officeFolder) {
        var shapes = page.getShapes();
        var room = new ParsedRoom();
        room.setPageId(page.getID());

        var roomInfo = findRoomInfo(shapes);
        if (roomInfo == null) {
            vsdxParsingValidator.addViolation(createViolation(OBJECT_IS_NULL, "Room info wasn't recognized"));
            room.setTables(emptyList());
            return room;
        }

        var cards = findEmployeeCards(shapes);
        room.setEmployeeCards(cards);
        room.setFreeSeats(findRoomFreeSeats(shapes));
        room.setRoomInfoCard(roomInfo);

        var roomNumber = room.getRoomInfoCard().getRoomNumber();
        log.info("parsePage - Current room: {}", roomNumber);

        var backgroundDiagram = buildBackgroundVsdx(shapes);
        if (backgroundDiagram == null) {
            var message = format("Background diagram wasn't recognized. Room %s", roomNumber);
            vsdxParsingValidator.addViolation(createViolation(OBJECT_IS_NULL, message));
            room.setTables(emptyList());
            return room;
        }

        var backgroundPage = backgroundDiagram.getPages().get(0);
        var svgSourceLink = buildSourceSvg(backgroundDiagram, officeFolder, roomNumber);
        room.setSvgSourceLink(svgSourceLink);
        var backgroundInfo = findPageInfo(page, backgroundPage);
        if (backgroundInfo == null) {
            var message = format("Background info wasn't recognized. Room %s", roomNumber);
            vsdxParsingValidator.addViolation(createViolation(OBJECT_IS_NULL, message));
            room.setTables(emptyList());
            return room;
        }

        var tables = findTables(backgroundPage.getShapes(), backgroundInfo, roomNumber);
        room.setTables(tables);
        if (tables.isEmpty()) {
            return room;
        }

        var relations = findTableCardRelations(tables, cards, roomNumber);
        room.setTableCardRelations(relations);
        var svgGenerationFolder = Paths.get(officeFolder, generatedImagesFolder).toString();
        var svgGeneratedLink = svgService.generateSvg(room, svgGenerationFolder);
        var svgGeneratedRelativePath = Paths.get(vsdxFolder).relativize(Paths.get(svgGeneratedLink)).toString();
        room.setSvgGeneratedLink(FilenameUtils.separatorsToUnix(svgGeneratedRelativePath));
        return room;
    }

    @SneakyThrows
    private Diagram buildBackgroundVsdx(final ShapeCollection shapes) {
        var shape = getBackgroundOrNull(shapes);
        if (shape == null) {
            return null;
        }

        var foreignData = shape.getForeignData();
        var backgroundVsdxData = foreignData.getObjectData();

        try (var fos = new FileOutputStream(vsdxFolder + "temp_background.vsdx")) {
            fos.write(backgroundVsdxData);
        } catch (Exception e) {
            log.info("buildBackgroundPage -> Error during creation inner vsdx process");
        }

        return new Diagram(vsdxFolder + "temp_background.vsdx");
    }

    @SneakyThrows
    private String buildSourceSvg(final Diagram diagram, final String officeFolder, final String roomName) {
        var options = new SVGSaveOptions();
        options.setSaveFormat(SaveFileFormat.SVG);

        var svgName = String.format("room_%s.svg", roomName);
        var sourceSvgLink = Paths.get(officeFolder, sourceImagesFolder, svgName).toString();
        diagram.save(sourceSvgLink, SaveFileFormat.SVG);
        return sourceSvgLink;
    }

    private Shape getBackgroundOrNull(final ShapeCollection shapes) {
        for (var object : shapes) {
            var shape = (Shape) object;
            if (isBackground(shape)) {
                return shape;
            }
        }
        return null;
    }

    private List<Table> findTables(final ShapeCollection backgroundShapes, final BackgroundInfo backgroundInfo,
                                   final String roomNumber) {
        var tables = findTablesWithoutNumbers(backgroundShapes, new LinkedList<>(), backgroundInfo);
        var tableNumbers = findTableNumbers(backgroundShapes, new LinkedList<>());

        if (tables.size() != tableNumbers.size()) {
            var message = format("There are %s tables and %s table numbers found in room %s", tables.size(),
                    tableNumbers.size(), roomNumber);
            vsdxParsingValidator.addViolation(createViolation(INVALID_ATTRIBUTE, message));
        }
        tables.forEach(table -> table.setNumber(findIntersectingNumber(table, tableNumbers)));

        var numbers = tableNumbers.stream().map(TableNumber::getNumber).collect(toList());
        var foundNumbers = tables.stream().map(Table::getNumber).collect(toList());
        var notFoundNumbers = numbers.stream().filter(number -> !foundNumbers.contains(number)).collect(toList());
        if (!notFoundNumbers.isEmpty()) {
            var message = format("Tables weren't found for numbers %s in room %s", notFoundNumbers, roomNumber);
            vsdxParsingValidator.addViolation(createViolation(INVALID_ATTRIBUTE, message));
        }
        return tables;
    }

    private Map<Integer, UUID> findTableCardRelations(final List<Table> tables, final List<EmployeeCard> cards,
                                                      final String roomNumber) {
        var foundCardsIds = new LinkedList<UUID>();
        var relations = new HashMap<Integer, UUID>();
        tables.forEach(table -> {
            var candidates = new LinkedList<EmployeeCard>();
            cards.forEach(card -> {
                if (cardOnTable(card, table) && !foundCardsIds.contains(card.getId())) {
                    candidates.add(card);
                }
            });

            if (candidates.size() == 0) {
                vsdxParsingValidator.addViolation(analyzeTableCardRelations(table, cards, roomNumber));
            } else {
                var mostProbableCard = findLargestIntersection(table, candidates);
                relations.put(table.getNumber(), mostProbableCard.getId());
                foundCardsIds.add(mostProbableCard.getId());
                log.info("Table: {} | Employee: {}", table.getNumber(), mostProbableCard.getEmployeeName());
            }
        });
        return relations;
    }

}
