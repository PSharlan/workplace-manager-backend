package com.itechart.workplace.vsdx.service.impl;

import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.entity.OfficeEntity;
import com.itechart.workplace.entity.PlaceEntity;
import com.itechart.workplace.entity.RoomEntity;
import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.service.EmployeeService;
import com.itechart.workplace.validators.VsdxParsingValidator;
import com.itechart.workplace.vsdx.model.EmployeeCard;
import com.itechart.workplace.vsdx.model.ParsedRoom;
import com.itechart.workplace.vsdx.service.VsdxParserService;
import com.itechart.workplace.vsdx.service.VsdxResolverService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static com.itechart.workplace.entity.enums.PlaceStatusEntity.AVAILABLE;
import static com.itechart.workplace.exception.enums.ParseWarningType.INCORRECT_RESULT_SET;
import static com.itechart.workplace.mapper.EmployeeMapper.EMPLOYEE_MAPPER;
import static com.itechart.workplace.mapper.OfficeMapper.OFFICE_MAPPER;
import static com.itechart.workplace.mapper.PlaceMapper.PLACE_MAPPER;
import static com.itechart.workplace.mapper.RoomMapper.ROOM_MAPPER;
import static com.itechart.workplace.vsdx.util.VsdxModelStatisticUtils.createViolation;
import static java.lang.String.format;
import static java.nio.file.Files.newOutputStream;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@SuppressWarnings("PMD")
@RequiredArgsConstructor
public class VsdxResolverServiceImpl implements VsdxResolverService {

    private final EmployeeService employeeService;
    private final VsdxParserService parser;

    private final AppConfiguration appConfiguration;
    private final VsdxParsingValidator vsdxParsingValidator;

    private String vsdxFolder;
    private String sourceImagesFolder;
    private String generatedImagesFolder;

    @PostConstruct
    public void init() {
        vsdxFolder = appConfiguration.getVsdx().getFolder();
        sourceImagesFolder = appConfiguration.getVsdx().getSourceImagesFolder();
        generatedImagesFolder = appConfiguration.getVsdx().getGeneratedImagesFolder();
    }

    @Override
    public OfficeEntity prepareOffice(final MultipartFile vsdxImage, final String location) {
        var officeFolder = prepareFolders(vsdxFolder, location);
        var filepath = saveFile(vsdxImage, officeFolder);

        var parsedOffice = parser.parseOffice(filepath);
        parsedOffice.setLocation(location);
        var officeEntity = OFFICE_MAPPER.toEntity(parsedOffice);

        var parsedRooms = parsedOffice.getParsedRooms();
        var rooms = prepareRooms(parsedRooms, officeEntity);
        log.info("Rooms were successfully mapped for office {}", officeEntity.getLocation());
        officeEntity.setRooms(rooms);
        return officeEntity;
    }

    @Override
    public RoomEntity prepareRoom(final MultipartFile vsdxImage, final OfficeEntity office) {
        var officeFolder = prepareFolders(vsdxFolder, office.getLocation());
        var filepath = saveFile(vsdxImage, officeFolder);

        var parsedRoom = parser.parseRoom(filepath);
        return mapRoom(parsedRoom, office);
    }

    private String saveFile(final MultipartFile vsdxImage, final String officeFolder) {
        var filename = vsdxImage.getOriginalFilename();
        var filepath = Paths.get(officeFolder, filename);
        try (var os = newOutputStream(filepath)) {
            os.write(vsdxImage.getBytes());
        } catch (Exception e) {
            log.error("Exception during vsdx MultipartFile saving process. Message: " + e.getMessage());
        }
        return filepath.toString();
    }

    private List<RoomEntity> prepareRooms(final List<ParsedRoom> parsedRooms, final OfficeEntity officeEntity) {
        return parsedRooms.stream().map(parsedRoom -> mapRoom(parsedRoom, officeEntity)).collect(toList());
    }

    private RoomEntity mapRoom(final ParsedRoom parsedRoom, final OfficeEntity officeEntity) {
        var manager = retrieveEmployee(parsedRoom.getRoomInfoCard().getResponsibleName(), "",
                parsedRoom.getRoomInfoCard().getRoomNumber());
        var roomEntity = ROOM_MAPPER.toEntity(parsedRoom, officeEntity, manager);
        var places = preparePlaces(parsedRoom, roomEntity);

        log.info("Places were successfully mapped for room {}", parsedRoom.getRoomInfoCard().getRoomNumber());
        roomEntity.setPlaces(places);
        return roomEntity;
    }

    private List<PlaceEntity> preparePlaces(final ParsedRoom parsedRoom, final RoomEntity roomEntity) {
        var deps = employeeService.getDepartments();
        if (parsedRoom.getTables() == null) {
            return null;
        }
        return parsedRoom.getTables().stream().map(table -> {
            var relations = parsedRoom.getTableCardRelations();
            var cardId = relations.get(table.getNumber());
            var cards = parsedRoom.getEmployeeCards().stream().filter(employeeCard -> employeeCard.getId()
                    .equals(cardId)).collect(toList());

            EmployeeCard card;
            if (cards.isEmpty()) {
                var message = format("Cards weren't parsed correctly for room %s",
                        parsedRoom.getRoomInfoCard().getRoomNumber());
                vsdxParsingValidator.addViolation(createViolation(INCORRECT_RESULT_SET, message));
                card = new EmployeeCard();
            } else {
                card = cards.get(0);
            }

            Employee employee = null;
            var name = card.getEmployeeName();
            if (!"".equals(name)) {
                var dep = card.getEmployeeDepartment();
                employee = retrieveEmployee(name, deps.contains(dep) ? dep : "",
                        parsedRoom.getRoomInfoCard().getRoomNumber());
            }

            return PLACE_MAPPER.toEntity(roomEntity, EMPLOYEE_MAPPER.toEntity(employee), card, table, AVAILABLE);
        }).collect(toList());
    }

    private Employee retrieveEmployee(final String name, final String department, final String roomNumber) {
        var resultSet = employeeService.getAllByUsernameAndDepartment(name, department);
        if (resultSet.size() != 1) {
            var message = format("There is %s results for name %s. Room %s", resultSet.size(), name, roomNumber);
            vsdxParsingValidator.addViolation(createViolation(INCORRECT_RESULT_SET, message));
        }

        if (resultSet.isEmpty()) {
            return null;
        }
        return resultSet.get(0);
    }

    /**
     * Creates directory hierarchy for a given office.
     *
     * @param rootDir directory where directory hierarchy should be created
     * @param officeLocation office location in the defined format ({@code Minsk | T10})
     * @return path to office folder in hierarchy
     */
    @SneakyThrows
    protected String prepareFolders(final String rootDir, final String officeLocation) {

        List<String> locations = Arrays.stream(officeLocation.split("\\|"))
                .map(String::trim)
                .collect(toList());

        var city = locations.get(0);
        var office = locations.get(1);

        final Path sourceImagesPath = Path.of(rootDir, city, office, this.sourceImagesFolder);
        final Path generatedImagesPath = Path.of(rootDir, city, office, this.generatedImagesFolder);

        Files.createDirectories(sourceImagesPath);
        Files.createDirectories(generatedImagesPath);

        return Path.of(rootDir, city, office).toString();
    }

}
