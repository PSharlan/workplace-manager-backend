package com.itechart.workplace.vsdx.service;

import com.itechart.workplace.entity.OfficeEntity;
import com.itechart.workplace.entity.RoomEntity;
import org.springframework.web.multipart.MultipartFile;

public interface VsdxResolverService {

    OfficeEntity prepareOffice(MultipartFile vsdxImage, String location);

    RoomEntity prepareRoom(MultipartFile vsdxImage, OfficeEntity office);

}
