package com.itechart.workplace.model;

import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class AmazonS3Object implements FileStorageObject {

    private final S3Object s3Object;

    @Override
    @SneakyThrows
    public byte[] getFile() {
        try (var s3ObjectInputStream = s3Object.getObjectContent()) {
            return s3ObjectInputStream.readAllBytes();
        }
    }

    @Override
    public String getContentType() {
        return s3Object.getObjectMetadata().getContentType();
    }
}
