package com.itechart.workplace.model;

/**
 * This interface provides methods to access file and its metadata.
 *
 * @author D.Delmukhambetov
 * @since 26.02.2020
 */
public interface FileStorageObject {

    byte[] getFile();

    String getContentType();

}
