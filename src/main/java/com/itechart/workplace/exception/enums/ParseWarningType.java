package com.itechart.workplace.exception.enums;

public enum ParseWarningType {

    OBJECT_IS_NULL,
    INCORRECT_RESULT_SET,
    INVALID_ATTRIBUTE,
    INCORRECT_INPUT_FILE

}
