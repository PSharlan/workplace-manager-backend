package com.itechart.workplace.exception;

import com.itechart.workplace.generated.model.Violation;
import lombok.Getter;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.List;

@Getter
public class UnprocessableEntityException extends AbstractThrowableProblem {

    private List<Violation> violations;

    public UnprocessableEntityException(final String message, final List<Violation> violations) {
        super(ErrorConstants.UNPROCESSABLE_ENTITY, "Unprocessable entity", Status.UNPROCESSABLE_ENTITY, message);
        this.violations = violations;
    }

}
