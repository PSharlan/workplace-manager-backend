package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.AuthApi;
import com.itechart.workplace.generated.model.LoginRequest;
import com.itechart.workplace.generated.model.Token;
import com.itechart.workplace.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@AllArgsConstructor
public class AuthApiController implements AuthApi {

    private final AuthService authService;

    @Override
    public ResponseEntity<Token> login(@Valid @RequestBody final LoginRequest body) {
        var token = authService.login(body);
        return new ResponseEntity<>(token, CREATED);
    }

    @Override
    public ResponseEntity<Token> refresh(@Valid @RequestParam final String refreshToken) {
        var token = authService.refresh(refreshToken);
        return new ResponseEntity<>(token, CREATED);
    }

}
