package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.UsersApi;
import com.itechart.workplace.generated.model.User;
import com.itechart.workplace.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
public class UsersApiController implements UsersApi {

    private final UserService userService;

    @Override
    public ResponseEntity<User> getCurrentUser() {
        var currentUser = userService.getCurrent();
        return new ResponseEntity<>(currentUser, OK);
    }

}
