package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.RequestsApi;
import com.itechart.workplace.generated.model.CreateRequest;
import com.itechart.workplace.generated.model.Request;
import com.itechart.workplace.service.RequestService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@AllArgsConstructor
public class RequestsApiController implements RequestsApi {

    private final RequestService requestService;

    @Override
    public ResponseEntity<Request> createRequest(@Valid @RequestBody final CreateRequest body) {
        var request = requestService.create(body);
        return new ResponseEntity<>(request, CREATED);
    }

}
