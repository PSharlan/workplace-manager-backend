package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.DepartmentsApi;
import com.itechart.workplace.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
public class DepartmentsApiController implements DepartmentsApi {

    private final EmployeeService employeeService;

    @Override
    public ResponseEntity<List<String>> getDepartmentGroups(@RequestParam @NotNull @Valid final String department) {
        var groups = employeeService.getDepartmentGroups(department);
        return new ResponseEntity<>(groups, OK);
    }

    @Override
    public ResponseEntity<List<String>> getDepartments() {
        var departments = employeeService.getDepartments();
        return new ResponseEntity<>(departments, OK);
    }

}
