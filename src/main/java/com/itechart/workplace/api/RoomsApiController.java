package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.RoomsApi;
import com.itechart.workplace.generated.model.Room;
import com.itechart.workplace.service.RoomService;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.*;

@RestController
@AllArgsConstructor
public class RoomsApiController implements RoomsApi {

    private final RoomService roomService;

    @Override
    public ResponseEntity<Void> deleteRoomById(final UUID id) {
        roomService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @Override
    public ResponseEntity<Room> getRoomById(@PathVariable final UUID id) {
        var room = roomService.getById(id);
        return new ResponseEntity<>(room, OK);
    }

    @Override
    public ResponseEntity<Resource> getRoomSvgById(@PathVariable final UUID id) {
        final var svgImage = roomService.getSvgImage(id);
        return ResponseEntity
                .ok()
                .header(CONTENT_TYPE, svgImage.getContentType())
                .body(new ByteArrayResource(svgImage.getFile()));
    }

    @Override
    public ResponseEntity<Room> importRoomsFromVsdx(@Valid @RequestPart("schemaFile") final MultipartFile vsdxImage,
                                                    @NotNull @Valid @RequestParam("officeId") final UUID officeId,
                                                    @RequestParam(defaultValue = "false") final
                                                    Boolean ignoreWarnings) {
        var room = roomService.importFromVsdx(officeId, vsdxImage, ignoreWarnings);
        return new ResponseEntity<>(room, CREATED);
    }

    @Override
    public ResponseEntity<Room> updateRoomAttributes(@PathVariable final UUID id,
                                                     @NotNull @Valid final UUID newManagerId) {
        var room = roomService.updateAttributes(id, newManagerId);
        return new ResponseEntity<>(room, OK);
    }

}
