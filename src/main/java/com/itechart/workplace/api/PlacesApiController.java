package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.PlacesApi;
import com.itechart.workplace.generated.model.Place;
import com.itechart.workplace.service.PlaceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
public class PlacesApiController implements PlacesApi {

    private final PlaceService placeService;

    @Override
    public ResponseEntity<List<Place>> getAllPlaces(@Valid @RequestParam(required = false) final String location,
                                                    @Valid @RequestParam(required = false) final String roomNumber,
                                                    @Valid @RequestParam(required = false, defaultValue = "0")
                                                        final Integer number) {
        var place = placeService.getPlacesByLocationAndRoomAndNumber(location, roomNumber, number);
        return new ResponseEntity<>(place, OK);
    }

}
