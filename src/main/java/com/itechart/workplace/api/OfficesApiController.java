package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.OfficesApi;
import com.itechart.workplace.generated.model.Office;
import com.itechart.workplace.generated.model.Room;
import com.itechart.workplace.service.OfficeService;
import com.itechart.workplace.service.RoomService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
public class OfficesApiController implements OfficesApi {

    private final OfficeService officeService;
    private final RoomService roomService;

    @Override
    public ResponseEntity<List<Room>> getOfficeRooms(@PathVariable final UUID id) {
        var rooms = roomService.getAllByOfficeId(id);
        return new ResponseEntity<>(rooms, OK);
    }

    @Override
    public ResponseEntity<Office> importOfficesFromVsdx(
            @Valid @RequestPart("schemaFile") final MultipartFile schemaFile,
            @RequestParam final String city,
            @RequestParam final String officeName,
            @Valid @RequestPart(value = "imageFile", required = false) final MultipartFile imageFile,
            @RequestParam(value = "ignoreWarnings", required = false, defaultValue = "false")
            final Boolean ignoreWarnings,
            @RequestParam(value = "latitude", required = false, defaultValue = "0") final Double latitude,
            @RequestParam(value = "longitude", required = false, defaultValue = "0") final Double longitude) {
        List<Double> coordinates = List.of(latitude, longitude);
        var savedOffice = officeService.importFromVsdx(schemaFile,
                String.format("%s | %s", city, officeName), coordinates, ignoreWarnings);
        return new ResponseEntity<>(savedOffice, CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteOfficeById(final UUID id) {
        officeService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @Override
    public ResponseEntity<List<Office>> getAllOffices() {
        var offices = officeService.getAll();
        return new ResponseEntity<>(offices, OK);
    }

}
