package com.itechart.workplace.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itechart.workplace.util.VersionUtils;
import lombok.SneakyThrows;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class SwaggerApiController {

    public static final String SWAGGER_UI_REDIRECT_PATH = "/";
    public static final String GET_UI_CONFIGURATION_PATH = "/swagger-resources/configuration/ui";
    public static final String GET_SECURITY_CONFIGURATION_PATH = "/swagger-resources/configuration/security";
    public static final String GET_CONFIGURATION_PATH = "/swagger-resources";
    public static final String GET_SWAGGER_YML_PATH = "/swagger.yml";
    public static final String GET_SWAGGER_YAML_PATH = "/swagger.yaml";
    public static final String GET_SWAGGER_JSON_PATH = "/swagger.json";

    private final String uiConfigurationJson;
    private final String securityConfigurationJson;
    private final String configurationJson;
    private final String swaggerYaml;
    private final String swaggerJson;

    @SneakyThrows
    @SuppressWarnings("checkstyle:LineLength")
    public SwaggerApiController(final ResourceLoader resourceLoader,
                                final ObjectMapper objectMapper,
                                final Yaml yaml) {
        var configurationUiJson = loadJson(objectMapper, resourceLoader, "classpath:/swagger/configuration-ui.json");
        var configurationSecurityJson = loadJson(objectMapper, resourceLoader, "classpath:/swagger/configuration-security.json");
        var configurationJson = loadJson(objectMapper, resourceLoader, "classpath:/swagger/configuration.json");
        var swaggerYaml = populateVersion(loadYaml(yaml, resourceLoader, "classpath:/swagger/swagger.yaml"));
        this.uiConfigurationJson = objectMapper.writeValueAsString(configurationUiJson);
        this.securityConfigurationJson = objectMapper.writeValueAsString(configurationSecurityJson);
        this.configurationJson = objectMapper.writeValueAsString(configurationJson);
        this.swaggerYaml = yaml.dump(swaggerYaml);
        this.swaggerJson = objectMapper.writeValueAsString(swaggerYaml);
    }

    @GetMapping(SWAGGER_UI_REDIRECT_PATH)
    public String swaggerUiRedirect() {
        return "redirect:swagger-ui.html";
    }

    @ResponseBody
    @GetMapping(GET_UI_CONFIGURATION_PATH)
    public String getUiConfiguration() {
        return uiConfigurationJson;
    }

    @ResponseBody
    @GetMapping(GET_SECURITY_CONFIGURATION_PATH)
    public String getSecurityConfiguration() {
        return securityConfigurationJson;
    }

    @ResponseBody
    @GetMapping(GET_CONFIGURATION_PATH)
    public String getConfiguration() {
        return configurationJson;
    }

    @ResponseBody
    @GetMapping(value = {GET_SWAGGER_YML_PATH, GET_SWAGGER_YAML_PATH}, produces = "text/yaml")
    public String getSwaggerYaml() {
        return swaggerYaml;
    }

    @ResponseBody
    @GetMapping(GET_SWAGGER_JSON_PATH)
    public String getSwaggerJson() {
        return swaggerJson;
    }

    private Map<String, Object> loadYaml(final Yaml yaml,
                                         final ResourceLoader resourceLoader,
                                         final String location) throws IOException {
        return yaml.load(resourceLoader.getResource(location).getInputStream());
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> populateVersion(final Map<String, Object> swaggerYaml) {
        if (swaggerYaml.containsKey("info")) {
            var info = (Map<String, Object>) swaggerYaml.get("info");
            info.put("version", VersionUtils.getAppVersion());
        }
        return swaggerYaml;
    }

    private JsonNode loadJson(final ObjectMapper objectMapper,
                              final ResourceLoader resourceLoader,
                              final String location) throws IOException {
        return objectMapper.readTree(resourceLoader.getResource(location).getInputStream());
    }

}
