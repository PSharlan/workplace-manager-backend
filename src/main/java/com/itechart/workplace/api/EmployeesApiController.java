package com.itechart.workplace.api;

import com.itechart.workplace.generated.api.EmployeesApi;
import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.generated.model.Place;
import com.itechart.workplace.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
public class EmployeesApiController implements EmployeesApi {

    private final EmployeeService employeeService;

    @Override
    public ResponseEntity<List<Employee>> getAllEmployees(@Valid @RequestParam(required = false) final String
                                                                  department,
                                                          @Valid @RequestParam(required = false) final String
                                                                  username) {
        var employees = employeeService.getAllByUsernameAndDepartment(username, department);
        return new ResponseEntity<>(employees, OK);
    }

    @Override
    public ResponseEntity<Place> getEmployeePlace(@PathVariable final UUID id) {
        var place = employeeService.getPlace(id);
        return new ResponseEntity<>(place, OK);
    }

}
