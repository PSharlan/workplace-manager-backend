package com.itechart.workplace.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "office")
@EqualsAndHashCode(callSuper = false, of = "id")
public class OfficeEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "address")
    private String address;

    @Column(name = "imageLink")
    private String imageLink;

    @Column(name = "visio_link", nullable = false)
    private String visioLink;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @OneToMany(fetch = LAZY, mappedBy = "office", cascade = ALL)
    private List<RoomEntity> rooms;

}
