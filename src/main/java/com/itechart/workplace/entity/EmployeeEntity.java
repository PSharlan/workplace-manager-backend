package com.itechart.workplace.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee")
@EqualsAndHashCode(callSuper = false, of = "id")
public class EmployeeEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @Column(name = "department")
    private String department;

    @Column(name = "department_group")
    private String group;

    @Column(name = "localized_name", nullable = false)
    private String localizedName;

    @Column(name = "location")
    private String location;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "smg_id", nullable = false)
    private String smgId;

    @Column(name = "image_link", nullable = false)
    private String imageLink;

    @OneToOne(fetch = LAZY, mappedBy = "employee")
    private PlaceEntity place;

}
