package com.itechart.workplace.entity;

import com.itechart.workplace.entity.enums.RequestDetailsStatusEntity;
import com.itechart.workplace.entity.enums.RequestDetailsTypeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "request_details")
@EqualsAndHashCode(callSuper = false, of = "id")
public class RequestDetailsEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "request_id", referencedColumnName = "id")
    private RequestEntity request;

    @ManyToOne
    @JoinColumn(name = "place_from_id", referencedColumnName = "id", nullable = false)
    private PlaceEntity placeFrom;

    @ManyToOne
    @JoinColumn(name = "place_to_id", referencedColumnName = "id", nullable = false)
    private PlaceEntity placeTo;

    @Enumerated(STRING)
    @Type(type = "enumType")
    @Column(name = "type", nullable = false)
    private RequestDetailsTypeEntity type;

    @Enumerated(STRING)
    @Type(type = "enumType")
    @Column(name = "status", nullable = false)
    private RequestDetailsStatusEntity status;

    @OneToMany(fetch = LAZY, mappedBy = "requestDetails", cascade = ALL)
    private List<RequestApproveEntity> requestApproves;

}
