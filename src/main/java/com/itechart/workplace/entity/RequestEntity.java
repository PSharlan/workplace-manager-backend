package com.itechart.workplace.entity;

import com.itechart.workplace.entity.enums.RequestStatusEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "request")
@EqualsAndHashCode(callSuper = false, of = "id")
public class RequestEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "closed_by_id", referencedColumnName = "id", nullable = false)
    private UserEntity closedBy;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "requester_id", referencedColumnName = "id", nullable = false)
    private UserEntity requester;

    @Enumerated(STRING)
    @Type(type = "enumType")
    @Column(name = "status", nullable = false)
    private RequestStatusEntity status;

    @OneToMany(fetch = LAZY, mappedBy = "request", cascade = ALL)
    private List<RequestDetailsEntity> requestsDetails;

}
