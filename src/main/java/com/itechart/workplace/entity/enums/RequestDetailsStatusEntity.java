package com.itechart.workplace.entity.enums;

public enum RequestDetailsStatusEntity {

    APPROVED,
    PENDING,
    REJECTED

}
