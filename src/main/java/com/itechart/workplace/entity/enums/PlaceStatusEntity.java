package com.itechart.workplace.entity.enums;

public enum PlaceStatusEntity {

    AVAILABLE,
    REQUESTED

}
