package com.itechart.workplace.entity.enums;

public enum RequestDetailsTypeEntity {

    MOVE,
    REMOVE,
    PLACE

}
