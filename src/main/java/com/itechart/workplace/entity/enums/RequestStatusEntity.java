package com.itechart.workplace.entity.enums;

public enum RequestStatusEntity {

    APPROVED,
    COMPLETED,
    PENDING,
    REJECTED

}
