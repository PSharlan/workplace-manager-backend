package com.itechart.workplace.entity;

import com.itechart.workplace.entity.enums.RequestDetailsStatusEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "request_approve")
@EqualsAndHashCode(callSuper = false, of = "id")
public class RequestApproveEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "approver_id", referencedColumnName = "id", nullable = false)
    private UserEntity approver;

    @ManyToOne
    @JoinColumn(name = "request_details_id", referencedColumnName = "id")
    private RequestDetailsEntity requestDetails;

    @Enumerated(STRING)
    @Type(type = "enumType")
    @Column(name = "status")
    private RequestDetailsStatusEntity status;

}
