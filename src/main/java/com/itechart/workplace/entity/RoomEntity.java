package com.itechart.workplace.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.AUTO;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "room")
@ (name = "room_view")
@EqualsAndHashCode(callSuper = false, of = "id")
public class RoomEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "manager_id", referencedColumnName = "id")
    private EmployeeEntity manager;

    @ManyToOne
    @JoinColumn(name = "office_id", referencedColumnName = "id")
    private OfficeEntity office;

    @Column(name = "floor_number", nullable = false)
    private Integer floorNumber;

    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "total_places", nullable = false)
    private Integer totalPlaces;

    @Column(name = "visio_page_id", nullable = false)
    private Integer visioPageId;

    @Column(name = "svg_link")
    private String svgLink;

    @Column(name = "free_places", table = "room_view", insertable = false, updatable = false)
    private Long freePlaces;

    @OneToMany(fetch = LAZY, mappedBy = "room", cascade = ALL)
    private List<PlaceEntity> places;

}
