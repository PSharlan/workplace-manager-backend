package com.itechart.workplace.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileStorageConfiguration {

    @Bean
    public AmazonS3 s3Client(final AWSCredentialsProvider awsCredentialsProvider,
                             final AppConfiguration appConfiguration) {
        var region = Regions.US_EAST_1.getName();
        var s3ClientBuilder = AmazonS3ClientBuilder.standard();
        var storageUrl = appConfiguration.getAws().getS3().getStorageUrl();

        /*
         * Storage URL is not a required parameter
         * AWS SDK can provide URL based on region
         * Endpoint overriding here solves the problem with simulating AWS S3 via MinIO
         */

        if (StringUtils.isEmpty(storageUrl)) {
            s3ClientBuilder.setRegion(region);
        } else {
            s3ClientBuilder.withPathStyleAccessEnabled(true)
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(storageUrl, region));
        }

        s3ClientBuilder.withCredentials(awsCredentialsProvider);
        return s3ClientBuilder.build();
    }

    @Bean
    public AWSCredentialsProvider awsCredentialsProvider(final AppConfiguration appConfiguration) {
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(
                        appConfiguration.getAws().getAccessKeyId(),
                        appConfiguration.getAws().getSecretKey()));
    }

}
