package com.itechart.workplace.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@Getter
@Component
@EnableConfigurationProperties
@SuppressWarnings("PMD")
@ConfigurationProperties(prefix = "app", ignoreUnknownFields = false)
public class AppConfiguration {

    private Aes aes = new Aes();
    private Jwt jwt = new Jwt();
    private Smg smg = new Smg();
    private Vsdx vsdx = new Vsdx();
    private Aws aws = new Aws();

    @Getter
    @Setter
    public static class Jwt {

        @NotBlank
        private String base64Secret;

        private long accessTokenValidity;

        private long refreshTokenValidity;

    }

    @Getter
    @Setter
    public static class Aes {

        @NotBlank
        private String initVector;

        @NotBlank
        private String secretKey;

    }

    @Getter
    @Setter
    public static class Smg {

        @NotBlank
        private String username;

        @NotBlank
        private String password;

        @NotBlank
        private String apiUrl;

    }

    @Getter
    @Setter
    public static class Vsdx {

        @NotBlank
        private String folder;

        @NotBlank
        private String sourceImagesFolder;

        @NotBlank
        private String generatedImagesFolder;

    }

    @Getter
    @Setter
    public static class Aws {

        private String accessKeyId;

        private String secretKey;

        private S3 s3 = new S3();

    }

    @Getter
    @Setter
    public static class S3 {

        private String bucketName;

        private String storageUrl;

    }

}
