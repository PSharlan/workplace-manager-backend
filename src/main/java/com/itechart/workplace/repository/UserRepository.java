package com.itechart.workplace.repository;

import com.itechart.workplace.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    @Override
    @Query(value = "SELECT * FROM workplace_user "
            + "WHERE id = :id "
            + "AND deleted_at IS NULL ",
            nativeQuery = true)
    Optional<UserEntity> findById(@Param("id") UUID id);

    @Query(value = "SELECT * FROM workplace_user "
            + "WHERE username = :username "
            + "AND deleted_at IS NULL ",
            nativeQuery = true)
    Optional<UserEntity> findByUsername(@Param("username") String username);

    @Modifying
    @Query(value = "UPDATE workplace_user "
            + "SET encrypted_password = :encryptedPassword "
            + "WHERE id = :id ",
            nativeQuery = true)
    void updatePasswordById(@Param("id") UUID id,
                            @Param("encryptedPassword") String encryptedPassword);

}
