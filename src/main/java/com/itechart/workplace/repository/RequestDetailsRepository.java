package com.itechart.workplace.repository;

import com.itechart.workplace.entity.RequestDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RequestDetailsRepository extends JpaRepository<RequestDetailsEntity, UUID> {
}
