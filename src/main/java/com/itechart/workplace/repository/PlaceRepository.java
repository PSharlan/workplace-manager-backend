package com.itechart.workplace.repository;

import com.itechart.workplace.entity.PlaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PlaceRepository extends JpaRepository<PlaceEntity, UUID> {

    @Query(value = "SELECT * FROM place "
            + "WHERE deleted_at IS NULL"
            + " AND room_id = :roomId",
            nativeQuery = true)
    List<PlaceEntity> findAllByRoomId(@Param("roomId") UUID roomId);

    @Query(value = "SELECT * FROM place "
            + "WHERE deleted_at IS NULL"
            + " AND room_id = :roomId"
            + " AND (:numbers = '{}' OR number = ANY(cast(:numbers AS INTEGER [])))",
            nativeQuery = true)
    List<PlaceEntity> findAllByRoomIdAndNumbers(@Param("roomId") UUID roomId, @Param("numbers") String numbers);

    @Query(value = "SELECT *"
            + "FROM place"
            + "         INNER JOIN office ON (:location = '' OR office.location = :location)"
            + "         INNER JOIN room ON office.id = room.office_id AND (:roomNumber = '' "
            + "     OR room.number = :roomNumber)"
            + "WHERE place.deleted_at IS NULL"
            + "  AND room_id = room.id"
            + "  AND (:number = 0 OR place.number = :number)",
            nativeQuery = true)
    List<PlaceEntity> findByLocationAndRoomAndNumber(@Param("location") String location,
                                                     @Param("roomNumber") String roomNumber,
                                                     @Param("number") Integer number);

}
