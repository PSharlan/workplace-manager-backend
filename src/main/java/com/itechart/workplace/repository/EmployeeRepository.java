package com.itechart.workplace.repository;

import com.itechart.workplace.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, UUID> {

    @Query(value = "SELECT DISTINCT department_group "
            + "FROM employee "
            + "WHERE deleted_at IS NULL"
            + "  AND department = :department",
            nativeQuery = true)
    List<String> findAllDepartmentGroups(@Param("department") String department);

    @Query(value = "SELECT DISTINCT department "
            + "FROM employee "
            + "WHERE deleted_at IS NULL"
            + "  AND department IS NOT NULL",
            nativeQuery = true)
    List<String> findAllDepartments();

    @Query(value = "SELECT * "
            + "FROM employee "
            + "WHERE deleted_at IS NULL"
            + "  AND (:name = '' OR (:department = '' OR department = :department)"
            + "  AND SIMILARITY(METAPHONE(name, 20), METAPHONE(:name, 20)) >= 0.5"
            + "  AND (SELECT max(SIMILARITY(METAPHONE(name, 20), METAPHONE(:name, 20)))"
            + "       FROM employee"
            + "       WHERE deleted_at IS NULL"
            + "         AND (:department = '' OR department = :department)) -"
            + "      (SIMILARITY(METAPHONE(name, 20), METAPHONE(:name, 20))) <= 0.2) "
            + "ORDER BY SIMILARITY("
            + "                 METAPHONE(name, 20),"
            + "                 METAPHONE(:name, 20)"
            + "             ) DESC",
            nativeQuery = true)
    List<EmployeeEntity> findAllByNameAndDepartment(@Param("name") String name, @Param("department") String department);

    @Query(value = "SELECT * FROM employee "
            + "WHERE smg_id IN :smgIds ",
            nativeQuery = true)
    List<EmployeeEntity> findBySmgIds(@Param("smgIds") Collection<String> smgIds);

}
