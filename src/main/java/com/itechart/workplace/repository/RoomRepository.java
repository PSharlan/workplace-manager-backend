package com.itechart.workplace.repository;

import com.itechart.workplace.entity.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, UUID> {

    @Override
    @Query(value = "SELECT * FROM room_view "
            + "WHERE deleted_at IS NULL"
            + " AND id = :id ",
            nativeQuery = true)
    Optional<RoomEntity> findById(@Param("id") UUID id);

    @Query(value = "SELECT * FROM room_view "
            + "WHERE deleted_at IS NULL"
            + " AND office_id = :officeId "
            + "ORDER BY floor_number, number",
            nativeQuery = true)
    List<RoomEntity> findAllByOfficeId(@Param("officeId") UUID officeId);

    @Query(value = "SELECT CASE "
            + "WHEN EXISTS(SELECT * FROM room_view "
            + "WHERE deleted_at IS NULL"
            + " AND number = :number"
            + " AND office_id = :officeId) THEN TRUE "
            + "ELSE FALSE END",
            nativeQuery = true)
    boolean existsByOfficeIdAndNumber(@Param("officeId") UUID officeId, @Param("number") String number);

    @Modifying
    @Query(value = "DELETE FROM room "
            + "WHERE deleted_at IS NULL"
            + " AND number = :number"
            + " AND office_id = :officeId",
            nativeQuery = true)
    void deleteByOfficeIdAndNumber(@Param("officeId") UUID officeId, @Param("number") String number);

    @Override
    @Modifying
    @Query(value = "DELETE FROM room "
            + "WHERE deleted_at IS NULL"
            + " AND id = :id",
            nativeQuery = true)
    void deleteById(@Param("id") UUID id);

}
