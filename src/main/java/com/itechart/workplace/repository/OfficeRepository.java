package com.itechart.workplace.repository;

import com.itechart.workplace.entity.OfficeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OfficeRepository extends JpaRepository<OfficeEntity, UUID> {

    @Override
    @Query(value = "SELECT CASE "
            + "WHEN EXISTS(SELECT * "
            + "FROM office  "
            + "WHERE deleted_at IS NULL "
            + " AND id = :id) THEN TRUE "
            + "ELSE FALSE END ",
            nativeQuery = true)
    boolean existsById(@Param("id") UUID id);

    @Override
    @Query(value = "SELECT * FROM office "
            + "WHERE deleted_at IS NULL "
            + "ORDER BY location",
            nativeQuery = true)
    List<OfficeEntity> findAll();

    @Override
    @Query(value = "SELECT * FROM office "
            + "WHERE deleted_at IS NULL"
            + " AND id = :id ",
            nativeQuery = true)
    Optional<OfficeEntity> findById(@Param("id") UUID id);

    @Query(value = "SELECT CASE "
            + "WHEN EXISTS(SELECT * FROM office "
            + "WHERE deleted_at IS NULL"
            + " AND location = :location) THEN TRUE "
            + "ELSE FALSE END",
            nativeQuery = true)
    boolean existsByLocation(@Param("location") String location);

    @Modifying
    @Query(value = "DELETE FROM office CASCADE "
            + "WHERE deleted_at IS NULL"
            + " AND location = :location",
            nativeQuery = true)
    void deleteByLocation(@Param("location") String location);

    @Override
    @Modifying
    @Query(value = "DELETE FROM office CASCADE "
            + "WHERE deleted_at IS NULL"
            + " AND id = :id",
            nativeQuery = true)
    void deleteById(@Param("id") UUID id);

}
