package com.itechart.workplace.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * This event indicates that {@code VSDx} file parsing has been completed.
 *
 * @author D.Delmukhambetov
 * @since 03.03.2020
 */
@Getter
public class VsdxParsingCompletionEvent extends ApplicationEvent {

    private final String path;

    /**
     * Create a new {@code VsdxParsingCompletionEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     * @param path where file parsing outcomes (SVG images) are located
     */
    public VsdxParsingCompletionEvent(final Object source, final String path) {
        super(source);
        this.path = path;
    }
}
