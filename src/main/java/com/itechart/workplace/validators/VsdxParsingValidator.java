package com.itechart.workplace.validators;

import com.itechart.workplace.generated.model.Violation;

import java.util.List;

public interface VsdxParsingValidator {

    void addViolation(Violation violation);

    void addViolations(List<Violation> violations);

    void throwIfHasWarnings();

}
