package com.itechart.workplace.validators.impl;

import com.itechart.workplace.exception.UnprocessableEntityException;
import com.itechart.workplace.generated.model.Violation;
import com.itechart.workplace.validators.VsdxParsingValidator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

@Service(SCOPE_REQUEST)
public class VsdxParsingValidatorImpl implements VsdxParsingValidator {

    private final List<Violation> violations = new ArrayList<>();

    @Override
    public void addViolation(final Violation violation) {
        this.violations.add(violation);
    }

    @Override
    public void addViolations(final List<Violation> violations) {
        this.violations.addAll(violations);
    }

    @Override
    public void throwIfHasWarnings() {
        var violations = new ArrayList<>(this.violations);
        if (!violations.isEmpty()) {
            this.violations.clear();
            throw new UnprocessableEntityException("Warnings while parsing file", violations);
        }
    }

}
