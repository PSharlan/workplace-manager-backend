package com.itechart.workplace.validators.impl;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.entity.PlaceEntity;
import com.itechart.workplace.exception.BadRequestException;
import com.itechart.workplace.generated.model.CreateRequestDetails;
import com.itechart.workplace.validators.RequestValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static com.itechart.workplace.generated.model.RequestDetailsType.MOVE;
import static com.itechart.workplace.generated.model.RequestDetailsType.PLACE;
import static com.itechart.workplace.generated.model.RequestDetailsType.REMOVE;
import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestValidatorImpl implements RequestValidator {

    @Override
    public void validatePlaceFrom(final PlaceEntity from, final EmployeeEntity employee) {
        if (from.getEmployee() == null) {
            throw new BadRequestException(format("Employee with id %s isn't on place with id %s", employee.getId(),
                    from.getId()));
        }
    }

    @Override
    public void validatePlaceTo(final PlaceEntity to) {
        if (to.getEmployee() != null) {
            throw new BadRequestException(format("Place with id %s is not free", to.getId()));
        }
    }

    @Override
    public void validateRequestDetail(final CreateRequestDetails detail) {
        var type = detail.getType();
        if (!(type.equals(REMOVE) && detail.getPlaceFromId() != null && detail.getPlaceToId() == null
                || type.equals(MOVE) && detail.getPlaceFromId() != null && detail.getPlaceToId() != null
                || type.equals(PLACE) && detail.getPlaceFromId() == null && detail.getPlaceToId() != null)) {
            throw new BadRequestException("Incorrect input parameters: type of request don't match given places");
        }
    }

}
