package com.itechart.workplace.validators;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.entity.PlaceEntity;
import com.itechart.workplace.generated.model.CreateRequestDetails;

public interface RequestValidator {

    void validatePlaceFrom(PlaceEntity from, EmployeeEntity employee);

    void validatePlaceTo(PlaceEntity to);

    void validateRequestDetail(CreateRequestDetails detail);

}
