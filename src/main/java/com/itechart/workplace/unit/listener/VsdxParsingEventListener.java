package com.itechart.workplace.unit.listener;

import com.itechart.workplace.event.VsdxParsingCompletionEvent;
import com.itechart.workplace.service.impl.FileSyncVisitor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * VSDx file parsing events listener provides
 * files sync with storage.
 *
 * @author D.Delmukhambetov
 * @since 04.03.2020
 */
@Component
@RequiredArgsConstructor
public class VsdxParsingEventListener {

    private final FileSyncVisitor fileSyncVisitor;

    @SneakyThrows
    @TransactionalEventListener(VsdxParsingCompletionEvent.class)
    public void onParsingCompletionEvent(final VsdxParsingCompletionEvent event) {

        if (Objects.isNull(event) || StringUtils.isBlank(event.getPath())) {
            return;
        }

        Files.walkFileTree(Paths.get(event.getPath()), this.fileSyncVisitor);
    }

}
