package com.itechart.workplace.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

@UtilityClass
public class AesEncryptionUtils {

    private static final String AES = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5PADDING";

    private static final Base64.Decoder DECODER = Base64.getDecoder();
    private static final Base64.Encoder ENCODER = Base64.getEncoder();

    @SneakyThrows
    public static String encryptData(final String data, final String secretKey, final String initVector) {
        var ivParameterSpec = new IvParameterSpec(DECODER.decode(initVector));
        var secretKeySpec = new SecretKeySpec(DECODER.decode(secretKey), AES);
        var cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        return new String(ENCODER.encode(cipher.doFinal(data.getBytes())), UTF_8);
    }

    @SneakyThrows
    public static String decryptData(final String encryptedData, final String secretKey, final String initVector) {
        var ivParameterSpec = new IvParameterSpec(DECODER.decode(initVector));
        var secretKeySpec = new SecretKeySpec(DECODER.decode(secretKey), AES);
        var cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
        return new String(cipher.doFinal(DECODER.decode(encryptedData)), UTF_8);
    }

}
