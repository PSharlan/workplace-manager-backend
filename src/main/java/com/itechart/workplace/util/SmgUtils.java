package com.itechart.workplace.util;

import com.itechart.workplace.entity.EmployeeEntity;
import lombok.experimental.UtilityClass;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.net.URLEncoder;
import java.net.http.HttpHeaders;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.itechart.workplace.mapper.EmployeeMapper.EMPLOYEE_MAPPER;
import static java.lang.String.join;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.reverse;

@UtilityClass
public class SmgUtils {

    private static final String EMPLOYEE_ROW_CLASS_NAME = "dxgvDataRow_SMG";
    private static final String INPUT_TAG_NAME = "input";
    private static final String ID_ATTRIBUTE_KEY = "id";
    private static final String VALUE_ATTRIBUTE_KEY = "value";
    private static final String NAME_ATTRIBUTE_KEY = "name";
    private static final String VERIFICATION_TOKEN_KEY = "__RequestVerificationToken";

    public static String prepareLoginParameters(final String htmlDocument,
                                                final String username,
                                                final String password) {
        var verificationToken = Jsoup.parse(htmlDocument).getElementsByTag(INPUT_TAG_NAME).stream()
                .filter(element -> element.attr(NAME_ATTRIBUTE_KEY).equals(VERIFICATION_TOKEN_KEY))
                .map(element -> element.attr(VALUE_ATTRIBUTE_KEY))
                .findFirst()
                .orElse("");

        return String.join("&",
                prepareParameter(VERIFICATION_TOKEN_KEY, verificationToken),
                prepareParameter("DomainName", username),
                prepareParameter("Password", password),
                prepareParameter("KeepLoggedIn", "True"));
    }

    public static List<EmployeeEntity> retrieveEmployeeProfiles(final String htmlDocument) {
        return Jsoup.parse(htmlDocument).getElementsByClass(EMPLOYEE_ROW_CLASS_NAME)
                .stream()
                .filter(element -> element.attributes().asList()
                        .stream()
                        .anyMatch(attribute -> attribute.getKey().equals(ID_ATTRIBUTE_KEY)))
                .map(row -> row.children()
                        .stream()
                        .collect(Collectors.toMap(
                                Element::siblingIndex,
                                value -> row.child(value.siblingIndex() - 1).text())))
                .map(EMPLOYEE_MAPPER::toEntity)
                .collect(Collectors.toList());
    }

    public static String prepareDisplayName(final String username) {
        var usernameArray = username.split(Pattern.quote("."));
        reverse(asList(usernameArray));
        return join(" ", usernameArray);
    }

    public static String findRequestVerificationToken(final HttpHeaders headers) {
        return headers.allValues("set-cookie").stream()
                .filter(value -> value.contains(VERIFICATION_TOKEN_KEY))
                .findFirst()
                .orElse("");
    }

    private static String prepareParameter(final String key, final String value) {
        return URLEncoder.encode(key, UTF_8) + "=" + URLEncoder.encode(value, UTF_8);
    }

}
