package com.itechart.workplace.util;

import com.itechart.workplace.entity.enums.UserRoleEntity;
import com.itechart.workplace.exception.NotFoundException;
import com.itechart.workplace.security.impl.UserDetailsImpl;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@UtilityClass
public class SecurityUtils {

    public static Optional<UserDetailsImpl> getUserDetails() {
        return Optional.ofNullable(getContext().getAuthentication())
                .filter(authentication -> authentication.getPrincipal() instanceof UserDetails)
                .map(authentication -> (UserDetailsImpl) authentication.getPrincipal());
    }

    public static @NotNull UUID getCurrentUserIdOrException() {
        return getUserDetails()
                .map(UserDetailsImpl::getUserId)
                .orElseThrow(() -> new NotFoundException("User could not be found"));
    }

    public static @NotNull String getCurrentDisplayNameOrException() {
        return getUserDetails()
                .map(UserDetailsImpl::getDisplayName)
                .orElseThrow(() -> new NotFoundException("User could not be found"));
    }

    public static @NotNull UserRoleEntity getCurrentRoleOrException() {
        return getUserDetails()
                .map(UserDetailsImpl::getRole)
                .orElseThrow(() -> new NotFoundException("User could not be found"));
    }

}
