package com.itechart.workplace.service;

import com.itechart.workplace.entity.OfficeEntity;
import com.itechart.workplace.generated.model.Office;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

public interface OfficeService {

    Office importFromVsdx(MultipartFile vsdxImage, String location,
                          List<Double> coordinates,  Boolean ignoreWarnings);

    OfficeEntity getById(UUID officeId);

    List<Office> getAll();

    void delete(UUID id);

}
