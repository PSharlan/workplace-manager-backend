package com.itechart.workplace.service;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.generated.model.Place;

import java.util.List;
import java.util.UUID;

public interface EmployeeService {

    List<Employee> getAllByUsernameAndDepartment(String username, String department);

    List<String> getDepartmentGroups(String department);

    List<String> getDepartments();

    EmployeeEntity getById(UUID id);

    long synchronizeEmployee(List<EmployeeEntity> employees);

    Place getPlace(UUID id);

}
