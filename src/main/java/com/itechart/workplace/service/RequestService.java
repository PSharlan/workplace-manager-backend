package com.itechart.workplace.service;

import com.itechart.workplace.generated.model.CreateRequest;
import com.itechart.workplace.generated.model.Request;

public interface RequestService {

    Request create(CreateRequest body);

}
