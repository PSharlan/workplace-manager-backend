package com.itechart.workplace.service;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.entity.RequestDetailsEntity;

public interface RequestResolverService {

    void performRequestDetail(RequestDetailsEntity entity, EmployeeEntity employee);

}
