package com.itechart.workplace.service;

import com.itechart.workplace.generated.model.LoginRequest;
import com.itechart.workplace.generated.model.Token;

public interface AuthService {

    Token login(LoginRequest loginRequest);

    Token refresh(String refreshToken);

}
