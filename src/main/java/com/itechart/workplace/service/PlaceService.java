package com.itechart.workplace.service;

import com.itechart.workplace.entity.PlaceEntity;
import com.itechart.workplace.generated.model.Place;

import java.util.List;
import java.util.UUID;

public interface PlaceService {

    List<Place> getPlacesByLocationAndRoomAndNumber(String location, String roomNumber, Integer number);

    PlaceEntity getById(UUID id);

}
