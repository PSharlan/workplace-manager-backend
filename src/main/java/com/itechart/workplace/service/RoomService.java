package com.itechart.workplace.service;

import com.itechart.workplace.generated.model.Room;
import com.itechart.workplace.model.FileStorageObject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

public interface RoomService {

    Room getById(UUID id);

    Room importFromVsdx(UUID officeId, MultipartFile vsdxImage, Boolean ignoreWarnings);

    Room updateAttributes(UUID id, UUID newManagerId);

    List<Room> getAllByOfficeId(UUID id);

    void delete(UUID id);

    FileStorageObject getSvgImage(UUID id);

}
