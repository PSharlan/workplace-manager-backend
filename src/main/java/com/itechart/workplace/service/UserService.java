package com.itechart.workplace.service;

import com.itechart.workplace.entity.UserEntity;
import com.itechart.workplace.generated.model.LoginRequest;
import com.itechart.workplace.generated.model.User;

import java.util.UUID;

public interface UserService {

    User getCurrent();

    UserEntity getUserEntityById(UUID id);

    UserEntity getOrCreateByLoginRequest(LoginRequest loginRequest, String smgWebAuth);

    void updatePasswordIfChanged(UUID id, String newPassword);

}
