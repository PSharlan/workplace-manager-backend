package com.itechart.workplace.service.impl;

import com.itechart.workplace.client.SmgClient;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.exception.UnauthorizedException;
import com.itechart.workplace.generated.model.LoginRequest;
import com.itechart.workplace.generated.model.Token;
import com.itechart.workplace.security.TokenProvider;
import com.itechart.workplace.security.TokenValidator;
import com.itechart.workplace.service.AuthService;
import com.itechart.workplace.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

import static com.itechart.workplace.mapper.UserMapper.USER_MAPPER;
import static com.itechart.workplace.util.AesEncryptionUtils.decryptData;
import static com.itechart.workplace.util.AesEncryptionUtils.encryptData;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserService userService;

    private final AppConfiguration appConfiguration;
    private final SmgClient smgClient;
    private final TokenProvider tokenProvider;
    private final TokenValidator tokenValidator;

    private String initVector;
    private String secretKey;

    @PostConstruct
    public void init() {
        initVector = appConfiguration.getAes().getInitVector();
        secretKey = appConfiguration.getAes().getSecretKey();
    }

    @Override
    @Transactional
    public Token login(final LoginRequest loginRequest) {
        var username = loginRequest.getUsername();
        var password = loginRequest.getPassword();
        var encryptedPassword = encryptData(loginRequest.getPassword(), secretKey, initVector);

        var smgWebAuth = smgClient.login(username, password);
        var userEntity = userService.getOrCreateByLoginRequest(loginRequest, smgWebAuth);
        userService.updatePasswordIfChanged(userEntity.getId(), encryptedPassword);
        var userDetails = USER_MAPPER.toUserDetails(userEntity.getId(), userEntity.getRole(),
                userEntity.getDisplayName(), smgWebAuth);
        return tokenProvider.createToken(userDetails);
    }

    @Override
    @Transactional
    public Token refresh(final String refreshToken) {
        if (!tokenValidator.validateToken(refreshToken)) {
            throw new UnauthorizedException("Invalid refresh token");
        }
        var userId = tokenProvider.getUserIdByToken(refreshToken);
        var userEntity = userService.getUserEntityById(userId);
        var password = decryptData(userEntity.getEncryptedPassword(), secretKey, initVector);
        var smgWebAuth = smgClient.login(userEntity.getUsername(), password);
        return tokenProvider.refreshToken(refreshToken, smgWebAuth);
    }

}
