package com.itechart.workplace.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.model.AmazonS3Object;
import com.itechart.workplace.model.FileStorageObject;
import com.itechart.workplace.service.FileStorageService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * This class provides {@link FileStorageService} implementation
 * which uses Amazon Simple Storage Service (S3) as a storage.
 *
 * @author D.Delmukhambetov
 * @since 18.02.2020
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class AmazonS3FileStorageService implements FileStorageService {

    private final AmazonS3 s3Client;

    private final AppConfiguration appConfiguration;

    private String bucketName;

    @PostConstruct
    public void init() {
        this.bucketName = this.appConfiguration.getAws().getS3().getBucketName();

        if (!this.s3Client.doesBucketExistV2(bucketName)) {
            this.s3Client.createBucket(this.bucketName);
        }
    }

    @Override
    public void write(final MultipartFile fileToSave) throws IOException {
        var filename = fileToSave.getOriginalFilename();

        if (isBlank(filename)) {
            return;
        }

        try (var fileInputStream = fileToSave.getInputStream()) {
            this.doWrite(filename,
                    fileInputStream,
                    fileToSave.getSize(),
                    fileToSave.getContentType());
        }
    }

    /**
     * Saves file into file storage.
     *
     * @param filepath path to file to save
     * @param filename name with which file should be saved in a storage
     *                 (can be different than actual one)
     */
    @Override
    public void write(final Path filepath, final String filename) throws IOException {
        try (var fileInputStream = Files.newInputStream(filepath)) {
            this.doWrite(filename,
                    fileInputStream,
                    Files.size(filepath),
                    Files.probeContentType(filepath));
        }
    }

    /**
     * Saves file into file storage.
     *
     * <p>!!! Important: pay attention onto {@code InputStream fileInputStream} parameter,
     *     you should pass it in a scope of {@code try-with-resources} block from external invocation point
     *
     * <p/>
     *
     */
    private void doWrite(final String filename, final InputStream fileInputStream,
                         final long size, final String contentType) {
        var metadata = new ObjectMetadata();
        metadata.setContentLength(size);
        metadata.setContentType(contentType);

        /*
        * Replace Windows-specific delimiter ('\') with Unix-specific ('/')
        * because S3 can create directory hierarchy based on Unix format.
        *
        * See details {@link https://docs.aws.amazon.com/AmazonS3/latest/user-guide/using-folders.html}
        */
        var filepath = FilenameUtils.separatorsToUnix(filename);

        var putObjectRequest = new PutObjectRequest(bucketName,
                filepath, fileInputStream, metadata);

        s3Client.putObject(putObjectRequest);
    }

    @Override
    @SneakyThrows
    public FileStorageObject read(final String filePath) {
        var getObjectRequest = new GetObjectRequest(bucketName, filePath);
        final var s3Object = s3Client.getObject(getObjectRequest);
        return new AmazonS3Object(s3Object);
    }

    @Override
    public void delete(final String filePath) {
        var filename = Paths.get(filePath)
                .getFileName()
                .toString();
        var deleteObjectRequest = new DeleteObjectRequest(bucketName, filename);

        s3Client.deleteObject(deleteObjectRequest);
    }

    @Override
    public void rename(final String filePath, final String newFileName) {

        if (StringUtils.equals(filePath, newFileName)) {
            return;
        }

        var filename = Paths.get(filePath)
                .getFileName()
                .toString();

        CopyObjectRequest copyObjectRequest = new CopyObjectRequest(bucketName, filename, bucketName, newFileName);
        s3Client.copyObject(copyObjectRequest);

        delete(filename);
    }

    @Override
    public boolean exists(final String filePath) {
        if (isBlank(filePath)) {
            return false;
        }

        final ListObjectsV2Result listObjectsV2Result = s3Client.listObjectsV2(bucketName);
        final List<S3ObjectSummary> summaries = listObjectsV2Result.getObjectSummaries();
        return summaries.stream()
                .map(S3ObjectSummary::getKey)
                .anyMatch(filePath::equals);
    }
}
