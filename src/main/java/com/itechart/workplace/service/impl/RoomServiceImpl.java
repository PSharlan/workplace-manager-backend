package com.itechart.workplace.service.impl;

import com.itechart.workplace.entity.RoomEntity;
import com.itechart.workplace.exception.NotFoundException;
import com.itechart.workplace.generated.model.Room;
import com.itechart.workplace.model.FileStorageObject;
import com.itechart.workplace.repository.RoomRepository;
import com.itechart.workplace.service.EmployeeService;
import com.itechart.workplace.service.FileStorageService;
import com.itechart.workplace.service.OfficeService;
import com.itechart.workplace.service.RoomService;
import com.itechart.workplace.validators.VsdxParsingValidator;
import com.itechart.workplace.vsdx.service.VsdxResolverService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

import static com.itechart.workplace.exception.ErrorConstants.NOT_FOUND_MESSAGE;
import static com.itechart.workplace.mapper.RoomMapper.ROOM_MAPPER;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;

    private final EmployeeService employeeService;
    private final VsdxResolverService vsdxResolverService;

    private final FileStorageService fileStorageService;
    private final OfficeService officeService;
    private final VsdxParsingValidator vsdxParsingValidator;

    @Override
    @Transactional(readOnly = true)
    public Room getById(final UUID id) {
        return roomRepository.findById(id).map(ROOM_MAPPER::toModel)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "room", id)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Room> getAllByOfficeId(final UUID officeId) {
        var office = officeService.getById(officeId);
        return roomRepository.findAllByOfficeId(office.getId()).stream().map(ROOM_MAPPER::toPreviewModel)
                .collect(toList());
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        roomRepository.deleteById(id);
        log.info("Room with id {} was deleted", id);
    }

    @Override
    @Transactional(readOnly = true)
    public FileStorageObject getSvgImage(final UUID id) {
        var room = roomRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "room", id)));
        return fileStorageService.read(room.getSvgLink());
    }

    @Override
    @Transactional
    public Room importFromVsdx(final UUID officeId, final MultipartFile vsdxImage, final Boolean ignoreWarnings) {
        var office = officeService.getById(officeId);
        var preparedRoom = vsdxResolverService.prepareRoom(vsdxImage, office);
        if (!ignoreWarnings) {
            vsdxParsingValidator.throwIfHasWarnings();
        }
        deleteIfExists(officeId, preparedRoom);

        var savedRoom = roomRepository.saveAndFlush(preparedRoom);
        log.info("Room with number {} in office with id {} was successfully saved", savedRoom.getNumber(), officeId);
        return ROOM_MAPPER.toModel(savedRoom);
    }

    @Override
    @Transactional
    public Room updateAttributes(final UUID id, final UUID newManagerId) {
        return roomRepository.findById(id).map(entity -> {
            var newManager = employeeService.getById(newManagerId);
            entity.setManager(newManager);
            var updatedRoom = roomRepository.save(entity);
            return ROOM_MAPPER.toPreviewModel(updatedRoom);
        }).orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "room", id)));
    }

    private void deleteIfExists(final UUID officeId, final RoomEntity parsedRoom) {
        if (roomRepository.existsByOfficeIdAndNumber(officeId, parsedRoom.getNumber())) {
            roomRepository.deleteByOfficeIdAndNumber(officeId, parsedRoom.getNumber());
            log.info("Room with number {} in office with id {} was successfully deleted", parsedRoom.getNumber(),
                    officeId);
        }
    }

}
