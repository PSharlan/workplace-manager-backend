package com.itechart.workplace.service.impl;

import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.entity.UserEntity;
import com.itechart.workplace.exception.NotFoundException;
import com.itechart.workplace.generated.model.LoginRequest;
import com.itechart.workplace.generated.model.User;
import com.itechart.workplace.repository.UserRepository;
import com.itechart.workplace.service.EmployeeService;
import com.itechart.workplace.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.UUID;

import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.exception.ErrorConstants.NOT_FOUND_MESSAGE;
import static com.itechart.workplace.mapper.UserMapper.USER_MAPPER;
import static com.itechart.workplace.util.AesEncryptionUtils.encryptData;
import static com.itechart.workplace.util.SecurityUtils.*;
import static com.itechart.workplace.util.SmgUtils.prepareDisplayName;
import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final AppConfiguration appConfiguration;

    private final UserRepository userRepository;

    private final EmployeeService employeeService;

    private String initVector;
    private String secretKey;

    @PostConstruct
    public void init() {
        initVector = appConfiguration.getAes().getInitVector();
        secretKey = appConfiguration.getAes().getSecretKey();
    }

    @Override
    public User getCurrent() {
        var userId = getCurrentUserIdOrException();
        var role = getCurrentRoleOrException();
        var displayName = getCurrentDisplayNameOrException();
        var employee = employeeService.getAllByUsernameAndDepartment(displayName, "").get(0);
        return USER_MAPPER.toModel(userId, role, employee);
    }

    @Override
    @Transactional
    public UserEntity getUserEntityById(final UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "user", id)));
    }

    @Override
    @Transactional
    public UserEntity getOrCreateByLoginRequest(final LoginRequest loginRequest, final String smgWebAuth) {
        var username = loginRequest.getUsername();
        var password = encryptData(loginRequest.getPassword(), secretKey, initVector);

        return userRepository.findByUsername(username).orElseGet(() -> {
            var displayName = prepareDisplayName(username);
            var userEntity = USER_MAPPER.toUserEntity(username, password, displayName, ADMIN);
            var saved = userRepository.saveAndFlush(userEntity);
            log.info("User with id: {} was created", saved.getId());
            return saved;
        });
    }

    @Override
    @Transactional
    public void updatePasswordIfChanged(final UUID id, final String newPassword) {
        var userEntity = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "user", id)));
        if (!userEntity.getEncryptedPassword().equals(newPassword)) {
            userRepository.updatePasswordById(id, newPassword);
            log.info("Password was updated for user with id: {}", id);
        }
    }

}
