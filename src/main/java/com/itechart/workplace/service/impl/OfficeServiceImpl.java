package com.itechart.workplace.service.impl;

import com.itechart.workplace.entity.OfficeEntity;
import com.itechart.workplace.exception.NotFoundException;
import com.itechart.workplace.generated.model.Office;
import com.itechart.workplace.repository.OfficeRepository;
import com.itechart.workplace.service.OfficeService;
import com.itechart.workplace.validators.VsdxParsingValidator;
import com.itechart.workplace.vsdx.service.VsdxResolverService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.itechart.workplace.exception.ErrorConstants.NOT_FOUND_MESSAGE;
import static com.itechart.workplace.mapper.OfficeMapper.OFFICE_MAPPER;
import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class OfficeServiceImpl implements OfficeService {

    private final OfficeRepository officeRepository;

    private final VsdxResolverService vsdxResolverService;
    private final VsdxParsingValidator vsdxParsingValidator;

    @Override
    @Transactional
    public Office importFromVsdx(final MultipartFile vsdxImage,
                                 final String location,
                                 final List<Double> coordinates,
                                 final Boolean ignoreWarnings) {
        var preparedOffice = vsdxResolverService.prepareOffice(vsdxImage, location);
        setOfficeCoordinates(preparedOffice, coordinates);

        if (!ignoreWarnings) {
            vsdxParsingValidator.throwIfHasWarnings();
        }
        deleteIfExists(location);

        var savedOffice = officeRepository.saveAndFlush(preparedOffice);
        log.info("Office with location {} was successfully saved", location);
        return OFFICE_MAPPER.toModel(savedOffice);
    }

    @Override
    @Transactional(readOnly = true)
    public OfficeEntity getById(final UUID officeId) {
        return officeRepository.findById(officeId)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "office", officeId)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Office> getAll() {
        return officeRepository.findAll().stream().map(OFFICE_MAPPER::toModel).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        officeRepository.deleteById(id);
        log.info("Office with id {} was deleted", id);
    }

    private void deleteIfExists(final String location) {
        if (officeRepository.existsByLocation(location)) {
            officeRepository.deleteByLocation(location);
            log.info("Office with location {} was deleted", location);
        }
    }

    private void setOfficeCoordinates(final OfficeEntity officeEntity, final List<Double> coordinates) {

        double latitude = 0.0;
        double longitude = 0.0;

        if (!CollectionUtils.isEmpty(coordinates)) {
            latitude = ObjectUtils.firstNonNull(coordinates.get(0), 0.0);
            longitude = ObjectUtils.firstNonNull(coordinates.get(1), 0.0);
        }

        officeEntity.setLatitude(latitude);
        officeEntity.setLongitude(longitude);
    }

}
