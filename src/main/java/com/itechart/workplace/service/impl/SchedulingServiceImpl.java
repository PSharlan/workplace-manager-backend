package com.itechart.workplace.service.impl;

import com.itechart.workplace.client.SmgClient;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.service.EmployeeService;
import com.itechart.workplace.service.SchedulingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Slf4j
@Service
@RequiredArgsConstructor
public class SchedulingServiceImpl implements SchedulingService {

    private final EmployeeService employeeService;

    private final AppConfiguration appConfiguration;
    private final SmgClient smgClient;

    private String username;
    private String password;

    @PostConstruct
    public void init() {
        username = appConfiguration.getSmg().getUsername();
        password = appConfiguration.getSmg().getPassword();
    }

    @Override
    @Transactional
    @SuppressWarnings("PMD")
    @Scheduled(fixedRate = 15 * 60 * 1000)
    public void synchronizeEmployees() {
        try {
            var smgWebAuth = smgClient.login(username, password);
            var employees = smgClient.getEmployees(smgWebAuth);
            var newEmployeesNumber = employeeService.synchronizeEmployee(employees);
            log.info("{} new employees was saved to database", newEmployeesNumber);
        } catch (Exception e) {
            log.error("Employee synchronization failed", e);
        }
    }

}
