package com.itechart.workplace.service.impl;

import com.google.common.collect.Sets;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.service.FileStorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

/**
 * This class provides methods to sync files from local filesystem with file storage
 * and implements Visitor design pattern to walk through directories.
 *
 * @author D.Delmukhambetov
 * @since 02.03.2020
 */
@Slf4j
@Component
@Scope(SCOPE_PROTOTYPE)
@RequiredArgsConstructor
public class FileSyncVisitor extends SimpleFileVisitor<Path> {

    private final FileStorageService fileStorageService;
    private final AppConfiguration appConfiguration;

    private Collection<String> syncableFileFormats = Sets.newHashSet("vsdx", "svg");

    @Override
    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {

        super.visitFile(file, attrs);

        var extension = FilenameUtils.getExtension(file.toString());
        if (!attrs.isRegularFile() || !this.syncableFileFormats.contains(extension)) {
            return FileVisitResult.CONTINUE;
        }

        var rootDirPath = Path.of(this.appConfiguration.getVsdx().getFolder());
        this.fileStorageService.write(file, rootDirPath.relativize(file).toString());
        Files.deleteIfExists(file);

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
        Files.deleteIfExists(dir);
        return super.postVisitDirectory(dir, exc);
    }

    @Override
    public FileVisitResult visitFileFailed(final Path file, final IOException exc) throws IOException {
        log.error("During the file {} sync exception {} ({}) occurred",
                file.toString(), exc.getClass().getCanonicalName(), exc.getMessage());
        return super.visitFileFailed(file, exc);
    }
}
