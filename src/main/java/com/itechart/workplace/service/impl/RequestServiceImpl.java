package com.itechart.workplace.service.impl;

import com.itechart.workplace.entity.RequestDetailsEntity;
import com.itechart.workplace.entity.RequestEntity;
import com.itechart.workplace.generated.model.CreateRequest;
import com.itechart.workplace.generated.model.CreateRequestDetails;
import com.itechart.workplace.generated.model.Request;
import com.itechart.workplace.repository.RequestRepository;
import com.itechart.workplace.service.EmployeeService;
import com.itechart.workplace.service.PlaceService;
import com.itechart.workplace.service.RequestResolverService;
import com.itechart.workplace.service.RequestService;
import com.itechart.workplace.service.UserService;
import com.itechart.workplace.validators.RequestValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.itechart.workplace.entity.enums.RequestDetailsStatusEntity.APPROVED;
import static com.itechart.workplace.generated.model.RequestStatus.COMPLETED;
import static com.itechart.workplace.mapper.RequestDetailsMapper.REQUEST_DETAILS_MAPPER;
import static com.itechart.workplace.mapper.RequestMapper.REQUEST_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;

    private final EmployeeService employeeService;
    private final PlaceService placeService;
    private final RequestResolverService requestResolverService;
    private final UserService userService;

    private final RequestValidator requestValidator;

    @Override
    @Transactional
    public Request create(final CreateRequest body) {
        var requester = userService.getCurrent();
        var request = REQUEST_MAPPER.toEntity(requester, requester, COMPLETED);
        var requestDetails = prepareRequestDetails(body.getDetails(), request);
        request.setRequestsDetails(requestDetails);

        var savedRequest = requestRepository.saveAndFlush(request);
        log.info("Request was successfully saved");
        return REQUEST_MAPPER.toModel(savedRequest, requester, requester);
    }

    private List<RequestDetailsEntity> prepareRequestDetails(final List<CreateRequestDetails> details,
                                                             final RequestEntity request) {
        return details.stream().map(detail -> {
            requestValidator.validateRequestDetail(detail);
            log.info("Request was successfully validated");

            var placeFrom = placeService.getById(detail.getPlaceFromId());
            var placeTo = placeService.getById(detail.getPlaceToId());
            var employee = employeeService.getById(detail.getEmployeeId());

            var entity = RequestDetailsEntity.builder()
                    .placeFrom(placeFrom)
                    .placeTo(placeTo)
                    .status(APPROVED)
                    .request(request)
                    .type(REQUEST_DETAILS_MAPPER.toEntity(detail.getType()))
                    .build();
            requestResolverService.performRequestDetail(entity, employee);
            return entity;
        }).collect(Collectors.toList());
    }

}
