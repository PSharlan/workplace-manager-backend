package com.itechart.workplace.service.impl;

import com.itechart.workplace.entity.PlaceEntity;
import com.itechart.workplace.exception.NotFoundException;
import com.itechart.workplace.generated.model.Place;
import com.itechart.workplace.repository.PlaceRepository;
import com.itechart.workplace.service.PlaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static com.itechart.workplace.exception.ErrorConstants.NOT_FOUND_MESSAGE;
import static com.itechart.workplace.mapper.PlaceMapper.PLACE_MAPPER;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlaceServiceImpl implements PlaceService {

    private final PlaceRepository placeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Place> getPlacesByLocationAndRoomAndNumber(final String location, final String roomNumber,
                                                           final Integer number) {
        return placeRepository.findByLocationAndRoomAndNumber(trimToEmpty(location), trimToEmpty(roomNumber), number)
                .stream().map(PLACE_MAPPER::toModel).collect(toList());
    }

    @Override
    @Transactional(readOnly = true)
    public PlaceEntity getById(final UUID id) {
        if (id == null) {
            return null;
        }
        return placeRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "place", id)));
    }

}
