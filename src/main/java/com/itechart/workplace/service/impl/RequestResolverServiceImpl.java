package com.itechart.workplace.service.impl;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.entity.RequestDetailsEntity;
import com.itechart.workplace.service.RequestResolverService;
import com.itechart.workplace.validators.RequestValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static com.itechart.workplace.entity.enums.RequestDetailsTypeEntity.MOVE;
import static com.itechart.workplace.entity.enums.RequestDetailsTypeEntity.REMOVE;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestResolverServiceImpl implements RequestResolverService {

    private final RequestValidator requestValidator;

    @Override
    public void performRequestDetail(final RequestDetailsEntity entity, final EmployeeEntity employee) {
        var type = entity.getType();
        if (type.equals(REMOVE)) {
            removeFromPlace(entity, employee);
        } else if (type.equals(MOVE)) {
            moveFromPlaceToPlace(entity, employee);
        } else {
            setToPlace(entity, employee);
        }
        log.info("Request detail was successfully performed");
    }

    private void removeFromPlace(final RequestDetailsEntity entity, final EmployeeEntity employee) {
        var placeFrom = entity.getPlaceFrom();
        requestValidator.validatePlaceFrom(placeFrom, employee);
        placeFrom.setEmployee(null);
    }

    private void moveFromPlaceToPlace(final RequestDetailsEntity entity, final EmployeeEntity employee) {
        var placeFrom = entity.getPlaceFrom();
        var placeTo = entity.getPlaceTo();
        requestValidator.validatePlaceFrom(placeFrom, employee);
        requestValidator.validatePlaceTo(placeTo);

        placeFrom.setEmployee(null);
        placeTo.setEmployee(employee);
    }

    private void setToPlace(final RequestDetailsEntity entity, final EmployeeEntity employee) {
        var placeTo = entity.getPlaceTo();
        requestValidator.validatePlaceTo(placeTo);
        placeTo.setEmployee(employee);
    }


}
