package com.itechart.workplace.service.impl;

import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.exception.NotFoundException;
import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.generated.model.Place;
import com.itechart.workplace.repository.EmployeeRepository;
import com.itechart.workplace.service.EmployeeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.itechart.workplace.exception.ErrorConstants.NOT_FOUND_MESSAGE;
import static com.itechart.workplace.mapper.EmployeeMapper.EMPLOYEE_MAPPER;
import static com.itechart.workplace.mapper.PlaceMapper.PLACE_MAPPER;
import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.Arrays.asList;
import static java.util.Collections.reverse;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

@Slf4j
@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    @Transactional(readOnly = true)
    public List<String> getDepartmentGroups(final String department) {
        return employeeRepository.findAllDepartmentGroups(department).stream()
                .map(group -> group.substring(group.lastIndexOf('.') + 1))
                .collect(toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> getAllByUsernameAndDepartment(final String username, final String department) {
        var foundUsers = employeeRepository.findAllByNameAndDepartment(trimToEmpty(username), trimToEmpty(department))
                .stream().map(EMPLOYEE_MAPPER::toModel).collect(toList());
        if (!foundUsers.isEmpty() || employeeRepository.findAll().isEmpty()) {
            return foundUsers;
        }

        var nameParts = asList(username.split(" "));
        reverse(nameParts);
        var reversedName = join(" ", nameParts);
        return employeeRepository.findAllByNameAndDepartment(reversedName, trimToEmpty(department)).stream()
                .map(EMPLOYEE_MAPPER::toModel)
                .collect(toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> getDepartments() {
        return employeeRepository.findAllDepartments();
    }

    @Override
    @Transactional(readOnly = true)
    public EmployeeEntity getById(final UUID id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(format(NOT_FOUND_MESSAGE, "employee", id)));
    }

    @Override
    @Transactional
    public long synchronizeEmployee(final List<EmployeeEntity> employees) {

        if (CollectionUtils.isEmpty(employees)) {
            return 0;
        }

        final List<String> smgIds = employees.stream().map(EmployeeEntity::getSmgId).collect(toList());

        final List<EmployeeEntity> entities = employeeRepository.findBySmgIds(smgIds);

        final Map<String, EmployeeEntity> employeeBySmgId =
                entities.stream()
                        .collect(Collectors.toMap(EmployeeEntity::getSmgId, Function.identity()));

        employees.forEach(e -> e.setId(employeeBySmgId.getOrDefault(e.getSmgId(), new EmployeeEntity()).getId()));

        return employeeRepository.saveAll(employees).size();
    }

    @Override
    public Place getPlace(final UUID id) {
        var employee = getById(id);
        return PLACE_MAPPER.toModel(employee.getPlace());
    }

}
