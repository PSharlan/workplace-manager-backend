package com.itechart.workplace.service;

import com.itechart.workplace.model.FileStorageObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

/**
 * This interfaces provides methods to manipulate with files.
 *
 * @author D.Delmukhambetov
 * @since 18.02.2020
 */
public interface FileStorageService {

    void write(MultipartFile fileToSave) throws IOException;

    void write(Path filePathToSave, String filenameToSave) throws IOException;

    FileStorageObject read(String filePath);

    void delete(String filePath);

    void rename(String filePath, String newFileName);

    boolean exists(String filePath);

}
