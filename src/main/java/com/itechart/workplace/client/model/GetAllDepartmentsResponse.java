package com.itechart.workplace.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class GetAllDepartmentsResponse extends BaseSmgResponse {

    @JsonProperty("Depts")
    protected List<SmgDepartment> depts;

}
