package com.itechart.workplace.client.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * Represents employee profile short info from SMG.
 *
 * @author D.Delmukhambetov
 * @since 12.03.2020
 */
@Getter
@Setter
public class SmgEmployeeProfile {

    @JsonProperty("FirstName")
    private String firstName;

    @JsonProperty("FirstNameEng")
    private String firstNameEng;

    @JsonProperty("LastName")
    private String lastName;

    @JsonProperty("LastNameEng")
    private String lastNameEng;

    @JsonProperty("ProfileId")
    private Long profileId;

    @JsonProperty("DeptId")
    private Long departmentId;

    @JsonProperty("Image")
    private String image;

    @JsonProperty("Position")
    private String position;

    @JsonProperty("Room")
    private String location;

    @JsonProperty("IsEnabled")
    private boolean enabled;

    @JsonIgnore
    private SmgDepartment department;

    public String getDepartmentCode() {
        if (Objects.nonNull(department)) {
            return this.department.getCode();
        }
        return null;
    }

}
