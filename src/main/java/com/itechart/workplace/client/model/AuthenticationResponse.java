package com.itechart.workplace.client.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class AuthenticationResponse extends BaseSmgResponse {

    @JsonProperty("SessionId")
    protected Integer sessionId;

}
