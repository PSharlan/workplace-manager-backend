package com.itechart.workplace.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents Department object from SMG.
 *
 * @author D.Delmukhambetov
 * @since 12.03.2020
 */
@Getter
@Setter
public class SmgDepartment {

    @JsonProperty("Id")
    private Long id;

    @JsonProperty("DepCode")
    private String code;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("NumUsers")
    private Integer numUsers;

}
