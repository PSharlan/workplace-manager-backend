package com.itechart.workplace.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents base class for responses of SMG API.
 *
 * @author D.Delmukhambetov
 * @since 12.03.2020
 */
@Getter
@Setter
public class BaseSmgResponse {

    @JsonProperty("ErrorCode")
    protected String errorCode;

    @JsonProperty("Permission")
    protected String permission;

}
