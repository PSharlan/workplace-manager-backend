package com.itechart.workplace.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AuthenticationRequest {

    @JsonProperty("Username")
    private String username;

    @JsonProperty("Password")
    private String password;

}
