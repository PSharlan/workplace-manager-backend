package com.itechart.workplace.client.impl;

import com.itechart.workplace.client.SmgClient;
import com.itechart.workplace.client.model.AuthenticationRequest;
import com.itechart.workplace.client.model.AuthenticationResponse;
import com.itechart.workplace.client.model.GetAllDepartmentsResponse;
import com.itechart.workplace.client.model.GetAllEmployeesResponse;
import com.itechart.workplace.client.model.SmgDepartment;
import com.itechart.workplace.client.model.SmgEmployeeProfile;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.entity.EmployeeEntity;
import com.itechart.workplace.mapper.EmployeeProfileMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Provides access to SMG REST API.
 * <p>Web-services description is available <a href="https://smg.itechart-group.com/services/Api.svc/">here</a> </p>
 *
 * @author D.Delmukhambetov
 * @since 12.03.2020
 */
@Component
@RequiredArgsConstructor
public class SmgRestClient implements SmgClient {

    private final RestTemplate restTemplate;
    private final AppConfiguration appConfiguration;

    private String url;

    @PostConstruct
    public void init() {
        this.url = this.appConfiguration.getSmg().getApiUrl();
    }

    @Override
    public List<EmployeeEntity> getEmployees(final String smgSessionId) {

        final GetAllEmployeesResponse allEmployeesResponse = restTemplate.getForObject(
                url + "GetAllEmployees?sessionId={sessionId}",
                GetAllEmployeesResponse.class,
                Collections.singletonMap("sessionId", smgSessionId)
        );

        // temporary solution until department table added to DB
        final GetAllDepartmentsResponse allDepartmentsResponse = restTemplate.getForObject(
                url + "GetAllDepartments?sessionId={sessionId}",
                GetAllDepartmentsResponse.class,
                Collections.singletonMap("sessionId", smgSessionId)
        );

        final Map<Long, SmgDepartment> departmentsById =
                allDepartmentsResponse.getDepts()
                        .stream()
                        .collect(Collectors.toMap(SmgDepartment::getId, Function.identity()));

        final List<SmgEmployeeProfile> profiles = allEmployeesResponse.getProfiles();

        profiles.forEach(ep -> ep.setDepartment(departmentsById.get(ep.getDepartmentId())));

        return allEmployeesResponse.getProfiles()
                .stream()
                .map(EmployeeProfileMapper.EMPLOYEE_PROFILE_MAPPER::toEntity)
                .collect(Collectors.toList());
    }

    @Override
    public String login(final String username, final String encryptedPassword) {

        AuthenticationResponse response = restTemplate.postForObject(url + "PostAuthenticate",
                new AuthenticationRequest(username, encryptedPassword), AuthenticationResponse.class);

        return response.getSessionId().toString();
    }
}
