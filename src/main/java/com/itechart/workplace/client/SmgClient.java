package com.itechart.workplace.client;

import com.itechart.workplace.entity.EmployeeEntity;

import java.util.List;

public interface SmgClient {

    List<EmployeeEntity> getEmployees(String smgWebAuth);

    String login(String username, String encryptedPassword);

}
