package com.itechart.workplace.unit.vsdx.service;

import com.itechart.workplace.unit.AbstractUnitTest;
import com.itechart.workplace.vsdx.service.impl.SvgServiceImpl;
import com.itechart.workplace.vsdx.service.impl.VsdxParserServiceImpl;
import com.itechart.workplace.vsdx.service.impl.VsdxResolverServiceImpl;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static com.itechart.workplace.common.utils.TestUtils.employeeEntity;
import static com.itechart.workplace.common.utils.TestUtils.officeEntity;
import static com.itechart.workplace.mapper.EmployeeMapper.EMPLOYEE_MAPPER;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("VariableDeclarationUsageDistance")
public class VsdxResolverServiceImplTest extends AbstractUnitTest {

    private static final String VSDX_FOLDER = "src/test/resources/";
    private static final String SOURCE_SVG_FOLDER = "/source/svg/";
    private static final String GENERATED_SVG_FOLDER = "/generated/svg/";
    private static final String VSDX_FILE_NAME = "Minsk I T10_803.vsdx";

    @Test
    public void prepareOfficeHappyPath() throws IOException {
        //GIVEN
        var svgService = new SvgServiceImpl();
        var vsdxParserService = new VsdxParserServiceImpl(
                svgService, appConfiguration, parseValidator, applicationEventPublisher);
        var image = getMockMultiPartFile(Paths.get(VSDX_FOLDER, VSDX_FILE_NAME));
        var vsdxResolverService = new VsdxResolverServiceImpl(employeeService, vsdxParserService, appConfiguration,
                parseValidator);
        //WHEN
        when(appConfiguration.getVsdx()).thenReturn(vsdx);
        when(vsdx.getFolder()).thenReturn(VSDX_FOLDER);
        when(vsdx.getGeneratedImagesFolder()).thenReturn(GENERATED_SVG_FOLDER);
        when(vsdx.getSourceImagesFolder()).thenReturn(SOURCE_SVG_FOLDER);

        vsdxParserService.init();
        vsdxResolverService.init();

        when(employeeService.getDepartments()).thenReturn(emptyList());
        when(employeeService.getAllByUsernameAndDepartment(anyString(), anyString()))
                .thenReturn(singletonList(EMPLOYEE_MAPPER.toModel(employeeEntity())));

        var preparedOffice = vsdxResolverService.prepareOffice(image, TEST_LOCATION);
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(preparedOffice.getRooms()).isNotNull();
            softly.assertThat(preparedOffice.getVisioLink()).isNotNull();
        });
    }

    @Test
    public void prepareRoomHappyPath() throws IOException {
        //GIVEN
        var svgService = new SvgServiceImpl();
        var vsdxParserService = new VsdxParserServiceImpl(
                svgService, appConfiguration, parseValidator, applicationEventPublisher);
        var image = getMockMultiPartFile(Paths.get(VSDX_FOLDER, VSDX_FILE_NAME));
        var vsdxResolverService = new VsdxResolverServiceImpl(employeeService, vsdxParserService, appConfiguration,
                parseValidator);
        //WHEN
        when(appConfiguration.getVsdx()).thenReturn(vsdx);
        when(vsdx.getFolder()).thenReturn(VSDX_FOLDER);
        when(vsdx.getGeneratedImagesFolder()).thenReturn(GENERATED_SVG_FOLDER);
        when(vsdx.getSourceImagesFolder()).thenReturn(SOURCE_SVG_FOLDER);

        vsdxParserService.init();
        vsdxResolverService.init();

        when(employeeService.getDepartments()).thenReturn(emptyList());
        when(employeeService.getAllByUsernameAndDepartment(anyString(), anyString()))
                .thenReturn(singletonList(EMPLOYEE_MAPPER.toModel(employeeEntity())));

        var preparedRoom = vsdxResolverService.prepareRoom(image, officeEntity(TEST_LOCATION));
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(preparedRoom.getPlaces()).isNotNull();
            softly.assertThat(preparedRoom.getTotalPlaces()).isNotNull();
        });
    }

}
