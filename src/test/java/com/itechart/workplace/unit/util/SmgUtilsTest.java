package com.itechart.workplace.unit.util;

import com.itechart.workplace.unit.AbstractUnitTest;
import org.junit.jupiter.api.Test;

import static com.itechart.workplace.common.utils.TestUtils.createBody;
import static com.itechart.workplace.common.utils.TestUtils.loginRequest;
import static com.itechart.workplace.util.SmgUtils.retrieveEmployeeProfiles;
import static org.assertj.core.api.Assertions.assertThat;

public class SmgUtilsTest extends AbstractUnitTest {

    @Test
    public void testRetrieveEmployeeProfiles() {
        //GIVEN
        //WHEN
        var employees = retrieveEmployeeProfiles(createBody(loginRequest()));
        //THEN
        assertThat(employees.size()).isEqualTo(1);
    }

}
