package com.itechart.workplace.unit.util;

import com.itechart.workplace.unit.AbstractUnitTest;
import org.junit.jupiter.api.Test;

import static com.itechart.workplace.util.VersionUtils.getAppVersion;
import static org.assertj.core.api.Assertions.assertThat;

public class VersionUtilsTest extends AbstractUnitTest {

    @Test
    public void testGetAppVersion() {
        //GIVEN
        //WHEN
        var appVersion = getAppVersion();
        //THEN
        assertThat(appVersion).isNotNull();
    }

}
