package com.itechart.workplace.unit;

import com.itechart.workplace.client.SmgClient;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.repository.RequestRepository;
import com.itechart.workplace.service.impl.EmployeeServiceImpl;
import com.itechart.workplace.validators.impl.VsdxParsingValidatorImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.nio.file.Path;

import static com.itechart.workplace.common.utils.TestUtils.mockMultiPartFile;
import static java.nio.file.Files.readAllBytes;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public abstract class AbstractUnitTest {

    protected static final String FILE_PATH = "src/test/resources/";
    protected static final String IMAGE_NAME = "Minsk I T10_803.vsdx";
    protected static final String TEST_LOCATION = "Minsk | T10";

    @MockBean
    protected AppConfiguration.Vsdx vsdx;
    @MockBean
    protected AppConfiguration appConfiguration;
    @MockBean
    protected HttpClient httpClient;
    @MockBean
    protected HttpResponse<Object> httpResponse;
    @MockBean
    protected VsdxParsingValidatorImpl parseValidator;
    @MockBean
    protected SmgClient smgClient;

    @MockBean
    protected RequestRepository requestRepository;

    @MockBean
    protected EmployeeServiceImpl employeeService;
    @MockBean
    protected ApplicationEventPublisher applicationEventPublisher;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    protected MockMultipartFile getMockMultiPartFile(final Path vsdxFilePath) throws IOException {
        return mockMultiPartFile(IMAGE_NAME, readAllBytes(vsdxFilePath));
    }

}
