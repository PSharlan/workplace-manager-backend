package com.itechart.workplace.unit.service;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.service.impl.AmazonS3FileStorageService;
import com.itechart.workplace.unit.AbstractUnitTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static java.nio.file.Paths.get;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;
import static org.springframework.util.MimeTypeUtils.TEXT_PLAIN_VALUE;

public class AmazonS3FileStorageServiceTest extends AbstractUnitTest {

    private static final String BUCKET_NAME = "another-bucket";

    @MockBean
    private AmazonS3Client amazonS3;
    @MockBean
    private AppConfiguration.Aws aws;
    @MockBean
    private AppConfiguration.S3 s3;

    @Captor
    private ArgumentCaptor<PutObjectRequest> putObjectRequestCaptor;
    @Captor
    private ArgumentCaptor<GetObjectRequest> getObjectRequestCaptor;
    @Captor
    private ArgumentCaptor<DeleteObjectRequest> deleteObjectRequestCaptor;
    @Captor
    private ArgumentCaptor<CopyObjectRequest> copyObjectRequestCaptor;

    @Override
    @BeforeEach
    public void setUp() {
        when(appConfiguration.getAws()).thenReturn(aws);
        when(aws.getS3()).thenReturn(s3);
        when(s3.getBucketName()).thenReturn(BUCKET_NAME);
    }

    @Test
    public void initWhenBucketExists() {
        doReturn(true).when(amazonS3).doesBucketExistV2(anyString());

        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        amazonS3FileStorageService.init();

        verify(appConfiguration.getAws().getS3()).getBucketName();
        verify(amazonS3, Mockito.times(0)).createBucket(anyString());
    }

    @Test
    public void initWhenBucketDoesNotExist() {
        doReturn(false).when(this.amazonS3).doesBucketExistV2(anyString());

        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        amazonS3FileStorageService.init();

        verify(appConfiguration.getAws().getS3()).getBucketName();
        verify(amazonS3, Mockito.times(1)).createBucket(BUCKET_NAME);
    }

    @Test
    public void writeWhenFileExists() throws IOException {
        var testFileContent = "test file content";
        var filename = "file.txt";
        var file = new MockMultipartFile(filename, filename, TEXT_PLAIN_VALUE, testFileContent.getBytes());

        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        amazonS3FileStorageService.write(file);

        verify(this.amazonS3).putObject(this.putObjectRequestCaptor.capture());
        final var capturedPutObjectRequest = this.putObjectRequestCaptor.getValue();
        final var metadata = capturedPutObjectRequest.getMetadata();

        assertSoftly(softly -> {
            softly.assertThat(capturedPutObjectRequest).isNotNull();
            softly.assertThat(metadata).isNotNull();
            softly.assertThat(filename).isEqualTo(capturedPutObjectRequest.getKey());
            softly.assertThat(TEXT_PLAIN_VALUE).isEqualTo(metadata.getContentType());
            softly.assertThat(testFileContent.getBytes().length).isEqualTo(metadata.getContentLength());
        });
    }

    @Test
    public void writeWhenOriginalFilenameIsNotDefined() throws IOException {
        var testFileContent = "test file content";
        var file = new MockMultipartFile("filename", null, TEXT_PLAIN_VALUE, testFileContent.getBytes());

        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        amazonS3FileStorageService.write(file);

        verify(amazonS3, times(0)).putObject(any());
    }

    @Test
    public void writeWhenFilepathExists() throws IOException {
        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        amazonS3FileStorageService.write(get(FILE_PATH, IMAGE_NAME), IMAGE_NAME);

        verify(this.amazonS3).putObject(this.putObjectRequestCaptor.capture());
        final var capturedPutObjectRequest = this.putObjectRequestCaptor.getValue();
        assertSoftly(softly -> {
            softly.assertThat(capturedPutObjectRequest).isNotNull();
            softly.assertThat(IMAGE_NAME).isEqualTo(capturedPutObjectRequest.getKey());
        });
    }

    @Test
    public void readWhenFileExists() {
        var filepath = "/home/file.txt";
        var testFileContent = "test file content";
        var s3Object = new S3Object();
        s3Object.setBucketName(BUCKET_NAME);
        s3Object.setKey(filepath);
        s3Object.setObjectContent(new ByteArrayInputStream(testFileContent.getBytes()));

        var metadata = new ObjectMetadata();
        metadata.setContentType(TEXT_PLAIN_VALUE);
        s3Object.setObjectMetadata(metadata);
        doReturn(s3Object).when(this.amazonS3).getObject(Mockito.any(GetObjectRequest.class));

        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        var fileStorageObject = amazonS3FileStorageService.read(filepath);

        verify(this.amazonS3).getObject(getObjectRequestCaptor.capture());
        final var capturedGetObjectRequest = getObjectRequestCaptor.getValue();
        assertSoftly(softly -> {
            softly.assertThat(fileStorageObject).isNotNull();
            softly.assertThat(fileStorageObject.getFile()).isNotNull();
            softly.assertThat(filepath).isEqualTo(capturedGetObjectRequest.getKey());
            softly.assertThat(TEXT_PLAIN_VALUE).isEqualTo(fileStorageObject.getContentType());
        });
    }

    @Test
    public void deleteWhenFileExists() {
        var filepath = "/home/file.txt";
        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        amazonS3FileStorageService.delete(filepath);

        verify(this.amazonS3).deleteObject(deleteObjectRequestCaptor.capture());
        final var capturedDeleteObjectRequest = deleteObjectRequestCaptor.getValue();
        assertSoftly(softly -> {
            softly.assertThat(capturedDeleteObjectRequest).isNotNull();
            softly.assertThat("file.txt").isEqualTo(capturedDeleteObjectRequest.getKey());
        });
    }

    @Test
    public void existsWhenFilepathIsNull() {
        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        var exists = amazonS3FileStorageService.exists(null);

        assertThat(exists).isFalse();
        verify(this.amazonS3, Mockito.times(0)).listObjectsV2(anyString());
    }

    @Test
    public void existsWhenFileExists() {
        var filepath = "/home/file.txt";
        var objectSummary = new S3ObjectSummary();
        objectSummary.setBucketName(BUCKET_NAME);
        objectSummary.setKey(filepath);

        var listObjectsV2Result = new ListObjectsV2Result();
        listObjectsV2Result.getObjectSummaries().add(objectSummary);
        doReturn(listObjectsV2Result).when(this.amazonS3).listObjectsV2(BUCKET_NAME);

        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        setField(amazonS3FileStorageService, "bucketName", BUCKET_NAME);

        var exists = amazonS3FileStorageService.exists(filepath);
        assertThat(exists).isTrue();
        verify(this.amazonS3).listObjectsV2(anyString());
    }

    @Test
    public void renameWhenNewFilenameIsDefined() {
        var filename = "file.txt";
        var newFilename = "new_file.txt";
        var amazonS3FileStorageService = new AmazonS3FileStorageService(amazonS3, appConfiguration);
        setField(amazonS3FileStorageService, "bucketName", BUCKET_NAME);
        amazonS3FileStorageService.rename(filename, newFilename);

        verify(this.amazonS3).copyObject(this.copyObjectRequestCaptor.capture());
        verify(this.amazonS3).deleteObject(any(DeleteObjectRequest.class));
        var capturedCopyObjectRequest = this.copyObjectRequestCaptor.getValue();
        assertSoftly(softly -> {
            softly.assertThat(BUCKET_NAME).isEqualTo(capturedCopyObjectRequest.getDestinationBucketName());
            softly.assertThat(BUCKET_NAME).isEqualTo(capturedCopyObjectRequest.getSourceBucketName());
            softly.assertThat(filename).isEqualTo(capturedCopyObjectRequest.getSourceKey());
            softly.assertThat(newFilename).isEqualTo(capturedCopyObjectRequest.getDestinationKey());
        });
    }
    
}
