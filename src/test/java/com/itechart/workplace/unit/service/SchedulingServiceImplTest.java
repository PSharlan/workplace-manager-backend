package com.itechart.workplace.unit.service;

import com.itechart.workplace.exception.ServiceUnavailableException;
import com.itechart.workplace.service.impl.SchedulingServiceImpl;
import com.itechart.workplace.unit.AbstractUnitTest;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.itechart.workplace.common.utils.TestUtils.createBody;
import static com.itechart.workplace.common.utils.TestUtils.createHeader;
import static com.itechart.workplace.common.utils.TestUtils.employeeEntity;
import static com.itechart.workplace.common.utils.TestUtils.loginRequest;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("VariableDeclarationUsageDistance")
public class SchedulingServiceImplTest extends AbstractUnitTest {

    @Test
    public void synchronizeEmployeesHappyPath() throws IOException, InterruptedException {
        //GIVEN
        var loginRequest = loginRequest();
        var employees = singletonList(employeeEntity(loginRequest.getUsername()));
        var schedulingService = new SchedulingServiceImpl(employeeService, appConfiguration, smgClient);
        //WHEN
        when(httpClient.send(any(), any())).thenReturn(httpResponse);
        when(httpResponse.statusCode()).thenReturn(302);
        when(httpResponse.body()).thenReturn(createBody(loginRequest));
        when(httpResponse.headers()).thenReturn(createHeader());
        when(employeeService.synchronizeEmployee(employees)).thenReturn(Long.valueOf(employees.size()));
        //THEN
        schedulingService.synchronizeEmployees();
    }

    @Test
    public void synchronizeEmployeesWhenFailed() throws IOException, InterruptedException {
        //GIVEN
        var schedulingService = new SchedulingServiceImpl(employeeService, appConfiguration, smgClient);
        //WHEN
        when(httpClient.send(any(), any())).thenThrow(new ServiceUnavailableException("SMG is unavailable"));
        //THEN
        schedulingService.synchronizeEmployees();
    }

}
