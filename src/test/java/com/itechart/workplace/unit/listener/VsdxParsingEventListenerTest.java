package com.itechart.workplace.unit.listener;

import com.itechart.workplace.event.VsdxParsingCompletionEvent;
import com.itechart.workplace.service.impl.FileSyncVisitor;
import com.itechart.workplace.unit.AbstractUnitTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

import static java.nio.file.FileVisitResult.CONTINUE;
import static org.mockito.Mockito.*;

public class VsdxParsingEventListenerTest extends AbstractUnitTest {

    @MockBean
    private FileSyncVisitor fileSyncVisitor;

    @Test
    public void onParsingCompletionEventWhenEventIsNull() throws IOException {
        var eventListener = new VsdxParsingEventListener(fileSyncVisitor);
        eventListener.onParsingCompletionEvent(null);

        verify(fileSyncVisitor, times(0)).visitFile(any(), any());
        verify(fileSyncVisitor, times(0)).postVisitDirectory(any(), any());
    }

    @Test
    public void onParsingCompletionEventWhenEventPathIsBlank() throws IOException {
        var eventListener = new VsdxParsingEventListener(fileSyncVisitor);
        eventListener.onParsingCompletionEvent(new VsdxParsingCompletionEvent(this, ""));

        verify(fileSyncVisitor, times(0)).visitFile(any(), any());
        verify(fileSyncVisitor, times(0)).postVisitDirectory(any(), any());
    }

    @Test
    public void onParsingCompletionEventWhenEventPathIsDefined() throws IOException {
        when(fileSyncVisitor.postVisitDirectory(any(), any())).thenReturn(CONTINUE);
        when(fileSyncVisitor.preVisitDirectory(any(), any())).thenReturn(CONTINUE);
        when(fileSyncVisitor.visitFile(any(), any())).thenReturn(CONTINUE);
        when(fileSyncVisitor.visitFileFailed(any(), any())).thenReturn(CONTINUE);

        var eventListener = new VsdxParsingEventListener(fileSyncVisitor);
        eventListener.onParsingCompletionEvent(new VsdxParsingCompletionEvent(this, FILE_PATH));

        verify(fileSyncVisitor, atLeast(1)).visitFile(any(), any());
        verify(fileSyncVisitor, atLeast(1)).postVisitDirectory(any(), any());
    }

}
