package com.itechart.workplace.common.utils;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.github.javafaker.Faker;
import com.itechart.workplace.client.model.AuthenticationResponse;
import com.itechart.workplace.entity.*;
import com.itechart.workplace.generated.model.CreateRequest;
import com.itechart.workplace.generated.model.CreateRequestDetails;
import com.itechart.workplace.generated.model.LoginRequest;
import com.itechart.workplace.security.impl.UserDetailsImpl;
import lombok.experimental.UtilityClass;
import org.springframework.mock.web.MockMultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static com.itechart.workplace.entity.enums.PlaceStatusEntity.AVAILABLE;
import static com.itechart.workplace.generated.model.RequestDetailsType.PLACE;
import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static org.testcontainers.shaded.javax.ws.rs.core.MediaType.APPLICATION_SVG_XML;

@UtilityClass
public class TestUtils {

    private static final Faker FAKE = Faker.instance(Locale.US, ThreadLocalRandom.current());

    private static final String BODY = "Username=%s&Password=%s&__EVENTTARGET=btnLogin&__VIEWSTATE=Fw"
            + "EPDwUKMTU2Mjg3MDc4MA9&__VIEWSTATEGENERATOR=C2EE9ABB&__EVENTVALIDATION=wEdAAQqSlq0IwzHy"
            + "<html xmlns=http://www.w3.org/1999/xhtml><form id=aspnetForm _lpchecked=1><div><table><tbody>"
            + "<tr id=TrMain><td id=ctl00_BodyCenter class=ga_top><table id=TableMain><tbody><tr><td><div>"
            + "<table class=dxgvControl_SMG id=ctl00_dataGrid><tbody><tr><td><div><table id=ctl00_dataGrid_DXMainTable"
            + "class=dxgvTable_SMG onclick=aspxGVTableClick('ctl00_dataGrid', event);> <tbody><tr "
            + "id=ctl00_dataGrid_DXDataRow0 class=dxgvDataRow_SMG> <td class=dxgv>7</td><td class=dxgv>D3</td><td "
            + "class=dxgv>D3.G2</td><td class=dxgv>Balakshina Viktoria</td><td class=dxgv>Балакшина Виктория</td><td "
            + "class=dxgv>Minsk | T10 | 1001</td> </tr> </tbody></table> </div> </div> </td> </tr> </tbody></table>"
            + " </td> </tr> </tbody></table> </div> </form> </body> </html>";
    private static final String HEADER = "set-cookie";
    private static final String SMG_WEB_AUTH = "ASP.NET_SessionId=%s; path=/; HttpOnly;SMGProjectId=51; "
            + "expires=Sat, 13-Jan-2120 13:54:48 GMT; path=/;SMGUserId=%s; expires=Sat, 13-Jan-2120 13:54:48 GMT; "
            + "path=/;SMGWeb_AUTH=%s; path=/";
    private static final String TEST_URI = "http://foo.com/";

    public static LoginRequest loginRequest() {
        var loginRequest = new LoginRequest();
        loginRequest.setUsername(FAKE.bothify("???##@gmail.com"));
        loginRequest.setPassword(FAKE.bothify("????####"));
        return loginRequest;
    }

    public static CreateRequest createPlaceRequest(final UUID employeeId, final UUID placeId) {
        var createRequest = new CreateRequest();
        createRequest.setDetails(singletonList(createRequestDetails(employeeId, placeId)));
        return createRequest;
    }

    private static CreateRequestDetails createRequestDetails(final UUID employeeId, final UUID placeId) {
        var details = new CreateRequestDetails();
        details.setEmployeeId(employeeId);
        details.setType(PLACE);
        details.setPlaceToId(placeId);
        return details;
    }

    public static UserDetailsImpl userDetails(final UserEntity user) {
        var userDetails = new UserDetailsImpl();
        userDetails.setUserId(user.getId());
        userDetails.setRole(user.getRole());
        userDetails.setSmgWebAuth(smgWebAuth());
        userDetails.setDisplayName(user.getDisplayName());
        return userDetails;
    }

    public static EmployeeEntity employeeEntity(final String name) {
        return EmployeeEntity.builder()
                .id(UUID.randomUUID())
                .department(FAKE.bothify("?#"))
                .group(FAKE.bothify("?#"))
                .localizedName(FAKE.name().fullName())
                .name(name)
                .smgId(String.valueOf(FAKE.number().numberBetween(1000, 9999)))
                .location(FAKE.address().fullAddress())
                .imageLink(FAKE.internet().image())
                .place(null)
                .build();
    }

    public static EmployeeEntity employeeEntity() {
        return EmployeeEntity.builder()
                .id(UUID.randomUUID())
                .department(FAKE.bothify("?#"))
                .group(FAKE.bothify("?#"))
                .localizedName(FAKE.name().fullName())
                .name(FAKE.name().fullName())
                .smgId(String.valueOf(FAKE.number().numberBetween(1000, 9999)))
                .location(FAKE.address().fullAddress())
                .imageLink(FAKE.internet().image())
                .place(null)
                .build();
    }

    public static OfficeEntity officeEntity(final String location) {
        return OfficeEntity.builder()
                .location(location)
                .visioLink(FAKE.internet().image())
                .build();
    }

    public static RoomEntity roomEntity(final OfficeEntity officeEntity, final EmployeeEntity manager) {
        return RoomEntity.builder()
                .office(officeEntity)
                .svgLink(FAKE.internet().url())
                .visioPageId(FAKE.number().numberBetween(1, 100))
                .totalPlaces(FAKE.number().numberBetween(20, 50))
                .number(String.valueOf(FAKE.number().numberBetween(100, 9999)))
                .floorNumber(FAKE.number().numberBetween(1, 13))
                .manager(manager)
                .build();
    }

    public static PlaceEntity placeEntity(final RoomEntity room) {
        return PlaceEntity.builder()
                .number(FAKE.number().numberBetween(1, 20))
                .status(AVAILABLE)
                .room(room)
                .visioCardId((long) FAKE.number().numberBetween(1, 100))
                .build();
    }

    public static String smgWebAuth() {
        return format(SMG_WEB_AUTH, UUID.randomUUID(), FAKE.number().numberBetween(1000, 9999), UUID.randomUUID());
    }

    public static String createBody(final LoginRequest loginRequest) {
        return format(BODY, loginRequest.getUsername(), loginRequest.getPassword());
    }

    public static HttpHeaders createHeader() {
        return HttpRequest.newBuilder().header(HEADER, smgWebAuth()).uri(URI.create(TEST_URI)).build().headers();
    }

    public static AuthenticationResponse successfulAuthResponse() {
        AuthenticationResponse successfulAuthResponse = new AuthenticationResponse();
        successfulAuthResponse.setSessionId(123456789);
        successfulAuthResponse.setPermission("READ");
        return successfulAuthResponse;
    }

    public static MockMultipartFile mockMultiPartFile(final String filename, final byte[] bytes) {
        return new MockMultipartFile("vsdxImage", filename, "application/vnd.ms-visio.drawing",
                bytes);
    }

    public static S3Object s3Object(final InputStream inputStream) throws IOException {
        var s3Object = new S3Object();
        s3Object.setObjectContent(new ByteArrayInputStream(inputStream.readAllBytes()));

        var metadata = new ObjectMetadata();
        metadata.setContentType(APPLICATION_SVG_XML);
        s3Object.setObjectMetadata(metadata);
        return s3Object;
    }

}
