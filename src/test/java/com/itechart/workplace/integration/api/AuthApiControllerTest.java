package com.itechart.workplace.integration.api;

import com.itechart.workplace.exception.ServiceUnavailableException;
import com.itechart.workplace.exception.UnauthorizedException;
import com.itechart.workplace.generated.model.Token;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static com.itechart.workplace.common.utils.TestUtils.loginRequest;
import static com.itechart.workplace.common.utils.TestUtils.successfulAuthResponse;
import static com.itechart.workplace.common.utils.TestUtils.userDetails;
import static com.itechart.workplace.entity.enums.UserRoleEntity.USER;
import static com.itechart.workplace.generated.api.AuthApi.loginPath;
import static com.itechart.workplace.generated.api.AuthApi.refreshPath;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void loginHappyPath() throws Exception {
        //GIVEN
        var loginRequest = loginRequest();
        //WHEN
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn(successfulAuthResponse());

        var response = mockMvc.perform(
                post(loginPath)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginRequest)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Token.class);
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(body.getRefreshToken()).isNotNull();
            softly.assertThat(body.getAccessToken()).isNotNull();
            softly.assertThat(userRepository.findAll().size()).isEqualTo(1);
        });
    }

    @Test
    public void loginWhenUnauthorized() throws Exception {
        //GIVEN
        var loginRequest = loginRequest();
        //WHEN
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenThrow(new UnauthorizedException("Could not authorize to SMG"));

        var resultActions = mockMvc.perform(
                post(loginPath)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginRequest)));
        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

    @Test
    public void loginWhenSmgIsUnavailable() throws Exception {
        //GIVEN
        var loginRequest = loginRequest();
        //WHEN
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenThrow(new ServiceUnavailableException("SMG is unavailable"));
        var resultActions = mockMvc.perform(
                post(loginPath)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(loginRequest)));
        //THEN
        resultActions.andExpect(status().is5xxServerError());
    }

    @Test
    public void refreshHappyPath() throws Exception {
        //GIVEN
        var loginRequest = loginRequest();
        var user = createUser(loginRequest, USER);
        var token = tokenProvider.createToken(userDetails(user));
        //WHEN
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn(successfulAuthResponse());

        var response = mockMvc.perform(
                post(refreshPath)
                        .param("refreshToken", token.getRefreshToken())
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Token.class);
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(body.getRefreshToken()).isNotNull();
            softly.assertThat(body.getAccessToken()).isNotNull();
        });
    }

    @Test
    public void refreshWhenInvalidToken() throws Exception {
        //GIVEN
        //WHEN
        var resultActions = mockMvc.perform(
                post(refreshPath)
                        .param("refreshToken", "refreshToken"));
        //THEN
        resultActions.andExpect(status().isUnauthorized());
    }

}
