package com.itechart.workplace.integration.api;

import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static com.itechart.workplace.common.utils.TestUtils.loginRequest;
import static com.itechart.workplace.common.utils.TestUtils.userDetails;
import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.generated.api.UsersApi.getCurrentUserPath;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("PMD.LinguisticNaming")
public class UsersApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void getCurrentUserHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var employee = createEmployee(user.getUsername());
        //WHEN
        var response = mockMvc.perform(
                get(getCurrentUserPath)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Employee.class);
        //THEN
        assertThat(body.getName()).isEqualTo(employee.getName());
    }

}
