package com.itechart.workplace.integration.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.itechart.workplace.generated.model.Employee;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static com.itechart.workplace.common.utils.TestUtils.createBody;
import static com.itechart.workplace.common.utils.TestUtils.createHeader;
import static com.itechart.workplace.common.utils.TestUtils.loginRequest;
import static com.itechart.workplace.common.utils.TestUtils.userDetails;
import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.generated.api.EmployeesApi.getAllEmployeesPath;
import static com.itechart.workplace.generated.api.EmployeesApi.getEmployeePlacePath;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("PMD.LinguisticNaming")
public class EmployeesApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void getAllEmployeesHappyPath() throws Exception {
        //GIVEN
        var loginRequest = loginRequest();
        var user = createUser(loginRequest, ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        //WHEN
        when(httpClient.send(any(), any())).thenReturn(httpResponse);
        when(httpResponse.statusCode()).thenReturn(302);
        when(httpResponse.body()).thenReturn(createBody(loginRequest));
        when(httpResponse.headers()).thenReturn(createHeader());

        var response = mockMvc.perform(
                get(getAllEmployeesPath)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        final List<Employee> body = objectMapper.readValue(response.getContentAsByteArray(),
                new TypeReference<>() {
                });
        //THEN
        assertThat(body.size()).isEqualTo(employeeRepository.count());
    }

    @Test
    public void getEmployeePlaceHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var employee = createEmployee(user.getUsername());
        //WHEN
        var response = mockMvc.perform(
                get(getEmployeePlacePath, employee.getId())
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        //THEN
        assertThat(response.getContentAsByteArray()).isEmpty();
    }

    @Test
    public void getEmployeePlaceWhenNotFound() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        //WHEN
        var response = mockMvc.perform(
                get(getEmployeePlacePath, UUID.randomUUID())
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));
        //THEN
        response.andExpect(status().isNotFound());
    }

}
