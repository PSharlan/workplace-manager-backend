package com.itechart.workplace.integration.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.itechart.workplace.generated.model.Place;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.itechart.workplace.common.utils.TestUtils.loginRequest;
import static com.itechart.workplace.common.utils.TestUtils.userDetails;
import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.generated.api.PlacesApi.getAllPlacesPath;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("PMD.LinguisticNaming")
public class PlacesApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void getAllPlacesHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);
        var place = createPlace(room);

        //WHEN
        var response = mockMvc.perform(
                get(getAllPlacesPath)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        final List<Place> places = objectMapper.readValue(response.getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(places.get(0).getId()).isEqualTo(place.getId());
            softly.assertThat(places.size()).isEqualTo(placeRepository.count());
            softly.assertThat(places.get(0).getRoomId()).isEqualTo(room.getId());
        });
    }

}
