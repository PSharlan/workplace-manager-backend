package com.itechart.workplace.integration.api;

import com.itechart.workplace.generated.model.Room;
import com.itechart.workplace.generated.model.Violation;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import com.itechart.workplace.model.AmazonS3Object;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static com.itechart.workplace.common.utils.TestUtils.*;
import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.generated.api.RoomsApi.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("PMD.LinguisticNaming")
public class RoomsApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void deleteRoomByIdHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);

        //WHEN
        mockMvc.perform(delete(deleteRoomByIdPath, room.getId())
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isNoContent())
                .andReturn();
        //THEN
        assertThat(roomRepository.findAll().size()).isEqualTo(0);
    }

    @Test
    public void getRoomByIdHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);

        //WHEN
        var response = mockMvc.perform(get(getRoomByIdPath, room.getId())
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Room.class);
        //THEN
        assertThat(body.getId()).isEqualTo(room.getId());
    }

    @Test
    public void getRoomByIdWhenNotFound() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));

        //WHEN
        var response = mockMvc.perform(get(getRoomByIdPath, UUID.randomUUID())
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isNotFound());
    }

    @Test
    public void getRoomSvgByIdHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);

        var inputStream = resourceLoader.getResource(IMAGE_PATH + SVG_IMAGE_NAME).getInputStream();
        var s3Object = s3Object(inputStream);

        //WHEN
        when(amazonS3FileStorageService.read(room.getSvgLink())).thenReturn(new AmazonS3Object(s3Object));

        var response = mockMvc.perform(get(getRoomSvgByIdPath, room.getId())
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        //THEN
        assertThat(response.getHeader(CONTENT_TYPE)).isEqualTo(s3Object.getObjectMetadata().getContentType());
    }

    @Test
    public void getRoomSvgByIdWhenNotFound() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));

        //WHEN
        var response = mockMvc.perform(get(getRoomSvgByIdPath, UUID.randomUUID())
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isNotFound());
    }

    @Test
    public void importRoomsFromVsdxHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var vsdxImage = getMockMultiPartFile();
        var manager = createEmployee(user.getUsername());
        var office = createOffice();
        var room = roomEntity(office, manager);

        //WHEN
        when(vsdxResolverService.prepareRoom(vsdxImage, office)).thenReturn(room);

        var response = mockMvc.perform(
                multipart(importRoomsFromVsdxPath)
                        .file(vsdxImage)
                        .param("officeId", String.valueOf(office.getId()))
                        .param("ignoreWarnings", "true")
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Room.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(body.getId()).isNotNull();
            softly.assertThat(body.getOfficeId()).isEqualTo(office.getId());
            softly.assertThat(body.getManager().getId()).isEqualTo(manager.getId());
        });
    }

    @Test
    public void importRoomsFromVsdxWhenOfficeNotFound() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var vsdxImage = getMockMultiPartFile();

        //WHEN
        var response = mockMvc.perform(
                multipart(importRoomsFromVsdxPath)
                        .file(vsdxImage)
                        .param("officeId", String.valueOf(UUID.randomUUID()))
                        .param("ignoreWarnings", "true")
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isNotFound());
    }

    @Test
    public void importRoomsFromVsdxWithWarnings() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var vsdxImage = getMockMultiPartFile();
        var manager = createEmployee(user.getUsername());
        var office = createOffice();
        var room = roomEntity(office, manager);
        vsdxParsingValidator.addViolation(new Violation());

        //WHEN
        when(vsdxResolverService.prepareRoom(vsdxImage, office)).thenReturn(room);

        var response = mockMvc.perform(
                multipart(importRoomsFromVsdxPath)
                        .file(vsdxImage)
                        .param("officeId", String.valueOf(office.getId()))
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void updateRoomAttributesHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);

        var newUser = createUser(loginRequest(), ADMIN);
        var newManager = createEmployee(newUser.getUsername());

        //WHEN
        var response = mockMvc.perform(
                put(updateRoomAttributesPath, room.getId())
                        .param("newManagerId", String.valueOf(newManager.getId()))
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Room.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(body.getId()).isEqualTo(room.getId());
            softly.assertThat(body.getManager().getId()).isEqualTo(newManager.getId());
        });
    }

    @Test
    public void updateRoomAttributesWhenRoomNotFound() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));

        //WHEN
        var response = mockMvc.perform(
                put(updateRoomAttributesPath, UUID.randomUUID())
                        .param("newManagerId", String.valueOf(UUID.randomUUID()))
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isNotFound());
    }

    @Test
    public void updateRoomAttributesWhenManagerNotFound() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);

        //WHEN
        var response = mockMvc.perform(
                put(updateRoomAttributesPath, room.getId())
                        .param("newManagerId", String.valueOf(UUID.randomUUID()))
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isNotFound());
    }

}
