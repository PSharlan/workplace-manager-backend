package com.itechart.workplace.integration.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.itechart.workplace.generated.model.Office;
import com.itechart.workplace.generated.model.Room;
import com.itechart.workplace.generated.model.Violation;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static com.itechart.workplace.common.utils.TestUtils.*;
import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.generated.api.OfficesApi.*;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("PMD.LinguisticNaming")
public class OfficesApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void importOfficesFromVsdxHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var vsdxImage = getMockMultiPartFile();

        //WHEN
        when(vsdxResolverService.prepareOffice(vsdxImage, TEST_LOCATION)).thenReturn(officeEntity(TEST_LOCATION));

        var response = mockMvc.perform(
                multipart(importOfficesFromVsdxPath)
                        .file(vsdxImage)
                        .param("city", TEST_CITY)
                        .param("officeName", TEST_OFFICE)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Office.class);
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(body.getId()).isNotNull();
            softly.assertThat(body.getLocation()).isEqualTo(TEST_LOCATION);
            softly.assertThat(officeRepository.findAll().size()).isEqualTo(1);
        });
    }

    @Test
    public void importOfficesFromVsdxWithWarnings() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var vsdxImage = getMockMultiPartFile();
        vsdxParsingValidator.addViolation(new Violation());

        //WHEN
        when(vsdxResolverService.prepareOffice(vsdxImage, TEST_LOCATION)).thenReturn(officeEntity(TEST_LOCATION));

        var response = mockMvc.perform(
                multipart(importOfficesFromVsdxPath)
                        .file(vsdxImage)
                        .param("city", TEST_CITY)
                        .param("officeName", TEST_OFFICE)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void getOfficeRoomsHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var office = createOffice();
        office.setRooms(singletonList(roomEntity(office, manager)));
        officeRepository.saveAndFlush(office);

        //WHEN
        var response = mockMvc.perform(
                get(getOfficeRoomsPath, office.getId())
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        final List<Room> body = objectMapper.readValue(response.getContentAsByteArray(),
                new TypeReference<>() {
                });
        //THEN
        assertThat(body.size()).isEqualTo(office.getRooms().size());
    }

    @Test
    public void getOfficeRoomsWhenNotFound() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));

        //WHEN
        var response = mockMvc.perform(
                get(getOfficeRoomsPath, UUID.randomUUID())
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));
        //THEN
        response.andExpect(status().isNotFound());
    }

    @Test
    public void getAllOfficesHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));

        //WHEN
        var response = mockMvc.perform(
                get(getAllOfficesPath)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        final List<Office> body = objectMapper.readValue(response.getContentAsByteArray(),
                new TypeReference<>() {
                });
        //THEN
        assertThat(body.size()).isEqualTo(0);
    }

    @Test
    public void deleteOfficeByIdHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var office = createOffice();

        //WHEN
        mockMvc.perform(delete(deleteOfficeByIdPath, office.getId())
                .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isNoContent())
                .andReturn();
        //THEN
        assertThat(officeRepository.findAll().size()).isEqualTo(0);
    }

}
