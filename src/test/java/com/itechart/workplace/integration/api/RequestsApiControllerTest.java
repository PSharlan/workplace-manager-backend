package com.itechart.workplace.integration.api;

import com.itechart.workplace.generated.model.Request;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static com.itechart.workplace.common.utils.TestUtils.*;
import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.generated.api.RequestsApi.createRequestPath;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RequestsApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void createRequestHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);
        var place = createPlace(room);
        var request = createPlaceRequest(manager.getId(), place.getId());

        //WHEN
        var response = mockMvc.perform(
                post(createRequestPath)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse();
        var body = objectMapper.readValue(response.getContentAsByteArray(), Request.class);

        //THEN
        var updatedPlace = body.getRequestsDetails().get(0).getPlaceTo();
        assertSoftly(softly -> {
            softly.assertThat(body.getId()).isNotNull();
            softly.assertThat(updatedPlace.getId()).isEqualTo(place.getId());
            softly.assertThat(updatedPlace.getEmployee().getId()).isEqualTo(manager.getId());
        });
    }

    @Test
    public void createRequestWhenInvalidRequestParams() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var manager = createEmployee(user.getUsername());
        var room = createRoom(createOffice(), manager);
        var place = createPlace(room);
        place.setEmployee(manager);
        placeRepository.save(place);

        var request = createPlaceRequest(manager.getId(), place.getId());

        //WHEN
        var response = mockMvc.perform(
                post(createRequestPath)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()));

        //THEN
        response.andExpect(status().isBadRequest());
    }

}
