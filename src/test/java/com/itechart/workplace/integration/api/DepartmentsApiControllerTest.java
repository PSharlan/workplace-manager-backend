package com.itechart.workplace.integration.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.itechart.workplace.common.utils.TestUtils.loginRequest;
import static com.itechart.workplace.common.utils.TestUtils.userDetails;
import static com.itechart.workplace.entity.enums.UserRoleEntity.ADMIN;
import static com.itechart.workplace.generated.api.DepartmentsApi.getDepartmentGroupsPath;
import static com.itechart.workplace.generated.api.DepartmentsApi.getDepartmentsPath;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("PMD.LinguisticNaming")
public class DepartmentsApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void getDepartmentsHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var employee = createEmployee(user.getUsername());
        //WHEN
        var response = mockMvc.perform(
                get(getDepartmentsPath)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        final List<String> body = objectMapper.readValue(response.getContentAsByteArray(),
                new TypeReference<>() {
                });
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(body.size()).isEqualTo(1);
            softly.assertThat(body.get(0)).isEqualTo(employee.getDepartment());
        });
    }

    @Test
    public void getDepartmentGroupsHappyPath() throws Exception {
        //GIVEN
        var user = createUser(loginRequest(), ADMIN);
        var token = tokenProvider.createToken(userDetails(user));
        var employee = createEmployee(user.getUsername());
        //WHEN
        var response = mockMvc.perform(
                get(getDepartmentGroupsPath)
                        .header(AUTHORIZATION, TOKEN_PREFIX + token.getAccessToken())
                        .param("department", employee.getDepartment()))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        final List<String> body = objectMapper.readValue(response.getContentAsByteArray(),
                new TypeReference<>() {
                });
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(body.size()).isEqualTo(1);
            softly.assertThat(employee.getGroup().contains(body.get(0))).isTrue();
        });
    }

}
