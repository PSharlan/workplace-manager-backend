package com.itechart.workplace.integration.api;

import com.itechart.workplace.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import static com.itechart.workplace.api.SwaggerApiController.GET_CONFIGURATION_PATH;
import static com.itechart.workplace.api.SwaggerApiController.GET_SECURITY_CONFIGURATION_PATH;
import static com.itechart.workplace.api.SwaggerApiController.GET_SWAGGER_JSON_PATH;
import static com.itechart.workplace.api.SwaggerApiController.GET_SWAGGER_YAML_PATH;
import static com.itechart.workplace.api.SwaggerApiController.GET_SWAGGER_YML_PATH;
import static com.itechart.workplace.api.SwaggerApiController.GET_UI_CONFIGURATION_PATH;
import static com.itechart.workplace.api.SwaggerApiController.SWAGGER_UI_REDIRECT_PATH;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SwaggerApiControllerTest extends AbstractIntegrationTest {

    @Test
    public void testSwaggerUiRedirectHappyPath() throws Exception {
        //GIVEN
        //WHEN
        var resultActions = mockMvc.perform(
                get(SWAGGER_UI_REDIRECT_PATH));
        //THEN
        resultActions.andExpect(status().is3xxRedirection());
    }

    @Test
    public void testGetUiConfigurationHappyPath() throws Exception {
        //GIVEN
        //WHEN
        var resultActions = mockMvc.perform(
                get(GET_UI_CONFIGURATION_PATH));
        var body = resultActions.andReturn()
                .getResponse()
                .getContentAsString();
        //THEN
        resultActions.andExpect(status().isOk());
        assertNotNull(body);
    }

    @Test
    public void testGetSecurityConfigurationHappyPath() throws Exception {
        //GIVEN
        //WHEN
        var resultActions = mockMvc.perform(
                get(GET_SECURITY_CONFIGURATION_PATH));
        var body = resultActions.andReturn()
                .getResponse()
                .getContentAsString();
        //THEN
        resultActions.andExpect(status().isOk());
        assertNotNull(body);
    }

    @Test
    public void testGetConfigurationHappyPath() throws Exception {
        //GIVEN
        //WHEN
        var resultActions = mockMvc.perform(
                get(GET_CONFIGURATION_PATH));
        var body = resultActions.andReturn()
                .getResponse()
                .getContentAsString();
        //THEN
        resultActions.andExpect(status().isOk());
        assertNotNull(body);
    }

    @Test
    public void testGetSwaggerYamlHappyPath() throws Exception {
        //GIVEN
        //WHEN
        var resultActionsYml = mockMvc.perform(
                get(GET_SWAGGER_YML_PATH));
        var resultActionsYaml = mockMvc.perform(
                get(GET_SWAGGER_YAML_PATH));

        var bodyYml = resultActionsYml.andReturn()
                .getResponse()
                .getContentAsString();
        var bodyYaml = resultActionsYaml.andReturn()
                .getResponse()
                .getContentAsString();
        //THEN
        resultActionsYml.andExpect(status().isOk());
        resultActionsYaml.andExpect(status().isOk());
        assertNotNull(bodyYml);
        assertNotNull(bodyYaml);
    }

    @Test
    public void testGetSwaggerJsonHappyPath() throws Exception {
        //GIVEN
        //WHEN
        var resultActions = mockMvc.perform(
                get(GET_SWAGGER_JSON_PATH));
        var body = resultActions.andReturn()
                .getResponse()
                .getContentAsString();
        //THEN
        resultActions.andExpect(status().isOk());
        assertNotNull(body);
    }

}
