package com.itechart.workplace.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itechart.workplace.config.AppConfiguration;
import com.itechart.workplace.entity.*;
import com.itechart.workplace.entity.enums.UserRoleEntity;
import com.itechart.workplace.generated.model.LoginRequest;
import com.itechart.workplace.integration.initializer.PostgresInitializer;
import com.itechart.workplace.repository.*;
import com.itechart.workplace.security.TokenProvider;
import com.itechart.workplace.service.impl.AmazonS3FileStorageService;
import com.itechart.workplace.validators.VsdxParsingValidator;
import com.itechart.workplace.vsdx.service.impl.VsdxResolverServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.regex.Pattern;

import static com.itechart.workplace.common.utils.TestUtils.*;
import static com.itechart.workplace.util.AesEncryptionUtils.encryptData;
import static com.itechart.workplace.util.SmgUtils.prepareDisplayName;
import static java.lang.String.join;
import static java.util.Arrays.asList;
import static java.util.Collections.reverse;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@ContextConfiguration(initializers = {
        AbstractIntegrationTest.Initializer.class,
        PostgresInitializer.class
})
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {

    protected static final String TEST_LOCATION = "Minsk | T10";
    protected static final String TEST_CITY = "Minsk";
    protected static final String TEST_OFFICE = "T10";
    protected static final String TOKEN_PREFIX = "Bearer ";

    protected static final String SVG_IMAGE_NAME = "room_803.svg";
    protected static final String IMAGE_PATH = "classpath:./";

    private static final String VSDX_IMAGE_NAME = "Minsk I T10_803.vsdx";

    @Autowired
    protected EmployeeRepository employeeRepository;
    @Autowired
    protected OfficeRepository officeRepository;
    @Autowired
    protected PlaceRepository placeRepository;
    @Autowired
    protected RequestRepository requestRepository;
    @Autowired
    protected RoomRepository roomRepository;
    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected AppConfiguration appConfiguration;
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected ResourceLoader resourceLoader;
    @Autowired
    protected TokenProvider tokenProvider;
    @Autowired
    protected VsdxParsingValidator vsdxParsingValidator;

    @MockBean
    protected HttpClient httpClient;
    @MockBean
    protected HttpResponse<Object> httpResponse;
    @MockBean
    protected RestTemplate restTemplate;
    @MockBean
    protected VsdxResolverServiceImpl vsdxResolverService;
    @MockBean
    protected AmazonS3FileStorageService amazonS3FileStorageService;

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        @SneakyThrows
        public void initialize(final ConfigurableApplicationContext context) {
            TestPropertyValues.of(
                    "app.aes.init-vector:AeGw0IuSqNcoL8zbvmBKsw==",
                    "app.aes.secret-key:SoEprI5uHWGpKvECh/9/5hY/HhmT6LKK//7hygLlR1s=",
                    "app.jwt.base64-secret:"
                            + "2Cd4Z22IEd8AfDN0bRFtDAeldaw9kH1854XMiTbkd57M0b99clYPcGp7KWDjjE7N8Wu2cItmAjyExEyjtVyBtA=="
            ).applyTo(context);
        }

    }

    @AfterEach
    public void clearDatabase() {
        officeRepository.deleteAllInBatch();
        employeeRepository.deleteAll();
        requestRepository.deleteAll();
        userRepository.deleteAll();
    }

    protected UserEntity createUser(final LoginRequest loginRequest, final UserRoleEntity role) {
        var secretKey = appConfiguration.getAes().getSecretKey();
        var initVector = appConfiguration.getAes().getInitVector();
        var user = UserEntity.builder()
                .username(loginRequest.getUsername())
                .encryptedPassword(encryptData(loginRequest.getPassword(), secretKey, initVector))
                .displayName(prepareDisplayName(loginRequest.getUsername()))
                .role(role)
                .build();
        return userRepository.saveAndFlush(user);
    }

    protected EmployeeEntity createEmployee(final String username) {
        var usernameArray = username.split(Pattern.quote("."));
        reverse(asList(usernameArray));
        var name = join(" ", usernameArray);
        return employeeRepository.saveAndFlush(employeeEntity(name));
    }

    protected OfficeEntity createOffice() {
        return officeRepository.save(officeEntity(TEST_LOCATION));
    }

    protected RoomEntity createRoom(final OfficeEntity office, final EmployeeEntity manager) {
        return roomRepository.save(roomEntity(office, manager));
    }

    protected PlaceEntity createPlace(final RoomEntity room) {
        return placeRepository.save(placeEntity(room));
    }

    protected MockMultipartFile getMockMultiPartFile() throws IOException {
        var inputStream = resourceLoader.getResource(IMAGE_PATH + VSDX_IMAGE_NAME).getInputStream();
        return new MockMultipartFile("schemaFile", VSDX_IMAGE_NAME, "application/vnd.ms-visio.drawing",
                inputStream);
    }

}
