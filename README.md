# Workplace Manager Backend

[Application](http://94.130.59.30:8080) gives users an ability to [creating or updating offices](https://gitlab.itechart-group.com/d6.g6/workplace-manager/workplace-manager-backend/-/wikis/home/user_guide/new_office_import), 
[managing movement requests](https://gitlab.itechart-group.com/d6.g6/workplace-manager/workplace-manager-backend/-/wikis/home/user_guide/movement_requests), 
take a look at seatings in company offices with information about all employees there. For using it you should log in with 
your credentials for [SMG](https://smg.itechart-group.com/).

More information about how offices' images are synchronized with file storage you can find [here](https://gitlab.itechart-group.com/d6.g6/workplace-manager/workplace-manager-backend/-/wikis/home/vsdx-and-svg-sync-with-file-storage).

## Technologies

PostgreSql was chosen as the database. It's is an open source object-relational database system with over 30 years of 
active development that has earned it a strong reputation for reliability, feature robustness, and performance.
                                                                                  
Swagger was chosen for designing, building, documenting and consuming REST APIs.

Groovy was chosen for configuring Gradle. 

CheckStyle, Jacoco and Pmd plugins were chosen for supporting quality and reliable code.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Project requires Java 11, Make, Docker, PostgreSql(optional if you launch database via docker).

### Installing

Instructions for Java: [AdoptOpenJDK installation](https://adoptopenjdk.net/installation.html?variant=openjdk11)

Instructions for Docker: [Docker installation](https://docs.docker.com/v17.12/install/)

Instructions for Make: [Install make on Windows](http://gnuwin32.sourceforge.net/packages/make.htm)

Instructions for PostgreSql: [Install PostgreSql](https://www.postgresql.org/download/)

### Running

Project requires running PostgreSql database. You can connect it via `docker-compose up` or by adding it as datasource to
Intellij Idea. To configure datasource follow next steps:
 1. In the **Database** tool window **(View | Tool Windows | Database)**, click the Data Source Properties icon.
 2. In the **Data Sources and Drivers** dialog select PostgreSQL.
 3. Specify database connection details. You can find them in *application.yaml*.
 4. To ensure that the connection to the data source is successful, click **Test Connection**.

Then the project should be started, with the following command:

`./gradlew bootRun` for MacOS or Linux and `gradlew bootRun` for Windows.

### Running the tests

`./gradlew test` for MacOS or Linux and `gradlew test` for Windows.

## Contributing

Before adding new feature to project make sure code satisfies rules made with CheckStyle, Pmd plugins and there is enough
test coverage. Run `./gradlew clean build` for MacOS or Linux and `gradlew clean build` for Windows to check it. 
Also you can visit [Jenkins](http://95.217.21.250:8080) to ran pipeline and see the results.

Useful documentation:

For CheckStyle: [CheckStyle Rules](https://checkstyle.sourceforge.io/config_coding.html)

For Pmd: [Pmd Java Rules](https://pmd.github.io/pmd-6.20.0/pmd_rules_java_codestyle.html#shortvariable)

Minimal test coverage must be 75%. For more information visit 
[Jacoco Documentation](https://www.jacoco.org/jacoco/trunk/doc/)

## Deploying

The app is deployed via [Jenkins](http://95.217.21.250:8080) from `develop` branch only and is available [here](http://94.130.59.30:8080).
