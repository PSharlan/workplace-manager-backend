#!/bin/bash
PROJECT_NAME=workplace-backend
VERSION=$(echo `git rev-parse --short HEAD`)

echo 'Pulling...'

./gradlew clean
./gradlew build

docker build --build-arg APP_VERSION=${VERSION} -t ${PROJECT_NAME}:latest .