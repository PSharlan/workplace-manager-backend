#!/bin/bash

DOCKER_HUB_REPOSITORY=vladmigura/workplace
PROJECT_NAME=workplace-backend
VERSION=$(echo `git rev-parse --short HEAD`)

docker tag ${PROJECT_NAME} ${DOCKER_HUB_REPOSITORY}:latest
docker tag ${PROJECT_NAME} ${DOCKER_HUB_REPOSITORY}:${VERSION}

docker push ${DOCKER_HUB_REPOSITORY}:latest
docker push ${DOCKER_HUB_REPOSITORY}:${VERSION}